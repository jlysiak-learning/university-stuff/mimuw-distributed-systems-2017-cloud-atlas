#!/bin/bash

# Very sophisticated script to create procesess number fluctuations
#

if [[ "$#" != 2 ]]; then
  echo
  echo "Usage: $0 <# of processes> <sleep time range in seconds>"
  echo
  echo -e "\tProgram creates given number of sleeping processes in while true loop."
  echo -e "\tEach loop last excatly given number of seconds however each created"
  echo -e "\tsleep process sleeps random number of seconds in range [1, sleep time range]\n\n"
  exit 1
fi

CNT=$1
RANGE=$2

while true
do
  for i in `seq $CNT`
  do
    sleep $(( ( RANDOM % $RANGE ) + 1 )) &
  done
  echo "$CNT new processes created... wait $RANGE seconds..."
  sleep $RANGE

done


