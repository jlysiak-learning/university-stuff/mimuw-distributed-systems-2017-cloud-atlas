# Distributed Systems @ MIMUW Fall 2017

This repository contains CloudAtlas distributed system written during Distributed Systems course at University of Warsaw.

Part of code (with some implementation gaps) was given as an excercise/materials during labs.

## Content

- `CloudAtlas` - project sources
- `VBoxDemo` - some scripts for VBox VMs management and CloudAtlas deployment
- `MIMUWLabDemo` - scripts for quick demonstration during labs 

