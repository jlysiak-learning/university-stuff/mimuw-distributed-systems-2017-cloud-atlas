/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.rmi;

import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query.QueryAttribute;
import pl.edu.mimuw.cloudatlas.common.rmi.CloudAtlasRMI;
import pl.edu.mimuw.cloudatlas.common.rmi.RMICallResult;
import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.ZMI;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.AttributesMap;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueString;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import static pl.edu.mimuw.cloudatlas.datamodel.ZMI.findZmi;
import static pl.edu.mimuw.cloudatlas.datamodel.ZMI.getZonesList;

/**
 * Handle all remote RMI calls.
 */
public class RMIExecutor implements CloudAtlasRMI {
    private final RMIModule rmiModule;

    public RMIExecutor(RMIModule rmiModule) {
        this.rmiModule = rmiModule;
    }

    @Override
    public List<String> getStoredZMIs() throws RemoteException {
        List<String> list = new ArrayList<>();
        ZMI zmi = rmiModule.getCachedZmis();
        if (zmi != null)
            getZonesList(zmi, list);
        return list;
    }

    @Override
    public AttributesMap getZMIAttributes(String path) throws RemoteException {
        AttributesMap attr = new AttributesMap();
        ZMI zmi = rmiModule.getCachedZmis();
        List<QueryAttribute> queries = rmiModule.getQueries();
        for (QueryAttribute qa : queries)
            attr.addOrChange(qa.getAttribute(), new ValueString(qa.getQueryString()));
        if (zmi != null) {
            zmi = findZmi(new PathName(path), zmi);
            if (zmi != null) { // Not found... Return empty set.
                attr.addOrChange(zmi.getAttributes().clone());
            }
        }
        return attr;
    }

    @Override
    public RMICallResult installQuery(Attribute attribute, String query, byte[] signature, long id) throws RemoteException {
        rmiModule.installQuery(attribute, query, signature, id);
        return new RMICallResult(RMICallResult.RMICallStatus.OK, "Query installation requested!");
    }

    @Override
    public RMICallResult uninstallQuery(Attribute name, String query, byte[] signature, long id) throws RemoteException {
        rmiModule.uninstallQuery(name, query, signature, id);
        return new RMICallResult(RMICallResult.RMICallStatus.OK, "Query uninstallation request sent!");
    }

    @Override
    public RMICallResult setZMIAttribute(String name, Value value) throws RemoteException {
        rmiModule.setMyAttribute(name, value);
        return new RMICallResult(RMICallResult.RMICallStatus.OK, "Attribute set!");
    }

    @Override
    public void setFallbackContacts(ValueList fallbackContacts) throws RemoteException {
        rmiModule.setFallbackContacts(fallbackContacts);
    }

    @Override
    public String whoAreYou() throws RemoteException {
        return rmiModule.getMyPath();
    }
}