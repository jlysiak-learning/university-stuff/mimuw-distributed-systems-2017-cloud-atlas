package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.serialization;

import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.event.GossipEvent;

/** Wrapper helps Kryo to create abstract GossipEvent. */
public class Wrapper {
    private final GossipEvent event;

    private Wrapper() {
        event = null;
    }

    Wrapper(GossipEvent event) {
        this.event = event;
    }

    public GossipEvent getEvent() {
        return event;
    }
}
