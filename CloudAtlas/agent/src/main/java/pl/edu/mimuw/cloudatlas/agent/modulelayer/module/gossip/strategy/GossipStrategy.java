/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.strategy;

import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueContact;

import java.util.*;

public abstract class GossipStrategy {
    final int maxLevel;

    int chosenLevel;

    final Random random;

    GossipStrategy(int maxLevel) {
        this.maxLevel = maxLevel;
        random = new Random(TimerModule.now());
    }

    public static GossipStrategy getFromType(GossipStrategyType strategy, int singletonLevel) throws NoSuchSelectionStrategyException {
        switch (strategy) {
            case RR_EQF:
                return new EqFreqRoundRobinGossipStrategy(singletonLevel);
            case RR_EDF:
                return new ExpDecFreqRoundRobinGossipStrategy(singletonLevel);
            case RD_EQP:
                return new EqProbRandomGossipStrategy(singletonLevel);
            case RD_EDP:
                return new ExpDecProbRandomGossipStrategy(singletonLevel);
                default:
                    throw new NoSuchSelectionStrategyException(strategy);
        }
    }

    public ValueContact selectContact(List<List<ValueContact>> contacts) {
        int nonemptyCount = 0;
        int[] occupation = new int[contacts.size()];
        Arrays.fill(occupation, 0);
        for (int i = 1; i < contacts.size(); i++) {
            List<ValueContact> list = contacts.get(i);
            if (list.size() > 0) {
                nonemptyCount += list.size();
                occupation[i] = list.size();
            }
        }
        if (nonemptyCount == 0)
            return null;
        selectLevel(occupation);
        List<ValueContact> list = contacts.get(chosenLevel);
        ValueContact selectedContact = list.get(random.nextInt(list.size()));
        return selectedContact;
    }

    protected abstract void selectLevel(int[] occupation);

    public int getChosenLevel() {
        return chosenLevel;
    }
}
