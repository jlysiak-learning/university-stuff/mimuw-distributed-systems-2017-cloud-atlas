/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network;

import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.PreparedDatagram;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.SendRequest;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.*;
import java.util.concurrent.BlockingQueue;

import static java.lang.Integer.min;

/**
 * Network module's helper thread.
 *
 * In case of failures, signalizes it to parent module which can
 * try to resend packet or drop.
 */
public class UDPSender extends Thread {
    private final BlockingQueue<SendRequest> sndQueue;

    private final ExecutorHostInterface msgQueue;

    private volatile boolean isRunning;

    private DatagramSocket socket;

    private final int datagramMaxSize;
    private final static int HEADER_SIZE = 20;

    public UDPSender(BlockingQueue<SendRequest> sndQueue, ExecutorHostInterface msgQueue, int datagramMaxSize) {
        setName("UDP_SENDER");
        isRunning = true;
        this.msgQueue = msgQueue;
        this.sndQueue = sndQueue;
        this.datagramMaxSize = datagramMaxSize;
        Log.log(getName(), "Helper thread created.");
    }

    /**
     * Sender thread waits for send requests.
     * Each request is processed and whole data is packed into byte array.
     * 8 bytes of each byte array is left for actual time which is just before firing
     * OS send request. This should help in precise time tracking.
     */
    @Override
    public void run() {
        Log.log(getName(), "Running...");
        createSocket();
        while (isRunning) {
            try {
                SendRequest request = sndQueue.take();
//                Log.log(getName(), request.toString());
                Collection<PreparedDatagram> preparedDatagrams = prepareDatagrams(request);
                for (PreparedDatagram datagram : preparedDatagrams) {
                    socket.send(datagram.makeDatagram(TimerModule.now()));
                }
            } catch (InterruptedException e) {
                // Just begin new loop...
                Log.err(getName(), "Interrupted exception: " + e.getMessage());
            } catch (IOException e) {
                Log.err(getName(), "I/O exception: " + e.getMessage());
            }
        }
        socket.close();
        Log.log(getName(), "Exiting...");
    }

    public Collection<PreparedDatagram> prepareDatagrams(SendRequest request) throws IOException {
        List<PreparedDatagram> elements = new LinkedList<>();
        byte[] payloadBytes = request.getPayload();
        // Datagram payload: [timestamp, msgId, part, parts][data]
        int singlePayloadSize = datagramMaxSize - HEADER_SIZE;
        // # of datagrams required to send whole message object
        int parts = (payloadBytes.length + singlePayloadSize - 1) / singlePayloadSize;
        int toSendLeft = HEADER_SIZE * parts + payloadBytes.length; // bytes in total: headers + message
        int offset = 0;

        for (int i = 0; i < parts; i++) {
            int toSend = min(toSendLeft, datagramMaxSize);
            int toSendPayload = toSend - HEADER_SIZE;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(baos);
            dos.writeLong(0);
            dos.writeInt(request.getMsgId());
            dos.writeInt(i);
            dos.writeInt(parts);
            dos.write(payloadBytes, offset, toSendPayload);
            dos.close();
            PreparedDatagram element = new PreparedDatagram(request.getDestination(), baos.toByteArray());
            elements.add(element);
            offset += toSendPayload;
            toSendLeft -= toSend;
        }
        return elements;
    }

    private void createSocket() {
        try {
            socket = new DatagramSocket();
            Log.log(getName(), "Socket created.");
        } catch (SocketException e) {
            Log.err(getName(), "Socket exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void shutdown() {
        Log.log(getName(), "Shutdown signal from parent.");
        isRunning = false;
        sndQueue.clear();
        // Causing interrupt once on exit do not have
        // any impact on system performance
        this.interrupt();
    }
}
