/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi;

import pl.edu.mimuw.cloudatlas.agent.configuraton.Configuration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.MainConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.ZmiConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.NoConfigurationException;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Content;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.GetRequest;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Handler;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Payload;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.Module;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.QueryAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.action.AllQueriesPackage;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.action.StartTickAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.action.SetSingletonAttribute;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.action.UpdateNonlocalZmi;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage.ZMIStorage;

public class ZMIModule extends Module {
    private final ZMIStorage zmiStorage;

    private final long zmiRecomputationInterval;

    private final Handler<GetRequest> GET_ZMIS_HANDLER = new Handler<GetRequest>() {
        @Override
        public void handle(GetRequest payload) {
            ModuleType whom = payload.getWhom();
            int action = payload.getAction();
            send(whom, action, new Content<>(zmiStorage.getSnapshotClone()));
        }
    };

    private final Handler<UpdateNonlocalZmi> UPDATE_NONLOCAL_ZMI_HANDLER = new Handler<UpdateNonlocalZmi>() {
        @Override
        public void handle(UpdateNonlocalZmi payload) {
            zmiStorage.updateNonLocalZmi(payload.getFromHere(), payload.getToHere(), payload.getContent());
        }
    };

    private final Handler<AllQueriesPackage> RECOMPUTE_ZMIS_HANDLER = new Handler<AllQueriesPackage>() {
        @Override
        public void handle(AllQueriesPackage payload) {
            zmiStorage.recompute(payload.getPredefined(), payload.getInstalled());
        }
    };

    private final Handler<SetSingletonAttribute> SET_SINGLETON_ATTRIBUTE_HANDLER = new Handler<SetSingletonAttribute>() {
        @Override
        public void handle(SetSingletonAttribute payload) {
            zmiStorage.setSingletonAttribute(payload.getAttribute(), payload.getValue());
        }
    };

    private final Handler<Payload> INIT_RECOMPUTATION_HANDLER = new Handler<Payload>() {
        @Override
        public void handle(Payload payload) {
            send(ModuleType.QUERY, QueryAction.GET_ALL_QUERIES, new GetRequest(ModuleType.ZMI, ZMIAction.UPDATE_ZMIS));
        }
    };


    public ZMIModule(ExecutorHostInterface hostInterface, Configuration configuration) throws NoConfigurationException {
        super(hostInterface, ModuleType.ZMI, configuration);
        ZmiConfiguration zmiConfiguration = (ZmiConfiguration) configuration.getConfiguration(ModuleType.ZMI);
        zmiRecomputationInterval = zmiConfiguration.getRefreshInterval();
        long purgeAfterTime = zmiConfiguration.getPurgeUnfreshAfter();

        MainConfiguration mainConfiguration = (MainConfiguration) configuration.getConfiguration(ModuleType.MAIN);
        zmiStorage = new ZMIStorage(mainConfiguration.getNodeContact(), purgeAfterTime);

        registerActionHandler(ZMIAction.GET_ZMIS, GET_ZMIS_HANDLER);
        registerActionHandler(ZMIAction.UPDATE_NONLOCAL_ZMI, UPDATE_NONLOCAL_ZMI_HANDLER);
        registerActionHandler(ZMIAction.UPDATE_ZMIS, RECOMPUTE_ZMIS_HANDLER);
        registerActionHandler(ZMIAction.SET_SINGLETON_ATTRIBUTE, SET_SINGLETON_ATTRIBUTE_HANDLER);
        registerActionHandler(ZMIAction.INIT_RECOMPUTATION, INIT_RECOMPUTATION_HANDLER);
    }

    @Override
    public void init() {
        send(ModuleType.TIMER, TimerAction.START_TICK, new StartTickAction(ModuleType.ZMI, ZMIAction.INIT_RECOMPUTATION, zmiRecomputationInterval, 0, 0));
    }
}
