/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip;

import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.event.ExchangeQueryInit;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.event.GossipEvent;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueContact;

import java.net.InetAddress;

public class GossipState {
    private final int gossipAttempts;

    private final long gossipTimeout;

    private ValueContact contact;

    private int level;

    private int attemptsLeft;

    private long startedAt;

    private long nextAttemptAt;

    private boolean answerReceived;

    private GossipEvent msg;

    GossipState(int gossipAttempts, long gossipTimeout) {
        this.gossipAttempts = gossipAttempts;
        this.gossipTimeout = gossipTimeout;
        attemptsLeft = 0;
    }

    public void gossipStarted(ValueContact contact, int level, ExchangeQueryInit msg) {
        this.msg = msg;
        attemptsLeft = gossipAttempts - 1;
        startedAt = TimerModule.now();
        nextAttemptAt = startedAt + gossipTimeout;
        this.contact = contact;
        this.level = level;
        answerReceived = false;
    }

    public boolean tryAgain() {
        if (TimerModule.now() > nextAttemptAt && attemptsLeft > 0 && !answerReceived) {
            nextAttemptAt += gossipTimeout;
            attemptsLeft--;
            return true;
        }
        return false;
    }

    public void answeredReceived() {
        answerReceived = true;
    }

    public PathName getPartnerPath() {
        return contact.getName();
    }

    public InetAddress getContact() {
        return contact.getAddress();
    }

    public GossipEvent getMessage() {
        return msg;
    }
}
