/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network;

import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.router.Routable;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Content;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Message;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.MessageFragment;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.TimeSample;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;

/**
 * Networking module helper thread.
 * Binds UDP socket and listen in blocking mode.
 * Received bulk datagrams are passed to parent Network module.
 */
public class UDPReceiver extends Thread {
    private final ExecutorHostInterface parentQueue;

    private final int listenPort;

    private final int maxSize;

    private DatagramSocket socket;

    private volatile boolean isRunning;


    UDPReceiver(ExecutorHostInterface hostInterface, int port, int maxSize) {
        setName("UDP_RECEIVER");
        parentQueue = hostInterface;
        listenPort = port;
        isRunning = true;
        this.maxSize = maxSize;
        Log.log(getName(), "Helper thread created.");
    }

    /**
     * Attempts to create socket and listen.
     * All datagrams are parsed and sent directly to parent
     * which does higher level stuff.
     */
    @Override
    public void run() {
        Log.log(getName(), "Running...");
        createSocket();
        while (isRunning) {
            DatagramPacket recv = new DatagramPacket(new byte[maxSize], maxSize);
            try {
                socket.receive(recv);
                Routable msg = parseDatagram(recv);
                if (msg != null)
                    parentQueue.sendMessage(msg);
            } catch (IOException e) {
                handleIOExceptino(e);
            }
        }
        Log.log(getName(), "Exiting...");
    }

    /**
     * Try to create UDP socket.
     */
    private void createSocket() {
        try {
            socket = new DatagramSocket(listenPort);
            Log.log(getName(), "Socket created.");
        } catch (SocketException e) {
            socket = null;
            handleSocketException(e);
        }
    }

    private Routable parseDatagram(DatagramPacket recv) {
        long now = TimerModule.now();
        if (recv.getData().length > maxSize || recv.getData().length < 20) {
            Log.wrn(getName(), "Junk data received. To short or to long.");
            return null;
        }
        ByteBuffer buff = ByteBuffer.wrap(recv.getData(),0,recv.getLength());
        TimeSample timeSample = new TimeSample(buff.getLong(), now);
        int id = buff.getInt();
        int part = buff.getInt();
        int parts = buff.getInt();
        byte[] payload = new byte[recv.getLength() - 20];
        buff.get(payload);
        MessageFragment fragment = new MessageFragment((InetSocketAddress) recv.getSocketAddress(), id, part, parts, payload, timeSample);
        return new Message(ModuleType.NETWORK, ModuleType.NETWORK, NetworkAction._DATAGRAM_RECEIVED, new Content<>(fragment));
    }

    /**
     * Handle IOException.
     * Detects interrupt condition.
     * This exception can be caused by some OS/socket problems as well as
     * parent module shutdown signal. Shutdown procedure call can be detected
     * just by checking #isRunning flag.
     */
    private void handleIOExceptino(IOException e) {
        Log.err(getName(), "I/O exception: " + e.getMessage());
    }

    /**
     * Handles socket exception.
     *
     * @param e exception
     */
    private void handleSocketException(SocketException e) {
        Log.err(getName(), "Socket exception: " + e.getMessage());
    }

    /**
     * Shutdown receiver.
     * Clear #isRunning flag and close UDP socket (if open)
     */
    public void shutdown() {
        Log.log(getName(), "Shutdown signal from parent.");
        isRunning = false;
        if (socket != null) socket.close();
        this.interrupt();
    }
}
