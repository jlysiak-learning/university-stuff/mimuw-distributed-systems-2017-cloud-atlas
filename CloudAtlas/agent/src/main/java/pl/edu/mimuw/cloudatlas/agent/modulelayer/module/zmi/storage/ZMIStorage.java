/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage;

import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.TimeSample;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query.Query;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;
import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.ZMI;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.AttributesMap;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueContact;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueTime;
import pl.edu.mimuw.cloudatlas.service.interpreter.InterpreterService;

import java.util.Collection;
import java.util.List;

import static pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage.ZMIHierarchy.createBasicAttributeSet;

/**
 * Main ZMI storage.
 *
 * Buffer holds newest updates in singleton zone and
 * ZMIs exchanged by gossiping with other agents.
 * Local path above singleton will never be upadted.
 * However, this buffer is passed to #QueryModule and there
 * this path is recomputed.
 * Such #ZMIHierarchy backing after queries executions
 * is used as a snapshot and cloned to other modules as a cache.
 */
public class ZMIStorage {

    private final PathName myPath;

    private final long purgeAfterTime;

    private ZMIHierarchy snapshot;

    private ZMIHierarchy buffer;

    /**
     * Create empty ZMI holder.
     *  @param contact       agents singleton path
     * @param purgeAfterTime ZMI expiration timeout
     */
    public ZMIStorage(ValueContact contact, long purgeAfterTime) {
        myPath = contact.getName();
        this.purgeAfterTime = purgeAfterTime;

        snapshot = ZMIHierarchy.fromNodeContact(contact);
        buffer = ZMIHierarchy.fromNodeContact(contact);
    }

    public ZMIHierarchy getSnapshotClone() {
        return snapshot.clone();
    }

    /**
     * Clone buffer, recompute and overwrite snapshot buffer.
     *
     * @param predefined predefined queries
     * @param installed  installed queries
     */
    public void recompute(List<Query> predefined, List<Query> installed) {
        ZMIHierarchy zmis = buffer.clone(); // Take snapshot of buffer
        zmis.removeZmisOlderThan(TimerModule.now(), purgeAfterTime); // Remove all old ZMIs before re-computation
        ZMI zmi = zmis.getSingleton();
        zmi.getAttributes().addOrChange("timestamp", new ValueTime(TimerModule.now())); // In case of fetcher failure, keep it alive
        while (zmi.getFather() != null) {
            AttributesMap newAttributes = computeParentsAttributes(zmi.getFather(), predefined, installed);
            zmi = zmi.getFather();
            zmi.getAttributes().clear();
            zmi.getAttributes().add(newAttributes);
        }
        snapshot = zmis;
        Log.log("ZMI-STORE", "ZMI structure recomputed!");
//        snapshot.getRoot().printAttributes(System.err);
    }

    /**
     * Recompute single level of zone hierarchy.
     *
     * @return new set of attributes with fresh timestamps
     */
    private AttributesMap computeParentsAttributes(ZMI parent, List<Query> predefinedQueries, Collection<Query> queries) {
        AttributesMap queryResults = new AttributesMap();
        for (Query predefined : predefinedQueries)
            // Predefined queries must be correct
            InterpreterService.executeQuery(predefined.getProgram(), parent, queryResults);
        for (Query query : queries) {
            try {
                InterpreterService.executeQuery(query.getProgram(), parent, queryResults);
            } catch (Exception e) {
                // Print error and skip buggy query
                Log.err("ZMI-STORE", String.format("Query execution exception for %s (level %d): %s", parent.getLocalName(), parent.getLevel(), e.getMessage()));
            }
        }
        AttributesMap attributesMap = createBasicAttributeSet(parent.getLocalName(), myPath.getName(), parent.getLevel(), TimerModule.now());
        attributesMap.addOrChange(queryResults);
        return attributesMap;
    }

    /**
     * Update buffer ZMI hierarchy.
     * @param fromHere from here
     * @param toHere to here
     * @param update gossiped update
     */
    public void updateNonLocalZmi(TimeSample fromHere, TimeSample toHere, ZMIUpdate update) {
        buffer.updateNonLocalZmi(fromHere, toHere, update);
    }

    /**
     * Update agent's singleton ZMI in buffer hierarchy.
     * @param attribute name
     * @param value value
     */
    public void setSingletonAttribute(Attribute attribute, Value value) {
        buffer.setSingletonAttribute(attribute, value);
    }
}
