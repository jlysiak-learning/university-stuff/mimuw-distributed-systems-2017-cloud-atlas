/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query;

import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.query.Absyn.Program;

/** Simple query storage container */
public class Query {
    private final Attribute name;

    private final String query;

    private final Program program;

    private final byte[] signature;

    private final long id;

    private final boolean deleted;

    public Query(Attribute name, String query, Program program, byte[] signature, long id, boolean deleted) {
        this.name = name;
        this.query = query;
        this.program = program;
        this.signature = signature;
        this.id = id;
        this.deleted = deleted;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public Attribute getName() {
        return name;
    }

    public String getQuery() {
        return query;
    }

    public Program getProgram() {
        return program;
    }


    public long getId() {
        return id;
    }

    public byte[] getSignature() {
        return signature;
    }
}
