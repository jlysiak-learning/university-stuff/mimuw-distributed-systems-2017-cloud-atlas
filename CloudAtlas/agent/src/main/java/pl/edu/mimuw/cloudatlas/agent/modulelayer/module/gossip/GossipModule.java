/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip;

import pl.edu.mimuw.cloudatlas.agent.configuraton.Configuration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.GossipConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.MainConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.ZmiConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.NoConfigurationException;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Content;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.GetRequest;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Handler;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Payload;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.Module;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.action.InetMessageReceived;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.event.*;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.serialization.GossipEventSerializer;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.strategy.GossipStrategy;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.strategy.NoSuchSelectionStrategyException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.NetworkAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.action.InetMessageRequest;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.TimeSample;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.QueryAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.action.InstallQuery;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query.QueryAttribute;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.storage.QueryStorageCache;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.action.StartTickAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.ZMIAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.action.UpdateNonlocalZmi;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage.ZMIHierarchy;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage.ZMIOffer;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage.ZMIUpdate;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueContact;

import java.net.InetAddress;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GossipModule extends Module {
    private final Random random = new Random(TimerModule.now());

    private final GossipStrategy strategy;

    private final int gossipAttempts;

    private final long gossipInterval;

    private final long cacheUpdateInterval;

    private final List<ValueContact> fallbackContacts = new LinkedList<>();

    private final long gossipTimeout;

    private final QueryStorageCache queryCache;

    private final GossipEventSerializer gossipEventSerializer;

    private final EventHandler[] gossipEventsHandlers;

    private final GossipState gossipState;

    private final Handler<Payload> GOSSIP_TIMEOUT_HANDLER = new Handler<Payload>() {
        @Override
        public void handle(Payload payload) {
            if (gossipState.tryAgain()) {
                Log.log(name, "Gossip again...");
                sendInetMessage(gossipState.getContact(), gossipState.getMessage());
            }
        }
    };

    private final Handler<Payload> UPDATE_CACHE_INIT_HANDLER = new Handler<Payload>() {
        @Override
        public void handle(Payload payload) {
            send(ModuleType.ZMI, ZMIAction.GET_ZMIS, new GetRequest(ModuleType.GOSSIP, GossipAction.UPDATE_ZMI_CACHE));
            send(ModuleType.QUERY, QueryAction.GET_QUERIES, new GetRequest(ModuleType.GOSSIP, GossipAction.UPDATE_QUERY_CACHE));
        }
    };

    private final Handler<Content<List<QueryAttribute>>> UPDATE_QUERY_CACHE_HANDLER = new Handler<Content<List<QueryAttribute>>>() {
        @Override
        public void handle(Content<List<QueryAttribute>> payload) {
            queryCache.updateCache(payload.getContent());
        }
    };

    private final Handler<GetRequest> GET_FALLBACK_CONTACTS_HANDLER = new Handler<GetRequest>() {
        @Override
        public void handle(GetRequest payload) {
            send(payload.getWhom(), payload.getAction(), new Content<>(Collections.unmodifiableList(fallbackContacts)));
        }
    };

    private final Handler<Content<List<ValueContact>>> SET_FALLBACK_CONTACTS_HANDLER = new Handler<Content<List<ValueContact>>>() {
        @Override
        public void handle(Content<List<ValueContact>> payload) {
            fallbackContacts.clear();
            fallbackContacts.addAll(payload.getContent());
            for (ValueContact contact : payload.getContent()) {
                contactQueue.push(contact);
            }
        }
    };

    /**
     * Gossiping starts with exchanging list of installed queries on agents.
     * Agent just send cached queries stored on gossiping module.
     * Cache is updated 4 times more often than ZMIs are recomputed.
     */
    private final EventHandler<ExchangeQueryInit> EXCHANGE_QUERY_INIT_HANDLER = new EventHandler<ExchangeQueryInit>() {
        @Override
        public void handle(InetAddress sender, TimeSample timeSample, ExchangeQueryInit event) {
            Log.log(name, "Gossip from: " + sender.toString());
            ExchangeQueryAnswer answer = new ExchangeQueryAnswer(queryCache.dumpCache(), timeSample);
            send(ModuleType.QUERY, QueryAction.INSTALL, new InstallQuery(event.getQueryOffer()));
            sendInetMessage(sender, answer);
//            dump(sender, timeSample, event, answer);
        }

        private void dump(InetAddress sender, TimeSample timeSample, ExchangeQueryInit incoming, ExchangeQueryAnswer answer) {
            StringBuilder dump = new StringBuilder("--- EXCHANGE_QUERY_INIT ---\n");
            dump.append("From: ").append(sender.toString()).append("\n");
            dump.append("Timesample: ").append(timeSample.toString()).append("\n");
            dump.append(incoming.toString()).append("\n");
            dump.append(answer.toString()).append("\n");
            dump.append("--- END of EXCHANGE_QUERY_INIT ---").append("\n");
            Log.log(name, dump.toString());
        }
    };

    private final EventHandler<UpdateZmi> UPDATE_ZMI_HANDLER = new EventHandler<UpdateZmi>() {
        private void dump(InetAddress sender, TimeSample timeSample, UpdateZmi incoming) {
            StringBuilder dump = new StringBuilder("--- UPDATE_ZMI_HANDLER ---\n");
            dump.append("From: ").append(sender.toString()).append("\n");
            dump.append("Timesample: ").append(timeSample.toString()).append("\n");
            dump.append("ZMI: ").append("\n").append(incoming.getUpdate().getZmi().toString()).append("\n");
            dump.append("Sons: ").append(incoming.getUpdate().getZmi().getSons().size()).append("\n");
            dump.append("--- END of UPDATE_ZMI_HANDLER ---").append("\n");
            Log.log(name, dump.toString());
        }

        @Override
        public void handle(InetAddress sender, TimeSample timeSample, UpdateZmi event) {
            Log.log(name, event.toString());
            TimeSample toHere = timeSample;
            TimeSample fromHere = event.getTimeSample();
            send(ModuleType.ZMI, ZMIAction.UPDATE_NONLOCAL_ZMI, new UpdateNonlocalZmi(fromHere, toHere, event.getUpdate()));
//            dump(sender, timeSample, event);
        }
    };

    private final Handler<InetMessageReceived> INET_MSG_RECV_HANDLER = new Handler<InetMessageReceived>() {
        @Override
        public void handle(InetMessageReceived payload) {
            GossipEvent gossipEvent = gossipEventSerializer.getEvent(payload.getContent());
            byte type = gossipEvent.getEventType();
            EventHandler eventHandler = gossipEventsHandlers[type];
            eventHandler.handleEvent(payload.getSender(), payload.getTimeSample(), gossipEvent);
        }
    };

    private final LinkedList<ValueContact> contactQueue;

    private ZMIHierarchy zmiCache;

    private final Handler<Payload> GOSSIP_INIT_HANDLER = new Handler<Payload>() {
        @Override
        public void handle(Payload payload) {
            startGossiping();
        }
    };

    private final Handler<Content<ZMIHierarchy>> UPDATE_ZMI_CACHE_HANDLER = new Handler<Content<ZMIHierarchy>>() {
        @Override
        public void handle(Content<ZMIHierarchy> payload) {
            zmiCache = payload.getContent();
        }
    };

    private final EventHandler<ExchangeQueryAnswer> EXCHANGE_QUERY_ANSWER_HANDLER = new EventHandler<ExchangeQueryAnswer>() {

        @Override
        public void handle(InetAddress sender, TimeSample timeSample, ExchangeQueryAnswer event) {
            List<QueryAttribute> offer = event.getQueryOffer();
            send(ModuleType.QUERY, QueryAction.INSTALL, new InstallQuery(offer));
            TimeSample toHere = timeSample;
            TimeSample fromHere = event.getTimeSample();
            List<ZMIOffer> zmiOffers = zmiCache.prepareZmiOffer(gossipState.getPartnerPath());
            sendInetMessage(sender, new OfferZMIInit(toHere, zmiOffers, zmiCache.getMyPath()));
            gossipState.answeredReceived();
        }
    };

    private final EventHandler<OfferZMIInit> OFFER_ZMI_INIT_HANDLER = new EventHandler<OfferZMIInit>() {
        @Override
        public void handle(InetAddress sender, TimeSample timeSample, OfferZMIInit event) {
            TimeSample toHere = timeSample;
            TimeSample fromHere = event.getTimeSample();
            List<ZMIOffer> recvZmiOffers = event.getZmiOffers();
            List<ZMIOffer> myZmiOffers = zmiCache.prepareZmiOffer(event.getSendersPath());
            List<ZMIUpdate> updates = zmiCache.prepareZmiUpdates(recvZmiOffers, fromHere, toHere, event.getSendersPath());
            sendInetMessage(sender, new OfferZmiAnswer(toHere, myZmiOffers));
            for (ZMIUpdate update : updates)
                sendInetMessage(sender, new UpdateZmi(timeSample, update));
        }
    };

    private final EventHandler<OfferZmiAnswer> OFFER_ZMI_ANSWER_HANDLER = new EventHandler<OfferZmiAnswer>() {
        @Override
        public void handle(InetAddress sender, TimeSample timeSample, OfferZmiAnswer event) {
            TimeSample toHere = timeSample;
            TimeSample fromHere = event.getTimeSample();
            List<ZMIOffer> recvZmiOffers = event.getZmiOffers();
            List<ZMIUpdate> updates = zmiCache.prepareZmiUpdates(recvZmiOffers, fromHere, toHere, gossipState.getPartnerPath());
            for (ZMIUpdate update : updates)
                sendInetMessage(sender, new UpdateZmi(toHere, update));
        }
    };

    public GossipModule(ExecutorHostInterface e, Configuration configuration) throws NoConfigurationException, NoSuchSelectionStrategyException {
        super(e, ModuleType.GOSSIP, configuration);
        contactQueue = new LinkedList<>();
        MainConfiguration mainConfiguration = (MainConfiguration) configuration.getConfiguration(ModuleType.MAIN);
        for (Value val : mainConfiguration.getFallbackContacts().getValue()) {
            ValueContact contact = (ValueContact) val;
            fallbackContacts.add(contact);
            contactQueue.push(contact);
        }

        ZmiConfiguration zmiConfiguration = (ZmiConfiguration) configuration.getConfiguration(ModuleType.ZMI);
        cacheUpdateInterval = zmiConfiguration.getRefreshInterval() / 2;

        registerActionHandler(GossipAction.GOSSIP_INIT, GOSSIP_INIT_HANDLER);
        registerActionHandler(GossipAction.GOSSIP_TIMEOUT, GOSSIP_TIMEOUT_HANDLER);
        registerActionHandler(GossipAction.UPDATE_CACHE_INIT, UPDATE_CACHE_INIT_HANDLER);
        registerActionHandler(GossipAction.UPDATE_ZMI_CACHE, UPDATE_ZMI_CACHE_HANDLER);
        registerActionHandler(GossipAction.UPDATE_QUERY_CACHE, UPDATE_QUERY_CACHE_HANDLER);
        registerActionHandler(GossipAction.INET_MSG_RECV, INET_MSG_RECV_HANDLER);
        registerActionHandler(GossipAction.GET_FALLBACK_CONTACTS, GET_FALLBACK_CONTACTS_HANDLER);
        registerActionHandler(GossipAction.SET_FALLBACK_CONTACTS, SET_FALLBACK_CONTACTS_HANDLER);

        queryCache = new QueryStorageCache();
        zmiCache = ZMIHierarchy.fromNodeContact(mainConfiguration.getNodeContact());


        GossipConfiguration gossipConfiguration = (GossipConfiguration) configuration.getConfiguration(ModuleType.GOSSIP);
        strategy = GossipStrategy.getFromType(gossipConfiguration.getStrategy(), zmiCache.getMyLevel());
        gossipAttempts = gossipConfiguration.getContactAttempts();
        gossipInterval = gossipConfiguration.getInterval();
        gossipTimeout = gossipInterval / (gossipAttempts * 2);

        gossipEventSerializer = new GossipEventSerializer();
        gossipEventsHandlers = new EventHandler[]{EXCHANGE_QUERY_INIT_HANDLER, EXCHANGE_QUERY_ANSWER_HANDLER, OFFER_ZMI_INIT_HANDLER, OFFER_ZMI_ANSWER_HANDLER, UPDATE_ZMI_HANDLER};
        gossipState = new GossipState(gossipAttempts, gossipTimeout);
    }

    /**
     * Gossiping initiation procedure.
     *
     * When we provide some fallback contacts at startup or set a new one
     * we want to talk once with each. It helps in data propagation.
     *
     * Since we exchange siblings ZMIs, there is a problem when two nodes in one zone
     * i.e. /pw/A, /pw/B have never been communicated but they were talking with i.e. /uw/C, /uw/D.
     * C and D cannot override /pw zone because it's local path of A and B.
     * In this situation we will end with two separated nodes in one zone /pw communicating with all nodes in /uw.
     *
     * We should provide fallback contact to node at each level then
     */
    private void startGossiping() {
        ValueContact contact;
        int level;
        if (contactQueue.size() == 0) {
            List<List<ValueContact>> contacts = zmiCache.getContactsStructure();
            contact = strategy.selectContact(contacts);
            level = strategy.getChosenLevel();
            if (contact == null) {
                Log.log(name, "No contact selected! Get one from fallback list.");
                if (fallbackContacts.size() == 0) {
                    Log.err(name, "*** No fallback contacts configured! ***");
                    return;
                }
                contact = fallbackContacts.get(random.nextInt(fallbackContacts.size()));
                level = zmiCache.getMyLevel();
            }
        } else {
            contact = contactQueue.poll();
            level = zmiCache.getMyLevel();
        }

        Log.log(name, "Start gossiping with " + contact.toString() + " @ level " + level);
        ExchangeQueryInit msg = new ExchangeQueryInit(queryCache.dumpCache(), level);
        gossipState.gossipStarted(contact, level, msg);
        sendInetMessage(contact.getAddress(), msg);
    }

    private void sendInetMessage(InetAddress sender, GossipEvent event) {
        byte[] payload = gossipEventSerializer.getBytes(event);
        send(ModuleType.NETWORK, NetworkAction.SEND_MESSAGE, new InetMessageRequest(sender, payload));
    }

    @Override
    public void init() {
        send(ModuleType.TIMER, TimerAction.START_TICK, new StartTickAction(ModuleType.GOSSIP, GossipAction.GOSSIP_INIT, gossipInterval, 900, 0));
        send(ModuleType.TIMER, TimerAction.START_TICK, new StartTickAction(ModuleType.GOSSIP, GossipAction.GOSSIP_TIMEOUT, gossipTimeout, 1000, 0));
        send(ModuleType.TIMER, TimerAction.START_TICK, new StartTickAction(ModuleType.GOSSIP, GossipAction.UPDATE_CACHE_INIT, cacheUpdateInterval, 1000, 0));
    }
}
