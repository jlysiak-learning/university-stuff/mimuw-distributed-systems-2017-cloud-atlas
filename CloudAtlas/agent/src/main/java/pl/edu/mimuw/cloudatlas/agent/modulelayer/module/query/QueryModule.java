/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query;

import pl.edu.mimuw.cloudatlas.agent.configuraton.Configuration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.SecurityConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.NoConfigurationException;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Content;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.GetRequest;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Handler;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.Module;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.action.InstallQuery;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.action.UninstallQuery;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.action.AllQueriesPackage;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query.QueryAttribute;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.storage.QueryStorage;
import pl.edu.mimuw.cloudatlas.common.security.SignerCipher;

import java.security.PublicKey;

public class QueryModule extends Module {
    private final QueryStorage queryStorage;


    private final Handler<GetRequest> GET_QUERIES_HANDLER = new Handler<GetRequest>() {
        @Override
        public void handle(GetRequest payload) {
            send(payload.getWhom(), payload.getAction(), new Content<>(queryStorage.dumpQueries()));
        }
    };

    private final Handler<InstallQuery> INSTALL_QUERY_HANDLER = new Handler<InstallQuery>() {
        @Override
        public void handle(InstallQuery payload) {
            for (QueryAttribute query : payload.getContent())
                queryStorage.installQuery(query);
        }
    };

    private final Handler<UninstallQuery> UNINSTALL_QUERY_HANDLER = new Handler<UninstallQuery>() {
        @Override
        public void handle(UninstallQuery payload) {
            // Call install query as well
            // Read more about it in SignerService
            queryStorage.installQuery(payload.getContent());
        }
    };

    private final Handler<GetRequest> GET_ALL_QUERIES_HANDLER = new Handler<GetRequest>() {
        @Override
        public void handle(GetRequest payload) {
            send(payload.getWhom(), payload.getAction(), new AllQueriesPackage(queryStorage.getPredefinedQueries(), queryStorage.getQueries()));
        }
    };

    public QueryModule(ExecutorHostInterface hostInterface, Configuration configuration) throws Exception, NoConfigurationException {
        super(hostInterface, ModuleType.QUERY, configuration);

        SecurityConfiguration securityConfiguration = (SecurityConfiguration) configuration.getConfiguration(ModuleType.SECURITY);

        registerActionHandler(QueryAction.GET_QUERIES, GET_QUERIES_HANDLER);
        registerActionHandler(QueryAction.INSTALL, INSTALL_QUERY_HANDLER);
        registerActionHandler(QueryAction.UNINSTALL, UNINSTALL_QUERY_HANDLER);
        registerActionHandler(QueryAction.GET_ALL_QUERIES, GET_ALL_QUERIES_HANDLER);

        PublicKey publicKey = SignerCipher.getPublicKey(securityConfiguration.getKeyPath());
        queryStorage = new QueryStorage(publicKey);
    }
}
