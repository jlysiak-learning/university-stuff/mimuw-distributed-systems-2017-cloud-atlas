/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.executorlayer.executor;

import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorGuestInterface;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorMessageQueue;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.router.Routable;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.router.RouterInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Executor class.
 * Base runnable class which executes modules' code.
 */
public class Executor implements Runnable, ExecutorHostInterface {
    /// Message queue
    private final BlockingQueue<Routable> messageQueue;

    /// Hosting modules map
    private final Map<ModuleType, ExecutorGuestInterface> moduleMapping;

    /// Message queue interface to let others queueing new messages.
    private final ExecutorMessageQueue messageQueueHandler;

    /// Router interface
    private final RouterInterface router;

    /// Executor's name
    protected String name;

    /// Is executor running?
    boolean isRunning;

    public Executor(RouterInterface router) {
        this("EXECUTOR", router, new LinkedBlockingQueue<>());
    }

    public Executor(String name, RouterInterface router) {
        this(name, router, new LinkedBlockingQueue<>());
    }

    Executor(String name, RouterInterface router, BlockingQueue<Routable> queue) {
        this.name = name;
        messageQueue = queue;
        this.router = router;
        moduleMapping = new HashMap<>();
        messageQueueHandler = new ExecutorMessageQueue() {
            public boolean pushMessage(Routable message) {
                try {
                    return messageQueue.add(message);
                } catch (IllegalStateException e) { // No space in queue
                    return false;
                }
            }
        };
        Log.log(getName(), "Creating...");
    }

    /** Get executor name. */
    public String getName() {
        return name;
    }

    /** Attach given module to this executor and execute its messages. */
    public void attachModule(ExecutorGuestInterface module) {
        moduleMapping.put(module.getType(), module);
    }

    /** Returns interface to access back of message queue. */
    public ExecutorMessageQueue getMessageQueueHandler() {
        return messageQueueHandler;
    }

    /** Returns all guests modules running within this executor. */
    public Collection<ModuleType> getGuestsTypes() {
        return moduleMapping.keySet();
    }

    /**
     * Run executor.
     * 1. At the beginning run executor initializer and modules initializers.
     * 2. Enter main working loop. Dequeue next message, find module to which message is addressed
     * and run internal module dispatcher.
     * 3. At the end, when there is any serious reason to finish job, call #exit() method
     * on every module running on the executor.
     *
     * #init and #interruptHandler are separated to let children classes using
     * same run code, because access pattern is almost the same in all executors.
     * #init and #interruptHandler let to do additional actions on init and on exit.
     */
    @Override
    public void run() {
        init();
        initAttachedModules();
        loop();
        stopAttachedModules();
    }

    /** Default executor initializer. */
    protected void init() {
        Log.log(getName(), "Executor starting...");
        isRunning = true;
    }

    /** Initialize all hosting modules. */
    private void initAttachedModules() {
        for (ExecutorGuestInterface guest : moduleMapping.values())
            guest.init();
    }

    /** Just main loop. */
    private void loop() {
        while (isRunning) {
            try {
                Routable message = messageQueue.take();
                ExecutorGuestInterface module = moduleMapping.get(message.getTarget());
                if (module == null) {
                    // RouterInterface shouldn't make a mistake but..
                    Log.err(getName(), "Message delivery error! Received message to: " + message.getTarget().toString());
                    continue;
                }
                module.dispatch(message);
            } catch (InterruptedException e) {
                interruptHandler();
            }
        }
    }

    /** Let modules finish job. */
    private void stopAttachedModules() {
        for (ExecutorGuestInterface guest : moduleMapping.values())
            guest.exit();
    }

    /** Default thread's interrupt handler. */
    protected void interruptHandler() {
        Log.log(getName(), "Executor interrupted!");
        // In default Executor's interrupt handler just do nothing.
        // If Terminator would stop application, it will change #isRunning flag.
    }

    /** Sends message do executor which has destination module attached.
     * @param msg message to send
     * */
    public boolean sendMessage(Routable msg) {
        return router.route(msg);
    }

    /** Switch off the main loop and exit on next wakeup. */
    public void shutdown() {
        isRunning = false;
    }
}
