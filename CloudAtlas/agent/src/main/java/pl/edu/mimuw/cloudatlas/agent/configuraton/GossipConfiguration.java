/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.configuraton;

import org.ini4j.Profile;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.strategy.GossipStrategyType;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

public class GossipConfiguration extends ModuleConfiguration {
    private final GossipStrategyType strategy;

    private final int contactAttempts;

    private final long interval;

    GossipConfiguration(Profile.Section section) {
        String strategy = section.get("GOSSIP_STRATEGY");
        this.strategy = GossipStrategyType.valueOf(strategy);

        interval = section.get("GOSSIP_INTERVAL", Long.class);
        contactAttempts = section.get("CONTACT_ATTEMPTS", Integer.class);

        Log.log("GOSSIP_CONFIGURATION", toString());
    }

    @Override
    public String toString() {
        return String.format("strategy: %s, contactAttempts: %d, gossip-interval: %d ms", strategy.toString(), contactAttempts, interval);
    }

    public GossipStrategyType getStrategy() {
        return strategy;
    }

    public int getContactAttempts() {
        return contactAttempts;
    }

    public long getInterval() {
        return interval;
    }
}
