/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers;

import java.net.InetSocketAddress;

public class MessageFragment {
    private final SenderData senderData;

    private final int part;

    private final int parts;

    private final byte[] payload;

    private final TimeSample timeSample;

    public MessageFragment(InetSocketAddress socketAddress, int id, int part, int parts, byte[] bytes, TimeSample timeSample) {

        payload = bytes;
        this.parts = parts;
        senderData = new SenderData(socketAddress, id);
        this.part = part;
        this.timeSample = timeSample;
    }

    public TimeSample getTimeSample() {
        return timeSample;
    }

    public SenderData getSenderData() {
        return senderData;
    }

    public int getPart() {
        return part;
    }

    public int getParts() {
        return parts;
    }

    public byte[] getPayload() {
        return payload;
    }

    @Override
    public String toString() {
        return "[MessageFragment: (from: " + senderData + ", parts: " + part + " of " + parts + ", Tsmpl: " + timeSample +  ", payload-len:" + payload.length + ")]";
    }
}
