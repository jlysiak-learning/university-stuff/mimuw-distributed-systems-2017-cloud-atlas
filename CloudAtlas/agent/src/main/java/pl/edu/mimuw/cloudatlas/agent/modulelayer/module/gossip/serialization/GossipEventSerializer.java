/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.serialization;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.event.*;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.TimeSample;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query.QueryAttribute;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage.ZMIOffer;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage.ZMIUpdate;
import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.AttributesMap;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypeCollection;
import pl.edu.mimuw.cloudatlas.datamodel.value.*;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

import static java.lang.Integer.MAX_VALUE;

/**
 * Provides Kryo serialization.
 * Helps Kryo with guessing.
 */
public class GossipEventSerializer {
    private final Kryo kryo;

    public GossipEventSerializer() {
        kryo = new Kryo();
        kryo.register(Wrapper.class);
        kryo.register(GossipEvent.class);
        kryo.register(ExchangeQueryInit.class);
        kryo.register(ExchangeQueryAnswer.class);
        kryo.register(OfferZMIInit.class);
        kryo.register(OfferZmiAnswer.class);
        kryo.register(UpdateZmi.class);
        kryo.register(QueryAttribute.class);
        kryo.register(ZMIOffer.class);
        kryo.register(ZMIUpdate.class);
        kryo.register(TimeSample.class);
        kryo.register(ArrayList.class);
        kryo.register(LinkedList.class);
        kryo.register(Attribute.class);
        kryo.register(AttributesMap.class);
        kryo.register(PathName.class, new PathNameSerializer());
        kryo.register(HashMap.class);
        kryo.register(InetAddress.class);
        kryo.register(Inet4Address.class, new Inet4AddressSerializer());
        kryo.register(HashMap.class);
        kryo.register(ValueNull.class);
        kryo.register(ValueNull.class);
        kryo.register(ValueInt.class);
        kryo.register(ValueBoolean.class);
        kryo.register(ValueDouble.class);
        kryo.register(ValueDuration.class);
        kryo.register(ValueString.class);
        kryo.register(ValueTime.class);
        kryo.register(ValueContact.class);
        kryo.register(ValueList.class, new ValueListSerializer());
        kryo.register(ValueSet.class, new ValueSetSerializer());
    }

    // Wrapper helps Kryo to create abstract GossipEvent
    public GossipEvent getEvent(byte[] content) {
        return kryo.readObject(new Input(content), Wrapper.class).getEvent();
    }

    public byte[] getBytes(GossipEvent event) {
        Output output = new Output(1, MAX_VALUE);
        kryo.writeObject(output, new Wrapper(event));
        return output.toBytes();
    }

    private class PathNameSerializer extends Serializer<PathName> {

        @Override
        public void write(Kryo kryo, Output output, PathName pathName) {
            kryo.writeObject(output, pathName.getName()); // Trick, put just string
        }

        @Override
        public PathName read(Kryo kryo, Input input, Class<PathName> pathNameClass) {
            String str = kryo.readObject(input, String.class);
            return new PathName(str); // Create path name form string
        }
    }

    /** Serialize address just as four bytes */
    public class Inet4AddressSerializer extends Serializer<Inet4Address> {
        @Override
        public Inet4Address read(Kryo kryo, Input input, Class<Inet4Address> inet4AddressClass) {
            Serializer<byte[]> byteDefault = kryo.getDefaultSerializer(byte[].class);
            byte[] addr = byteDefault.read(kryo, input, byte[].class);
            try {
                return (Inet4Address) InetAddress.getByAddress(addr);
            } catch (UnknownHostException e) {
                throw new Error(e);
            }
        }

        @Override
        public void write(Kryo kryo, Output output, Inet4Address inet4Address) {
            Serializer<byte[]> byteDefault = kryo.getDefaultSerializer(byte[].class);
            byteDefault.write(kryo, output, inet4Address.getAddress());
        }

    }

    /** Put element type first, then content.
     * Without custom serializer list of null elements is kept in ZMI. */
    public class ValueListSerializer extends Serializer<ValueList> {
        @Override
        public ValueList read(Kryo kryo, Input input, Class<ValueList> valueListClass) {
            TypeCollection type = kryo.readObject(input, TypeCollection.class);
            return new ValueList(kryo.readObject(input, LinkedList.class), type.getElementType());

        }

        @Override
        public void write(Kryo kryo, Output output, ValueList valueList) {
            kryo.writeObject(output, valueList.getType());
            kryo.writeObject(output, valueList.getValue());
        }

    }

    /** Put element type first, then content.
     * Without custom serializer set of null elements is kept in ZMI.  */
    public class ValueSetSerializer extends Serializer<ValueSet> {
        @Override
        public ValueSet read(Kryo kryo, Input input, Class<ValueSet> valueSetClass) {
            TypeCollection type = kryo.readObject(input, TypeCollection.class);
            return new ValueSet(kryo.readObject(input, HashSet.class) , type.getElementType());
        }

        @Override
        public void write(Kryo kryo, Output output, ValueSet object) {
            kryo.writeObject(output, object.getType());
            kryo.writeObject(output, object.getValue());
        }

    }
}
