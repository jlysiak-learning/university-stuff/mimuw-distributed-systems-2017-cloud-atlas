/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers;

import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class MessageFragments {
    private final int parts;

    private final long expirationTime;

    private int counter = 0;

    private final ArrayList<byte[]> fragments;

    private TimeSample timeSample;

    public MessageFragments(int parts, long now, long receiveTimeout) {
        this.parts = parts;
        expirationTime = now + receiveTimeout;
        fragments = new ArrayList<byte[]>(Collections.nCopies(parts, (byte[]) null));
    }

    public long getExiprationTIme() {
        return expirationTime;
    }

    public void add(MessageFragment fragment) {
        fragments.set(fragment.getPart(), fragment.getPayload());
        timeSample = fragment.getTimeSample(); // Take last timesample
        counter++;
    }

    public boolean isDone() {
        return parts == counter;
    }

    public TimeSample getTimeSample() {
        return timeSample;
    }

    public byte[] merge() {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            for (byte[] bytes : fragments)
                outputStream.write(bytes);
        } catch (IOException e) {
            Log.fatal("MSG-FRAGMENTS", "Merge IO exception:" + e.getMessage());
        }
        return outputStream.toByteArray();
    }
}
