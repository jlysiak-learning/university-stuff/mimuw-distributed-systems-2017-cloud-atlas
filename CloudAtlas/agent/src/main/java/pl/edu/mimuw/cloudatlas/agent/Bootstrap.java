/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent;

import pl.edu.mimuw.cloudatlas.agent.configuraton.Configuration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.ConfigurationSectionNotFoundException;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.NoConfigurationException;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.executor.Executor;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.executor.TerminatorExecutor;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.router.Router;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.exception.NoSuchModuleTypeException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.exception.NotPermitedModuleCreationException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.exception.NullAsModuleTypeException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.Module;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.strategy.NoSuchSelectionStrategyException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.terminator.TerminatorModule;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import static java.lang.System.exit;

/**
 * CloudAtlas entry point.
 */
final class Bootstrap {
    private final static String name = "BOOT";

    /// Executor running in main thread
    private final TerminatorExecutor terminatorExecutor;

    /// Main thread handler
    private final Thread main;

    /**
     * Initialize system. Create modules, executors and threads.
     * ShutdownHook thread is also prepared. This lets system to exit cleanly.
     *
     * @param configuration system configuration
     */
    private Bootstrap(Configuration configuration) {
        Log.log(name, "Initialization...");

        main = Thread.currentThread();
        Router router = new Router();

        // Here we store executors which will be controlled by Terminator
        Collection<Executor> executors = new ArrayList<>();
        // Array of created threads
        Collection<Thread> threads = new ArrayList<>();

        Log.log(name, "Creating executors and modules...");
        for (ModuleType type : ModuleType.values()) {
            if (type == ModuleType.TERMINATOR || type == ModuleType.MAIN || type == ModuleType.SECURITY) continue;

            Executor e = new Executor("EXEC-" + type.toString(), router);
            executors.add(e);
            try {
                e.attachModule(Module.createModuleByType(type, e, configuration));
                router.addMapping(e.getGuestsTypes(), e.getMessageQueueHandler());
            } catch (NoConfigurationException | NotPermitedModuleCreationException | NullAsModuleTypeException
                    | NoSuchModuleTypeException | NoSuchSelectionStrategyException | Exception exception) {
                Log.fatal(name, "CloudAtlas agent initialization exception...");
                Log.fatal(name, "Error message: " + exception.getMessage());
                exit(1);
            }
        }

        Log.log(name, "Creating threads and attaching executors...");
        for (Executor executor : executors) {
            Thread t = new Thread(executor);
            t.setName(executor.getName());
            threads.add(t);
        }
        terminatorExecutor = new TerminatorExecutor(router, threads, executors);
        terminatorExecutor.attachModule(new TerminatorModule(terminatorExecutor, configuration));
        router.addMapping(terminatorExecutor.getGuestsTypes(), terminatorExecutor.getMessageQueueHandler());

        // Add clean shutdown
        // This thread catch interrupt signal and force main system thread running
        // Terminal Executor to finish it's job.
        Runtime.getRuntime().addShutdownHook(new Thread() {
            private final String name = "SHUTDOWN";

            @Override
            public void run() {
                Log.log(name, "Interrupt caught! Closing system...");
                try {
                    terminatorExecutor.shutdown();
                    main.interrupt();
                    main.join();
                } catch (InterruptedException ignored) {
                }
                Log.log(name, "Currently running threads: " + Integer.toString(Thread.activeCount()));
                Log.log(name, "System shutdown... NOW!");
            }
        });
    }

    /**
     * Start CloudAtlas agent.
     *
     * @param args CL arguments
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            Log.fatal(name, "Configuration file not provided!");
            exit(1);
        }

        Log.log(name, "************** CLOUDATLAS - DISTRIBUTED SYSTEM **************");
        try {
            Configuration config = new Configuration(args[0]);
            Bootstrap bootstrap = new Bootstrap(config);
            bootstrap.run();
            Log.log(name, "Main thread goes down...");
        } catch (IOException | ConfigurationSectionNotFoundException e) {
            Log.fatal(name, "CloudAtlas agent startup exception...");
            Log.fatal(name, "Error message: " + e.getMessage());
            exit(1);
        }
    }

    /**
     * Method pass control to Terminator executor which will be responsible for
     * shutting-down the system.
     * Terminator executor runs only Terminator module.
     */
    private void run() {
        terminatorExecutor.run();
    }
}
