/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.rmi;

import pl.edu.mimuw.cloudatlas.agent.configuraton.Configuration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.MainConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.RMIConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.ZmiConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.NoConfigurationException;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Content;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.GetRequest;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Handler;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Payload;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.Module;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.GossipAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.QueryAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.action.InstallQuery;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.action.UninstallQuery;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query.QueryAttribute;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.storage.QueryStorageCache;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.action.StartTickAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.ZMIAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.action.SetSingletonAttribute;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage.ZMIHierarchy;
import pl.edu.mimuw.cloudatlas.common.rmi.RMIServer;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;
import pl.edu.mimuw.cloudatlas.datamodel.ZMI;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueContact;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;

import java.util.ArrayList;
import java.util.List;

public class RMIModule extends Module {
    private ZMIHierarchy zmiCache;
    private final QueryStorageCache queryCache;

    private final RMIServer server;

    private final long cacheUpdateInterval;

    private final Handler<Payload> CACHE_UPDATE_INIT_HANDLER = new Handler<Payload>() {
        @Override
        public void handle(Payload payload) {
            sendUpdateRequests();
        }
    };

    private final Handler<Content<List<QueryAttribute>>> CACHE_UPDATE_QUERY_HANDLER = new Handler<Content<List<QueryAttribute>>>() {
        @Override
        public void handle(Content<List<QueryAttribute>> payload) {
            queryCache.updateCache(payload.getContent());
        }
    };

    private final Handler<Content<ZMIHierarchy>> CACHE_ZMI_UPDATE_HANDLER = new Handler<Content<ZMIHierarchy>>() {
        @Override
        public void handle(Content<ZMIHierarchy> payload) {
            zmiCache = payload.getContent();
        }
    };

    public RMIModule(ExecutorHostInterface hostInterface, Configuration configuration) throws NoConfigurationException {
        super(hostInterface, ModuleType.RMI, configuration);
        registerActionHandler(RMIAction.CACHE_UPDATE_INIT, CACHE_UPDATE_INIT_HANDLER);
        registerActionHandler(RMIAction.CACHE_UPDATE_ZMI, CACHE_ZMI_UPDATE_HANDLER);
        registerActionHandler(RMIAction.CACHE_UPDATE_QUERY, CACHE_UPDATE_QUERY_HANDLER);

        ZmiConfiguration zmiConfiguration = (ZmiConfiguration) configuration.getConfiguration(ModuleType.ZMI);
        cacheUpdateInterval = zmiConfiguration.getRefreshInterval() / 2;

        RMIConfiguration rmiConfiguration = (RMIConfiguration) configuration.getConfiguration(ModuleType.RMI);
        server = new RMIServer(rmiConfiguration.getRmiHost(), rmiConfiguration.getRmiPort(), new RMIExecutor(this));
        server.run();
        queryCache = new QueryStorageCache();

        MainConfiguration mainConfiguration = (MainConfiguration) configuration.getConfiguration(ModuleType.MAIN);
        zmiCache = ZMIHierarchy.fromNodeContact(mainConfiguration.getNodeContact());
    }

    @Override
    public void init() {
        send(ModuleType.TIMER, TimerAction.START_TICK, new StartTickAction(ModuleType.RMI, RMIAction.CACHE_UPDATE_INIT, cacheUpdateInterval, 1000, 0));
        sendUpdateRequests();
    }

    private void sendUpdateRequests() {
        send(ModuleType.ZMI, ZMIAction.GET_ZMIS, new GetRequest(ModuleType.RMI, RMIAction.CACHE_UPDATE_ZMI));
        send(ModuleType.QUERY, QueryAction.GET_QUERIES, new GetRequest(ModuleType.RMI, RMIAction.CACHE_UPDATE_QUERY));
    }

    public ZMI getCachedZmis() {
        return zmiCache.getRoot();
    }

    public void installQuery(Attribute queryAttr, String valueString, byte[] signature, long id) {
        List<QueryAttribute> list = new ArrayList<>();
        list.add(new QueryAttribute(valueString, queryAttr, id, signature, false));
        InstallQuery query = new InstallQuery(list);
        send(ModuleType.QUERY, QueryAction.INSTALL, query);
        Log.log(name, query.toString());
    }

    public void uninstallQuery(Attribute queryAttr, String query, byte[] signature, long id) {
        UninstallQuery uninstallQuery = new UninstallQuery(new QueryAttribute(query, queryAttr, id, signature, true));
        send(ModuleType.QUERY, QueryAction.UNINSTALL, uninstallQuery);
        Log.log(name, uninstallQuery.toString());
    }

    public void setMyAttribute(String name, Value value) {
        send(ModuleType.ZMI, ZMIAction.SET_SINGLETON_ATTRIBUTE, new SetSingletonAttribute(new Attribute(name), value));
    }

    public void setFallbackContacts(ValueList fallbackContacts) {
        List<ValueContact> contacts = new ArrayList<>();
        for (Value v: fallbackContacts.getValue()) {
            contacts.add((ValueContact) v);
        }
        send(ModuleType.GOSSIP, GossipAction.SET_FALLBACK_CONTACTS, new Content<>(contacts));
    }

    public String getMyPath() {
        return zmiCache.getMyPath().toString();
    }

    public List<QueryAttribute> getQueries() {
        return queryCache.showInstalled();
    }
}
