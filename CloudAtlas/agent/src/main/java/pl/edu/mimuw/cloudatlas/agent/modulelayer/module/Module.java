/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module;

import pl.edu.mimuw.cloudatlas.agent.configuraton.Configuration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.NoConfigurationException;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorGuestInterface;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.router.Routable;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.exception.NoSuchModuleTypeException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.exception.NotPermitedModuleCreationException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.exception.NullAsModuleTypeException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.*;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.GossipModule;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.strategy.NoSuchSelectionStrategyException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.NetworkModule;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.QueryModule;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.rmi.RMIModule;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.ZMIModule;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.util.HashMap;
import java.util.Map;

public abstract class Module implements ExecutorGuestInterface {
    protected final ExecutorHostInterface host;

    protected final String name;

    private final ModuleType type;

    private final Map<Integer, Handler> handlerMap;

    public Module(ExecutorHostInterface hostInterface, ModuleType type, Configuration configuration) {
        this.host = hostInterface;
        this.name = type.toString();
        this.type = type;
        handlerMap = new HashMap<>();
        Log.log(name, "Created...");
    }

    public static ExecutorGuestInterface createModuleByType(ModuleType type, ExecutorHostInterface e, Configuration configuration) throws NotPermitedModuleCreationException, NoConfigurationException, NoSuchModuleTypeException, NullAsModuleTypeException, NoSuchSelectionStrategyException, Exception {
        if (type == null) throw new NullAsModuleTypeException();

        switch (type) {
            case TIMER:
                return new TimerModule(e, configuration);
            case TERMINATOR:
                throw new NotPermitedModuleCreationException(ModuleType.TERMINATOR);
            case NETWORK:
                return new NetworkModule(e, configuration);
            case ZMI:
                return new ZMIModule(e, configuration);
            case GOSSIP:
                return new GossipModule(e, configuration);
            case MAIN:
                throw new NotPermitedModuleCreationException(type);
            case SECURITY:
                throw new NotPermitedModuleCreationException(type);
            case QUERY:
                return new QueryModule(e, configuration);
            case RMI:
                return new RMIModule(e, configuration);
            default:
                throw new NoSuchModuleTypeException(type);
        }
    }

    protected void registerActionHandler(int action, Handler handler) {
        handlerMap.put(action, handler);
    }

    @Override
    public void dispatch(Routable message) {
        MessageInterface msg = (MessageInterface) message;
        Handler handler = handlerMap.get(msg.getAction());
        if (handler != null) {
            try {
                handler.handlePayload(msg.getPayload());
            } catch (Exception e) {
                Log.err(name, String.format("Message handling exception for %s:\n %s", msg.toString(), e.getMessage()));
                e.printStackTrace(System.err);
            }
        } else {
            Log.err(name, String.format("No handler for: %s", msg.toString()));
        }
    }

    @Override
    /// Default init - do nothing
    public void init() {
    }

    @Override
    /// Default exit - do nothing
    public void exit() {
    }

    @Override
    public ModuleType getType() {
        return type;
    }

    /** Send message to other module. */
    protected boolean send(ModuleType to, int action, Payload payload) {
        Message msg = new Message(to, this.type, action, payload);
        return host.sendMessage(msg);
    }
}
