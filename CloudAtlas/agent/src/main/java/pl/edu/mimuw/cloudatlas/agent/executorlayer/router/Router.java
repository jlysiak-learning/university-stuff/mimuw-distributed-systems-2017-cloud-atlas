/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.executorlayer.router;

import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorMessageQueue;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Message router.
 * This router is held by Executors and is used by them to organize
 * message passing between them.
 * Router automatically routes non local messages to Network module.
 */
public final class Router implements RouterInterface {
    private final static String NAME = "ROUTER";

    /// Module -> Executor's message queue map
    private final Map<ModuleType, ExecutorMessageQueue> executorMapping;

    public Router() {
        Log.log(NAME, "Creating router...");
        executorMapping = new HashMap<>();
    }

    /**
     * Route messages to appropriate message queue.
     * Detects non locally addressed messages and pass them to
     * Network module.
     * @param message message to transfer
     * @return true if ok, false if failed
     */
    @Override
    public boolean route(Routable message) {
        return executorMapping.get(message.getTarget()).pushMessage(message);
    }

    /**
     * Map module types existing in agent to corresponding Executors
     * messages queues.
     * @param types collection of modules types attached to given executor
     * @param queue executor message queue
     */
    public void addMapping(Collection<ModuleType> types, ExecutorMessageQueue queue) {
        for (ModuleType type : types)
            executorMapping.put(type, queue);
    }
}
