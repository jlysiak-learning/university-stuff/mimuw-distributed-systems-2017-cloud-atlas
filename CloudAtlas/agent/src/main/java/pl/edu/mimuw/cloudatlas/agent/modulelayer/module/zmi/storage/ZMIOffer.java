/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage;

import pl.edu.mimuw.cloudatlas.datamodel.ZMI;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueInt;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueString;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueTime;

/**
 * Minimum (or almost minimum) of information needed to
 * exchange ZMIs timestamps.
 */
public class ZMIOffer {
    /// Zone nameHash
    private final int nameHash;

    /// Just extracted 'timestamp' attribute
    private final long timestamp;

    /// ZMI level in hierarchy
    private final byte level;

    public ZMIOffer(String name, long timestamp, byte level) {
        this.nameHash = name.hashCode();
        this.timestamp = timestamp;
        this.level = level;
    }

    public ZMIOffer(int nameHash, long timestamp, byte level) {
        this.nameHash = nameHash;
        this.timestamp = timestamp;
        this.level = level;
    }

    public ZMIOffer(ZMI zmi) {                      // _     ?  _
        // In real world hierarchy with 200 levels...   \(0,o)_/
        this.nameHash = ((ValueString) zmi.getAttributes().get("name")).getValue().hashCode();
        this.level = (byte) (((ValueInt) zmi.getAttributes().get("level")).getValue() & 0xff);
        this.timestamp = ((ValueTime) zmi.getAttributes().get("timestamp")).getValue();
    }

    /** @implNote Hidden private constructor for Kryo serialization. */
    private ZMIOffer() {
        nameHash = 0;
        timestamp = 0;
        level = 0;
    }

    public int getNameHash() {
        return nameHash;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public byte getLevel() {
        return level;
    }

    @Override
    public String toString() {
        return String.format("[ZMIOffer: (#name: %x, lvl: %d, ts: %d)]", nameHash, level, timestamp);
    }
}
