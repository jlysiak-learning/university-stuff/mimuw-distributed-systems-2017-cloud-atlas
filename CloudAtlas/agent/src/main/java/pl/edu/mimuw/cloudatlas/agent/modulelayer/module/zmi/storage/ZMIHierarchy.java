/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.zmi.storage;

import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.TimeSample;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.ZMI;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.AttributesMap;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.*;

import java.util.*;

public class ZMIHierarchy {

    private final ZMI root;

    private final ZMI singleton;

    private final List<ZMI> locals;

    private final List<HashMap<Integer, ZMI>> foreigners;

    private final PathName localPath;

    private ZMIHierarchy(ZMI root, ZMI singleton, List<ZMI> locals, List<HashMap<Integer, ZMI>> foreigners, PathName localPath) {
        this.root = root;
        this.singleton = singleton;
        this.locals = locals;
        this.foreigners = foreigners;
        this.localPath = localPath;
    }

    private ZMIHierarchy(ValueContact contact) {
        this.locals = new ArrayList<>();
        this.foreigners = new ArrayList<>();
        this.localPath = contact.getName();

        root = new ZMI();
        root.getAttributes().add(createBasicAttributeSet("", contact.getName().toString(), 0, TimerModule.now()));
        root.getAttributes().addOrChange("cardinality", new ValueInt(1L));
        locals.add(root);
        foreigners.add(new HashMap<>());

        PathName path = new PathName("");
        for (String component : localPath.getComponents()) {
            path = path.levelDown(component);
            ZMI zmi = new ZMI();
            zmi.getAttributes().add(createBasicAttributeSet(component, contact.getName().toString(), path.getDepth(), TimerModule.now()));
            zmi.getAttributes().addOrChange("cardinality", new ValueInt(1L));
            locals.add(zmi);
            foreigners.add(new HashMap<>());
        }
        singleton = locals.get(path.getDepth());
        List<Value> contactList = new ArrayList<>();
        contactList.add(contact);
        singleton.getAttributes().addOrChange("contacts", new ValueList(contactList, TypePrimitive.CONTACT));
        setupRelationships();
    }

    /**
     * Handy method to create basic set of attributes for each ZMI
     *
     * @param zoneName local zone name, full path component
     * @param owner    full path of ZMI creator (owner)
     * @param level    hierarchy level
     * @param now      time now
     * @return nice attributes map
     */
    public static AttributesMap createBasicAttributeSet(String zoneName, String owner, int level, long now) {
        AttributesMap map = new AttributesMap();
        map.add("owner", new ValueString(owner));
        map.add("level", new ValueInt((long) level));
        map.add("name", new ValueString(zoneName));
        map.add("cardinality", new ValueInt(0L));
        map.add("timestamp", new ValueTime(now));
        map.add("contacts", new ValueList(TypePrimitive.CONTACT));
        return map;
    }

    /**
     * Set relations in ZMI tree.
     */
    private void setupRelationships() {
        for (int i = 0; i < locals.size() - 1; i++) {
            ZMI parent = locals.get(i);
            ZMI son = locals.get(i + 1);
            parent.addSon(son);
            son.setFather(parent);
            for (Map.Entry<Integer, ZMI> entry : foreigners.get(i + 1).entrySet()) {
                parent.addSon(entry.getValue());
                entry.getValue().setFather(parent);
            }
        }
    }

    public static ZMIHierarchy fromNodeContact(ValueContact contact) {
        return new ZMIHierarchy(contact);
    }

    /**
     * Set attributes of singleton zone and update timestamp.
     *
     * @param attribute name
     * @param value     value
     */
    public void setSingletonAttribute(Attribute attribute, Value value) {
        singleton.getAttributes().addOrChange(attribute, value);
        singleton.getAttributes().addOrChange("timestamp", new ValueTime(TimerModule.now()));
    }

    public ZMI getRoot() {
        return root;
    }

    public ZMI getSingleton() {
        return singleton;
    }

    /**
     * Iterate over all levels of siblings are remove ZMIs
     * which exceeded given time.
     *
     * @param now            time now
     * @param expirationTime ZMI timeout
     */
    public void removeZmisOlderThan(long now, long expirationTime) {
        for (HashMap<Integer, ZMI> level : foreigners) {
            for (Iterator<Map.Entry<Integer, ZMI>> it = level.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<Integer, ZMI> entry = it.next();
                ZMI son = entry.getValue();
                ZMI parent = son.getFather();
                if (son.getTimestamp() + expirationTime < now) {
                    son.setFather(null);
                    parent.removeSon(son);
                }
                it.remove();
            }
        }
    }

    /**
     * Get short form of ZMIs stored on agent, which could be interesting for our gossip partner.
     * It's used by cache in #GossipModule.
     *
     * @param partnerPath partner full path
     * @return list of ZMI offers with our timestamps
     */
    public List<ZMIOffer> prepareZmiOffer(PathName partnerPath) {
        PathName commonPath = getCommonPrefix(localPath, partnerPath);
        ArrayList<ZMIOffer> offer = new ArrayList<>();
        if (commonPath.getDepth() == localPath.getDepth()) // By mistake someone can put wrong address...
            return offer;
        for (int i = 0; i < commonPath.getDepth(); i++) {
            for (ZMI foreign : foreigners.get(i + 1).values())
                offer.add(new ZMIOffer(foreign));
        }
        // Common path ended, put last layer
        // Add ZMI on our local path after fork
        offer.add(new ZMIOffer(locals.get(commonPath.getDepth() + 1))); // Put
        // Take first partner's path component after fork
        String firstDiff = partnerPath.getComponents().get(commonPath.getDepth());
        // Add all foreign ZMI's but omit one on partner's local path
        for (ZMI foreign : foreigners.get(commonPath.getDepth()).values()) {
            if (!foreign.getLocalName().equals(firstDiff)) offer.add(new ZMIOffer(foreign));
        }
        return offer;
    }

    /**
     * Iterates along two paths and gets common prefix of both.
     *
     * @param p1 path one
     * @param p2 path two
     * @return common prefix path
     */
    private static PathName getCommonPrefix(PathName p1, PathName p2) {
        PathName l, s;
        if (p1.getDepth() > p2.getDepth()) {
            l = p1;
            s = p2;
        } else {
            l = p2;
            s = p1;
        }
        List<String> cl = l.getComponents();
        List<String> cs = s.getComponents();
        PathName r = new PathName("");
        for (int i = 0; i < cs.size(); i++) {
            if (!cs.get(i).equals(cl.get(i))) break;
            r = r.levelDown(cs.get(i));
        }
        return r;
    }

    /**
     * Prepare full ZMI updates. Updates are prepared by means of negative selection.
     * Received offers what's what we can received from partner, so we select those ZMI
     * which are fresher or cannot be provided by our partner.
     * All offers has foreign timestamps! Conversion required!
     *
     * @param recvZmiOffers received ZMI offers with partner's timestamps
     * @param fromHere
     * @param toHere
     * @param partnersPath
     * @return list of ZMI update requests
     */
    public List<ZMIUpdate> prepareZmiUpdates(List<ZMIOffer> recvZmiOffers, TimeSample fromHere, TimeSample toHere, PathName partnersPath) {
        PathName commonPath = getCommonPrefix(localPath, partnersPath);
        if (commonPath.getDepth() == localPath.getDepth()) // By mistake someone can put wrong address...
            return new LinkedList<>();
        int uptoDepth = commonPath.getDepth() + 1;
        String firstPathDiff = partnersPath.getComponents().get(commonPath.getDepth());
        ZMIHierarchy temp = this.clone();
        List<ZMIOffer> offersConverted = convertOffersTimestamps(recvZmiOffers, fromHere, toHere);
        return prepareZmiUpdates(temp, offersConverted, uptoDepth, firstPathDiff);
    }

    public ZMIHierarchy clone() {
        return cloneFromTree(root, localPath);
    }

    private static List<ZMIOffer> convertOffersTimestamps(List<ZMIOffer> recvZmiOffers, TimeSample fromHere, TimeSample toHere) {
        ArrayList<ZMIOffer> updated = new ArrayList<>();
        for (ZMIOffer offer : recvZmiOffers) {
            long localTime = TimerModule.toLocalTime(offer.getTimestamp(), fromHere, toHere);
            updated.add(new ZMIOffer(offer.getNameHash(), localTime, offer.getLevel()));
        }
        return updated;
    }

    /**
     * Generates ZMI updates based upon local ZMIs hierarchy and converted offers received.
     * #hierarchy parameter MUST be clone of local cache, cause not necessary ZMIs are deleted
     * in this call.
     *
     * @param hierarchy         deep cloned ZMI hierarchy
     * @param offers            offers converted to localtime
     * @param uptoDepth         max depth to generate updates
     * @param lastLvlZoneToOmit name of ZMI to ommit at last level thus it will be recomputed locally
     * @return generated full ZMI updates
     */
    private static List<ZMIUpdate> prepareZmiUpdates(ZMIHierarchy hierarchy, List<ZMIOffer> offers, int uptoDepth, String lastLvlZoneToOmit) {
        for (ZMIOffer offer : offers)
            hierarchy.checkOffer(offer);
        return hierarchy.generateUpdates(uptoDepth, lastLvlZoneToOmit);
    }

    public static ZMIHierarchy cloneFromTree(ZMI tree, PathName localPath) {
        ZMI newRoot = tree.clone();
        return fromTree(newRoot, localPath);
    }

    /**
     * Check received offer and remove corresponding ZMI from hierarchy
     * if partner's offer is pretty good.
     * We don't want to send ZMI which will rejected.
     *
     * @param offer received offer
     */
    private void checkOffer(ZMIOffer offer) {
        byte lvl = offer.getLevel();
        int hash = offer.getNameHash();
        long time = offer.getTimestamp();
        HashMap<Integer, ZMI> map = foreigners.get(lvl);
        ZMI zmi = map.get(hash);
        if (zmi == null)
            // Ok, this ZMI will be send latter by partner because we don't have it in storage
            return;

        // Now, such ZMI is present in our storage.
        // If we can receive fresher ZMI, remove this it from update request
        if (zmi.getTimestamp() < offer.getTimestamp()) {
            map.remove(zmi);
            zmi.getFather().removeSon(zmi);
        }
    }

    /**
     * After offers check, we can send all others ZMIs.
     * Omit also partners local path ZMIs which will be
     * recomputed on it's own.
     * It operates on cloned ZMI structures.
     *
     * @return ZMI updates
     */
    private ArrayList<ZMIUpdate> generateUpdates(int uptoDepth, String lastLvlZoneToOmit) {
        ArrayList<ZMIUpdate> updates = new ArrayList<>();
        for (int i = 1; i < uptoDepth; i++) {
            for (ZMI zmi : foreigners.get(i).values())
                updates.add(new ZMIUpdate(zmi));
        }
        updates.add(new ZMIUpdate(locals.get(uptoDepth)));
        for (ZMI zmi : foreigners.get(uptoDepth).values())
            if (!zmi.getLocalName().equals(lastLvlZoneToOmit)) updates.add(new ZMIUpdate(zmi));
        return updates;
    }

    /** Create new ZMI hierarchy from existing tree and given local path. */
    public static ZMIHierarchy fromTree(ZMI tree, PathName localPath) {
        ZMI parent = tree;
        ZMI son = null;
        ArrayList<ZMI> locals = new ArrayList<>();
        locals.add(tree);
        ArrayList<HashMap<Integer, ZMI>> forigners = new ArrayList<>();
        forigners.add(new HashMap<>());
        for (String component : localPath.getComponents()) {
            HashMap<Integer, ZMI> forignersAtLevel = new HashMap<>();
            son = collectFromLevel(parent, component, locals, forignersAtLevel);
            forigners.add(forignersAtLevel);
            parent = son;
        }
        return new ZMIHierarchy(tree, parent, locals, forigners, localPath);
    }

    /**
     * Collect sons ZMIs at given level.
     *
     * @param zmi              parent ZMI
     * @param localSon         local son name
     * @param locals           local path ZMIs list
     * @param forignersAtLevel siblings #map
     * @return local son's ZMI
     */
    private static ZMI collectFromLevel(ZMI zmi, String localSon, List<ZMI> locals, HashMap<Integer, ZMI> forignersAtLevel) {
        ZMI localZMI = null;
        for (ZMI son : zmi.getSons()) {
            if (son.getLocalName().equals(localSon)) {
                locals.add(son);
                localZMI = son;
            } else {
                forignersAtLevel.put(son.getLocalName().hashCode(), son);
            }
        }
        return localZMI;
    }

    public PathName getMyPath() {
        return localPath;
    }

    /**
     * Try to apply update in hierarchy.
     * @param fromHere from here time sample
     * @param toHere to here time sample
     * @param zmiUpdate gossiped ZMI update
     */
    public void updateNonLocalZmi(TimeSample fromHere, TimeSample toHere, ZMIUpdate zmiUpdate) {
        ZMI zmi = zmiUpdate.getZmi();
        long localTime = TimerModule.toLocalTime(((ValueTime) zmi.getAttributes().get("timestamp")).getValue(), fromHere, toHere);
        zmi.getAttributes().addOrChange("timestamp", new ValueTime(localTime));
        int lvl = ((ValueInt) zmi.getAttributes().get("level")).getValue().intValue();
        int hash = zmi.getLocalName().hashCode();
        ZMI parent = locals.get(lvl - 1);
        Map<Integer, ZMI> map = foreigners.get(lvl);
        ZMI existing = map.get(hash);
        if (existing == null) {
            map.put(hash, zmi);
            zmi.setFather(parent);
            parent.addSon(zmi);
        } else {
            if (existing.getTimestamp() < zmi.getTimestamp()) {
                // Remove old and set new content
                existing.getAttributes().clear();
                existing.getAttributes().add(zmi.getAttributes());
            }

        }

    }

    public int getMyLevel() {
        return singleton.getLevel();
    }

    /** Collect all list of contacts on all levels from all sibling zones. */
    public List<List<ValueContact>> getContactsStructure() {
        List<List<ValueContact>> contacts = new ArrayList<>();
        contacts.add(new ArrayList<>());
        for (int i = 1; i < locals.size(); i++) {
            List<ValueContact> list = new ArrayList<>();
            for (ZMI zmi: foreigners.get(i).values()) {
                ValueList valueList = (ValueList) zmi.getAttributes().get("contacts");
                for (Value v : valueList.getValue()) {
                    ValueContact contact = (ValueContact) v;
                    if (!contact.getName().equals(localPath)) {
                        list.add(contact);
                    }
                }
            }
            contacts.add(list);
        }
        return contacts;
    }
}

