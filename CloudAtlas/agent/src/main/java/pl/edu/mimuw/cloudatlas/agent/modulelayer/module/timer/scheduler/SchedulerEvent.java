/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.scheduler;

import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.action.StartTickAction;

public final class SchedulerEvent implements Comparable<SchedulerEvent> {
    private final ModuleType whom;

    private final int action;

    private final long interval;

    private long scheduledTime;

    private long countLeft;

    public SchedulerEvent(StartTickAction request) {
        scheduledTime = request.getDelay() + TimerModule.now();
        interval = request.getInterval();
        countLeft = request.getCount();
        action = request.getAction();
        whom = request.getWhom();
    }

    @Override
    public int compareTo(SchedulerEvent event) {
        long time = event.getScheduledTime();
        return Long.compare(scheduledTime, time);
    }

    public ModuleType getWhom() {
        return whom;
    }

    public int getAction() {
        return action;
    }

    public long getInterval() {
        return interval;
    }

    public long getCountLeft() {
        return countLeft;
    }

    public long getScheduledTime() {
        return scheduledTime;
    }

    public boolean next() {
        scheduledTime = scheduledTime + interval;
        if (countLeft > 0) {
            countLeft--;
            return countLeft != 0;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SCHEDULER EVENT: [whom: " + whom + ", action: " + action + ", interval: " + interval + ", scheduled-time: " + scheduledTime + ", count-left: " + countLeft + ']';
    }
}
