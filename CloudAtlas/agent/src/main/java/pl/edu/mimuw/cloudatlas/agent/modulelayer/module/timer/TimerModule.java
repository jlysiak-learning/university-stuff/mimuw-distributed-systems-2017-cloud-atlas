/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer;

import pl.edu.mimuw.cloudatlas.agent.configuraton.Configuration;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Handler;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.Module;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.TimeSample;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.action.StartTickAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.scheduler.Scheduler;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.scheduler.SchedulerEvent;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.util.PriorityQueue;

import static pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType.TIMER;

/**
 * Timer module provide time scheduling of messages.
 * What's guaranteed time of message dequeueing is greater than scheduled time.
 */
public class TimerModule extends Module {
    private final Scheduler scheduler;

    private final PriorityQueue<SchedulerEvent> schedulerQueue;

    private final Handler<StartTickAction> START_TICK_HANDLER = new Handler<StartTickAction>() {
        @Override
        public void handle(StartTickAction payload) {
            SchedulerEvent event = new SchedulerEvent(payload);
            synchronized (schedulerQueue) {
                /// @notify Event-driven share-nothing architecture violation
                schedulerQueue.add(event);
                schedulerQueue.notify();
            }
            Log.log(name, "Tick started " + event.toString());
        }
    };

    public TimerModule(ExecutorHostInterface hostInterface, Configuration configuration) {
        super(hostInterface, TIMER, configuration);

        registerActionHandler(TimerAction.START_TICK, START_TICK_HANDLER);

        schedulerQueue = new PriorityQueue<>();
        // Create and run Scheduler helper thread
        scheduler = new Scheduler(hostInterface, schedulerQueue);
    }

    /** Helper method to easily get actual time in millis. */
    public static long now() {
        return System.currentTimeMillis();
    }

    /**
     * Converts foreign timestamp to local time.
     *
     * @param foreignTime foreign time to convert
     * @param fromHere    old timesample
     * @param toHere      new timesample
     * @return event timestamp in our time local
     */
    public static long toLocalTime(long foreignTime, TimeSample fromHere, TimeSample toHere) {
        double oneWayTripTime = 0.5 * ((toHere.getRecvTime() - fromHere.getSendTime()) - (toHere.getSendTime() - fromHere.getRecvTime()));
        double offset = toHere.getRecvTime() - toHere.getSendTime() - oneWayTripTime;
        return (long) (foreignTime + offset);
    }

    @Override
    public void init() {
        // Start helper thread when system framework is already created
        scheduler.start();
    }

    @Override
    public void exit() {
        try {
            scheduler.shutdown();
            scheduler.join();
        } catch (InterruptedException e) {
            Log.wrn(this.name, "Interrupted during stopping scheduler.");
        }
    }
}
