/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.scheduler;

import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Message;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Payload;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.util.PriorityQueue;

/**
 * Scheduler thread manages time ordered priority queue.
 * It takes the earliest event form the queue when event's schedule time is reached.
 */
public class Scheduler extends Thread {
    private final PriorityQueue<SchedulerEvent> schedulerQueue;

    private final ExecutorHostInterface timerQueue;

    private boolean isRunning;

    public Scheduler(ExecutorHostInterface timerQueue, PriorityQueue<SchedulerEvent> schedulerQueue) {
        this.timerQueue = timerQueue;
        this.schedulerQueue = schedulerQueue;
        this.isRunning = true;
        setName("SCHEDULER");
    }

    /**
     * Empty queue and stop scheduler.
     * Method called by Timer module.
     * It's assumed that no message will be pushed after that
     * and scheduler queue is empty.
     */
    public void shutdown() {
        isRunning = false;
        synchronized (schedulerQueue) {
            schedulerQueue.clear();
            schedulerQueue.notify();
        }
    }

    @Override
    public void run() {
        Log.log(getName(), "Scheduler is running...");
        synchronized (schedulerQueue) {
            while (true) {
                while (schedulerQueue.isEmpty()) {
                    try {
                        schedulerQueue.wait();
                    } catch (InterruptedException ignored) {
                        // This exception is thrown only on nasty interrupt.
                        // It must not be used in normal situation
                    }
                    // There are two possible scenarios after notify() wakeup:
                    //  1. Normal operation: notify() on queue modification, #isRunning = true
                    //  2. Shutdown case: notify() to wakeup and exit, #isRunning = false
                    if (!isRunning)
                    {
                        Log.wrn(getName(), "Empty exit!");
                        return;
                    }
                }
                SchedulerEvent event = schedulerQueue.peek();
                long now = TimerModule.now();
                long scheduled = event.getScheduledTime();
                if (scheduled > now) {
                    try {
                        // Sleep until schedule time comes
                        schedulerQueue.wait(scheduled - now);
                    } catch (InterruptedException ignored) {
                        // Nasty interrupt again...
                    }
                    if (!isRunning) {
                        Log.wrn(getName(), "Waiting exit!");
                        return;
                    }
                    continue;
                }
                event = schedulerQueue.poll();
//                Log.log(getName(), "Send: " + event.toString());
                timerQueue.sendMessage(new Message(event.getWhom(), ModuleType.TIMER, event.getAction(), new Payload()));
                if (event.next()) schedulerQueue.add(event);
            }
        }

    }
}
