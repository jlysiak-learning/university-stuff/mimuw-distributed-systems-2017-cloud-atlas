/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.configuraton;

import org.ini4j.Profile;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;
import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueContact;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class MainConfiguration extends ModuleConfiguration {
    private final static String name = "MAIN_CONFIGURATION";

    private final String fullPath;

    private final ValueList fallbackContacts;

    private final InetAddress nodeAddress;

    private final ValueContact nodeContact;

    MainConfiguration(Profile.Section section) throws UnknownHostException {
        fullPath = section.get("PATH");
        nodeAddress = InetAddress.getByName(section.get("NODE_CONTACT"));
        nodeContact = new ValueContact(new PathName(fullPath), nodeAddress);
        String contactString = section.get("CONTACTS");
        fallbackContacts = parseContactString(contactString);
        Log.log(name, toString());
    }

    private ValueList parseContactString(String contactString) {
        String[] contacts = contactString.split(";");
        ValueList contactSet = new ValueList(TypePrimitive.CONTACT);

        for (String contact : contacts) {
            try {
                String[] pair = contact.split(":");
                PathName path = new PathName(pair[0]);
                InetAddress address = InetAddress.getByName(pair[1]);
                ValueContact fallbackContact = new ValueContact(path, address);
                contactSet.add(fallbackContact);
            } catch (UnknownHostException ignored) {}
        }
        return contactSet;
    }

    public String toString() {
        return String.format("path: %s, contacts: %s", fullPath, fallbackContacts.toString());
    }

    private byte[] checkIPv4(String[] values) {
        byte[] bytes = new byte[4];
        if (values.length != 4) return null;
        int i = 0;
        for (String val : values) {
            try {
                byte b = Byte.parseByte(val);
                bytes[i++] = b;
            } catch (NumberFormatException e) {
                Log.err(name, "Number format exception for " + values);
                return null;
            }
        }
        return bytes;
    }

    public InetAddress getNodeAddress() {
        return nodeAddress;
    }

    public ValueContact getNodeContact() {
        return nodeContact;
    }

    public ValueList getFallbackContacts() {
        return fallbackContacts;
    }

    public String getFullPath() {
        return fullPath;
    }
}
