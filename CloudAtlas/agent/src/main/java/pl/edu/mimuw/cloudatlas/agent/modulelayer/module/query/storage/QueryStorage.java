/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.storage;

import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query.Query;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.query.query.QueryAttribute;
import pl.edu.mimuw.cloudatlas.common.security.Hash;
import pl.edu.mimuw.cloudatlas.common.security.SignerCipher;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.service.interpreter.InterpreterService;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.query.Absyn.Program;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.*;

public class QueryStorage {
    private final static String LOGNAME = "QUERY-STORAGE";

    private final List<Query> predefinedQueries;

    private final Map<Attribute, Query> queries;

    private final Cipher queryVerifier;

    public QueryStorage(PublicKey publicKey) throws Exception {
        // Add predefined queries
        predefinedQueries = new ArrayList<>();
        predefinedQueries.add(generatePredefinedQuery("&cardinality", "SELECT sum(cardinality) AS cardinality"));
        predefinedQueries.add(generatePredefinedQuery("&contacts", "SELECT to_list(random(5,unfold(contacts))) AS contacts"));

        queries = new HashMap<>();
        queryVerifier = SignerCipher.getVerifier(publicKey);
    }

    /** Generate query program to let CloudAtlas work correctly. */
    private Query generatePredefinedQuery(String name, String query) throws Exception {
        return new Query(new Attribute(name), query, InterpreterService.parseQueryProgram(query), new byte[0], -1, false);
    }

    public List<Query> getPredefinedQueries() {
        return predefinedQueries;
    }

    /**
     * Install new query.
     * Make update in storage.
     * This method also makes uninstallation.
     */
    public boolean installQuery(QueryAttribute query) {
        if (!verifyQuery(query)) return false;
        // Prepare query
        try {
            Query newQuery = fromQueryAttribute(query);
            Query stored = queries.get(query.getAttribute());
            if (stored == null) {
                queries.put(newQuery.getName(), newQuery);
                return true;
            } else {
                if (stored.getId() < newQuery.getId()) {
                    queries.put(newQuery.getName(), newQuery);
                    return true;
                }
            }
        } catch (Exception e) {
            Log.err(LOGNAME, "Parsing problem for: " + query);
        }
        return false;
    }

    /** Verify query attribute. */
    private boolean verifyQuery(QueryAttribute query) {
        try {
            byte[] descriptor = query.getDescriptor();
            byte[] descriptorHash = Hash.hashMe(descriptor);
            byte[] signatureDec = queryVerifier.doFinal(query.getSignature());
            if (!Arrays.equals(descriptorHash, signatureDec)) {
                Log.err(LOGNAME, "Signature of " + query.toString() + " invalid!");
                return false;
            }
        } catch (IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException e) {
            Log.err(LOGNAME, "Installation of " + query.toString() + "fail: " + e.getMessage());
            return false;
        }
        return true;
    }

    private static Query fromQueryAttribute(QueryAttribute query) throws Exception {
        Program program = query.isDeleted() ? null : InterpreterService.parseQueryProgram(query.getQueryString());
        return new Query(query.getAttribute(), query.getQueryString(), program, query.getSignature(), query.getId(), query.isDeleted());
    }

    public List<QueryAttribute> dumpQueries() {
        List<QueryAttribute> result = new ArrayList<>();
        for (Query q : queries.values())
            result.add(fromQuery(q));
        return result;
    }

    private static QueryAttribute fromQuery(Query q) {
        return new QueryAttribute(q.getQuery(), q.getName(), q.getId(), q.getSignature(), q.isDeleted());
    }

    /** Get not deleted, executable queries. */
    public List<Query> getQueries() {
        List<Query> result = new ArrayList<>();
        for (Query q : queries.values())
            if (!q.isDeleted()) result.add(q);
        return result;
    }
}
