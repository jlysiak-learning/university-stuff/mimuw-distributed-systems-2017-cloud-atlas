/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.configuraton;

import org.ini4j.Wini;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.ConfigurationSectionNotFoundException;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.NoConfigurationException;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Configuration {
    private final Map<ModuleType, ModuleConfiguration> configurationMap;

    public Configuration(String configFile) throws IOException, ConfigurationSectionNotFoundException {
        configurationMap = new HashMap<>();
        Log.log("CONFIGURATION", "Reading " + configFile);

        Wini ini = new Wini(new File(configFile));
        String section;

        section = "MAIN";
        if (!ini.containsKey(section)) throw new ConfigurationSectionNotFoundException(section);
        configurationMap.put(ModuleType.MAIN, new MainConfiguration(ini.get(section)));

        section = "NETWORK";
        if (!ini.containsKey(section)) throw new ConfigurationSectionNotFoundException(section);
        configurationMap.put(ModuleType.NETWORK, new NetworkConfiguration(ini.get(section)));

        section = "RMI";
        if (!ini.containsKey(section)) throw new ConfigurationSectionNotFoundException(section);
        configurationMap.put(ModuleType.RMI, new RMIConfiguration(ini.get(section)));

        section = "GOSSIP";
        if (!ini.containsKey(section)) throw new ConfigurationSectionNotFoundException(section);
        configurationMap.put(ModuleType.GOSSIP, new GossipConfiguration(ini.get(section)));

        section = "ZMI";
        if (!ini.containsKey(section)) throw new ConfigurationSectionNotFoundException(section);
        configurationMap.put(ModuleType.ZMI, new ZmiConfiguration(ini.get(section)));

        section = "SECURITY";
        if (!ini.containsKey(section)) throw new ConfigurationSectionNotFoundException(section);
        configurationMap.put(ModuleType.SECURITY, new SecurityConfiguration(ini.get(section)));
    }

    public ModuleConfiguration getConfiguration(ModuleType type) throws NoConfigurationException {
        ModuleConfiguration configuration = configurationMap.get(type);
        if (configuration == null) throw new NoConfigurationException(type);
        return configuration;
    }
}
