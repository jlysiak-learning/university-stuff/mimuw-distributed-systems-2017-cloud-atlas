/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network;

import pl.edu.mimuw.cloudatlas.agent.configuraton.Configuration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.NetworkConfiguration;
import pl.edu.mimuw.cloudatlas.agent.configuraton.exception.NoConfigurationException;
import pl.edu.mimuw.cloudatlas.agent.executorlayer.ExecutorHostInterface;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Content;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Handler;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.message.Payload;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.Module;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.ModuleType;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.GossipAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.gossip.action.InetMessageReceived;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.action.InetMessageRequest;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.MessageFragment;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.MessageFragments;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.SendRequest;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.network.helpers.SenderData;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerAction;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.TimerModule;
import pl.edu.mimuw.cloudatlas.agent.modulelayer.module.timer.action.StartTickAction;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Network module.
 * Handles communication over Internet.
 * Automatically sends/receives messages to remote agents.
 *
 * It uses two helper thread to send and receive data via UDP sockets.
 */
public class NetworkModule extends Module {

    private final BlockingQueue<SendRequest> sndQueue;

    private final UDPSender sndThread;

    private final UDPReceiver recvThread;

    private final long receiveTimeout; //!< Timeout to collect all message fragments

    private final long cleanupInterval; //!< Time between cleaning up datagram cache

    private final int udpMaxSize; //!< Max size of data in datagram

    private final int agentPort; //!< CloudAtlas agent listening port

    private final Map<SenderData, MessageFragments> cache; //!< Incoming messages

    // Iterate over cached datagram fragments and remove expired or done messages.
    private final Handler<Payload> CLEANUP_HANDLER = new Handler<Payload>() {
        @Override
        public void handle(Payload payload) {
            cache.entrySet().removeIf(entry -> entry.getValue().getExiprationTIme() > TimerModule.now() || entry.getValue().isDone());
        }
    };

    private final Handler<Content<MessageFragment>> DATAGRAM_RECEIVED_HANDLER = new Handler<Content<MessageFragment>>() {
        @Override
        public void handle(Content<MessageFragment> payload) {
            MessageFragment fragment = payload.getContent();
            SenderData senderData = fragment.getSenderData();
            MessageFragments fragments = cache.get(senderData);
            if (fragments == null) {
                fragments = new MessageFragments(fragment.getParts(), TimerModule.now(), receiveTimeout);
                cache.put(senderData, fragments);
            }
//            Log.log(name, "Recv: " + fragment.toString());
            fragments.add(fragment);
            if (!fragments.isDone()) return;
            byte[] content = fragments.merge();
            send(ModuleType.GOSSIP, GossipAction.INET_MSG_RECV, new InetMessageReceived(content, senderData.getSocketAddress().getAddress(), fragments.getTimeSample()));
        }
    };

    private int msgId = 0;

    private final Handler<InetMessageRequest> SEND_MESSAGE_HANDLER = new Handler<InetMessageRequest>() {
        @Override
        public void handle(InetMessageRequest payload) {
            SendRequest sendRequest = new SendRequest(new InetSocketAddress(payload.getTargetAddress(), agentPort), getMsgId(), payload.getContent());
            sndQueue.add(sendRequest);
        }
    };

    public NetworkModule(ExecutorHostInterface hostInterface, Configuration configuration) throws NoConfigurationException {
        super(hostInterface, ModuleType.NETWORK, configuration);
        NetworkConfiguration networkConfiguration = (NetworkConfiguration) configuration.getConfiguration(getType());
        receiveTimeout = networkConfiguration.getUdpReceiverTimeout();
        cleanupInterval = receiveTimeout / 4;
        udpMaxSize = networkConfiguration.getUdpMaxSize();
        agentPort = networkConfiguration.getAgentPort();

        cache = new HashMap<SenderData, MessageFragments>();
        sndQueue = new LinkedBlockingQueue<>();
        registerActionHandler(NetworkAction.CLEANUP, CLEANUP_HANDLER);
        registerActionHandler(NetworkAction.SEND_MESSAGE, SEND_MESSAGE_HANDLER);
        registerActionHandler(NetworkAction._DATAGRAM_RECEIVED, DATAGRAM_RECEIVED_HANDLER);

        // Create internal helper threads
        sndThread = new UDPSender(sndQueue, hostInterface, udpMaxSize);
        recvThread = new UDPReceiver(hostInterface, agentPort, udpMaxSize);
    }

    private int getMsgId() {
        return msgId++;
    }

    @Override
    public void init() {
        send(ModuleType.TIMER, TimerAction.START_TICK, new StartTickAction(getType(), NetworkAction.CLEANUP, cleanupInterval, 1000, 0));
        // Start helper threads when all system configuration is initialized
        sndThread.start();
        recvThread.start();
    }

    @Override
    public void exit() {
        try {
            Log.log(name, "Exiting...");
            sndThread.shutdown();
            Log.log(name, "UDPSender interrupted...");
            recvThread.shutdown();
            Log.log(name, "UDPReceiver interrupted...");
            sndThread.join();
            recvThread.join();
            Log.log(name, "Helper threads stopped...");
        } catch (InterruptedException e) {
            Log.err(name, "Interrupted exception: " + e.getMessage());
        }
    }
}
