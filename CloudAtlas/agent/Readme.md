# CloudAtlas
## Agent

Main service provided by CloudAtlas system.  
Agents communicate with each other and exchange data using gossiping.  
For more details see paper about Astrolabe system.

### Usage

Agent starts with RMI registry as a daemon and stores its logs in dedicated directory (`logs/<profile name>`).  
To handle many targets, configuration profiles were implemented.
All configurable parameters can be chosen during profile creation.

Agent should be managed using provided scripts:
- `create-profile [profile name]` - new configuration profile setup
- `rm-profile <profile name>` - delete given profile
- `run <profile name>` - start RMI and agent using given profile, creates `.$hostname-lock` file with daemons' PIDs
- `stop` - kills started services, removes `.$hostname-lock`
- `clean [all]` - removes log files from `logs/<profile name>` for all profiles, `all` option lets to delete all stored profiles also

