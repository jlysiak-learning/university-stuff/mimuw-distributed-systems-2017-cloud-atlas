# CloudAtlas 
## Distributed Systems @ MIMUW Fall 2017

Jacek Łysiak - jacek.lysiako.o@gmail.com

Course assignment project.
Distributed system which is simplified version of [Astrolabe](https://www.cs.cornell.edu/projects/quicksilver/public_pdfs/Astrolabe.pdf) system.

### Project structure

```
.                     - ROOT DIRECTORY
├── agent             - agent module
├── common            - separate module for RMI common code with some utilities
├── datamodel         - datamodel module
├── fetcher           - fetcher service module
├── interpreter       - interpreter module
├── libs              - external libraries directory
├── scripts           - common scripts 
├── signer            - signer service module
├── templates         - configuration templates
├── web               - web service/client module
├── cloudatlas.iml    - IntellijIdea env config
├── cloudatlas-pack   - package preparation script
├── LICENSE           - license...
├── pom.xml           - maven pom
└── Readme.md         - this
```

### Prerequisites 

Apache Maven is project management tool used.
For fast and hassle-free build install `maven`.

Code written using `OpenJDK 1.8` and this version is forced by maven during compilation.
``` xml
    <!-- somewhere inside pom.xml -->
    <configuration>
      <source>1.8</source>
      <target>1.8</target>
    </configuration>
```

Project would not compile nor run with version below 1.7.

### Building

Just run `mvn clean compile` from ROOT directory.

### Default configuration

Default configuration provided with project is enough to run all services on single machine for first try.
WWW service is provided at port 44444 as default. Just fire `./run` scripts in agent, fetcher, signer and web (web at the end) module directory.
Connect with `localhost:44444` - CloudAtlas Web Service should be available.

In repository root are two more directories with scripts/tools prepared for fast system setup in MIMUW lab and VirtualBox lab. 
 
### How to play with?

1. First, please, read detailed description of:
    - [agent](./agent/Readme.md)
    - [fetcher](./fetcher/Readme.md)
    - [web](./web/Readme.md)
    - [signer](./signer/Readme.md)
2. Create package and upload project on target machines.
    - Use `./cloudatlas-pack runtime path/to/your/archive.tar.gz` to compile all modules and pack all required files into one tar archive.
    - All files will be archived with `CloudAtlas` prefix
3. Move to next section

##### Starting up

_Note: all commands and paths in subsections are relative to module's root directory and all services start as daemons,_

###### Module `agent/`

1. Create agent's profile using `./create-profile` script. Here you need to answer some questions asked by wizard. It could be a little bit but to provide fexibility I decided to provide such a solution.
  I don't know where agent will be running and editing files is annoying I think.
2. Once profile is created, start agent on destination machine with `./run <profile name>` command. Agent startup failures should be detectd and you will be informed.
    - To follow agent log run: `tail -f logs/<profile name>/agent.log`
    - Agent starts as daemon. 
3. After all, stop agent using `./stop`

###### Module `fetcher/`

1. Create configuration using `./configure`. Where you need to provide RMI host and port as well as attributes fetching interval.
    - `host` can be set to `localhost` because fetcher is always running on same machine as agent. 
    - `port` same as in local agent's configuration profile
2. Generated configuration lets to define own attributes and bash commands for generating values for them. For more details, please, refer to `config/fetcher.ini`.
3. Run fetcher with `./run`
    - Fetcher is logging to `$hostname-fetcher.log` file
4. Stop using `./stop`

###### Module `signer/`

1. Place signer on one of machines. Project comes with pregenerated key pair placed in `keys` directory. Signer and agent uses keys stored in `*.der` files.
    - If you want to generate new keys, use `./generate-keys` script. New keys will override old ones.
2. Prepare signer configuration using `./configure`. Provide here key paths as well as RMI host and port. As in agent case, RMI host should be client machine resolvable address or hostname.
3. Start signer daemon with `./run`
    - Logs are stored in `signer.log`
4. Stop using `./stop`

###### Modules `web/`

1. Create client configuration using `./configure`. Provide here signer's and agent's RMI params as well as port on which website will be served.
2. Start daemon with `./run`.
    - Logs are available at `$hostname-web.log`
3. Stop with `./stop`


