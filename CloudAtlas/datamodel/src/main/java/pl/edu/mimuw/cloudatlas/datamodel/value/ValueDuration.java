/*
  Copyright (c) 2017, University of Warsaw
  All rights reserved.
  <p>
  Redistribution and use in source and binary forms, with or without modification, are permitted
  provided that the following conditions are met:
  <p>
  1. Redistributions of source code must retain the above copyright notice, this list of
  conditions and the following disclaimer.
  <p>
  2. Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or other materials provided
  with the distribution.
  <p>
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
  WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.datamodel.value;

import pl.edu.mimuw.cloudatlas.datamodel.exception.IncompatibleTypesException;
import pl.edu.mimuw.cloudatlas.datamodel.exception.UnsupportedConversionException;
import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class representing duration in milliseconds. The duration can be negative. This is a simple wrapper of a Java
 * <code>Long</code> object.
 */
public class ValueDuration extends ValueSimple<Long> implements Serializable {
    /**
     * Constructs a new <code>ValueDuration</code> object from the specified amounts of different time units.
     *
     * @param days         a number of full days
     * @param hours        a number of full hours (an absolute value does not have to be lower than 24)
     * @param minutes      a number of full minutes (an absolute value does not have to be lower than 60)
     * @param seconds      a number of full seconds (an absolute value does not have to be lower than 60)
     * @param milliseconds a number of milliseconds (an absolute value does not have to be lower than 1000)
     */
    public ValueDuration(long days, long hours, long minutes, long seconds, long milliseconds) {

        this(days * 24L + hours, minutes, seconds, milliseconds);
    }

    /**
     * Constructs a new <code>ValueDuration</code> object from the specified amounts of different time units.
     *
     * @param hours        a number of full hours
     * @param minutes      a number of full minutes (an absolute value does not have to be lower than 60)
     * @param seconds      a number of full seconds (an absolute value does not have to be lower than 60)
     * @param milliseconds a number of milliseconds (an absolute value does not have to be lower than 1000)
     */
    public ValueDuration(long hours, long minutes, long seconds, long milliseconds) {

        this(hours * 60L + minutes, seconds, milliseconds);
    }

    /**
     * Constructs a new <code>ValueDuration</code> object from the specified amounts of different time units.
     *
     * @param minutes      a number of full minutes
     * @param seconds      a number of full seconds (an absolute value does not have to be lower than 60)
     * @param milliseconds a number of milliseconds (an absolute value does not have to be lower than 1000)
     */
    public ValueDuration(long minutes, long seconds, long milliseconds) {

        this(minutes * 60L + seconds, milliseconds);
    }

    /**
     * Constructs a new <code>ValueDuration</code> object from the specified amounts of different time units.
     *
     * @param seconds      a number of full seconds
     * @param milliseconds a number of milliseconds (an absolute value does not have to be lower than 1000)
     */
    public ValueDuration(long seconds, long milliseconds) {

        this(seconds * 1000L + milliseconds);
    }

    /**
     * Constructs a new <code>ValueDuration</code> object wrapping the specified <code>value</code>.
     *
     * @param value the value to wrap
     */
    public ValueDuration(Long value) {

        super(value);
    }

    // Hidden kryo
    private ValueDuration() {
        this(0L);
    }

    /**
     * Constructs a new <code>ValueDuration</code> object from its textual representation. The representation has
     * format: <code>sd hh:mm:ss.lll</code> where:
     * <ul>
     * <li><code>s</code> is a sign (<code>+</code> or <code>-</code>),</li>
     * <li><code>d</code> is a number of days,</li>
     * <li><code>hh</code> is a number of hours (between <code>00</code> and <code>23</code>),</li>
     * <li><code>mm</code> is a number of minutes (between <code>00</code> and <code>59</code>),</li>
     * <li><code>ss</code> is a number of seconds (between <code>00</code> and <code>59</code>),</li>
     * <li><code>lll</code> is a number of milliseconds (between <code>000</code> and <code>999</code>).</li>
     * </ul>
     * <p>
     * All fields are obligatory.
     *
     * @param value a textual representation of a duration
     * @throws IllegalArgumentException if <code>value</code> does not meet described rules
     */
    public ValueDuration(String value) {

        this(parseDuration(value));
    }

    /**
     * @param value full representation of duration as string
     * @return duration
     */
    private static long parseDuration(String value) {

        String pattern = "([+-])([0-9]+) ([0-9]{2}):([0-9]{2}):([0-9]{2}):([0-9]{3})";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(value);
        if (m.find()) {
            System.out.println(m.group());
        } else {
            throw new IllegalArgumentException("String \'" + value + "\' doesn't match following pattern \'" + pattern + "\'.");
        }
        return 0L;
    }

    /**
     * Static method to produce string representation of duration given as signed milliseconds value.
     *
     * @param duration duration in milliseconds
     * @return string representation of duration
     * @see #parseDuration(String)
     */
    private static String stringifyDuration(long duration) {
        String result = "";
        long day = (24 * 3600 * 1000);
        long hour = 3600 * 1000;
        long minute = 60 * 1000;
        long second = 1000;

        if (duration < 0) {
            result += "-";
            duration = -duration;
        } else {
            result += "+";
        }
        long days = duration / day;
        long hours = (duration % day) / hour;
        long minutes = (duration % hour) / minute;
        long seconds = (duration % minute) / second;
        long milis = duration % second;
        result += Long.toString(days) + " ";
        result += (hours < 10 ? "0" : "") + Long.toString(hours) + ":";
        result += (minutes < 10 ? "0" : "") + Long.toString(minutes) + ":";
        result += (seconds < 10 ? "0" : "") + Long.toString(seconds) + ":";
        if (milis < 10) {
            result += "00";
        } else if (milis < 100) {
            result += "0";
        }
        result += Long.toString(milis);
        return result;
    }

    @Override
    public Type getType() {

        return TypePrimitive.DURATION;
    }

    /**
     * Return default value of duration which is 0ms.
     *
     * @return default value
     */
    @Override
    public Value getDefaultValue() {

        return new ValueDuration(0L);
    }

    /**
     * Compare two duration values.
     *
     * @param value the right side of the operator
     * @return see Value#isLowerThan(Value)
     */
    @Override
    public ValueBoolean isLowerThan(Value value) {

        sameTypesOrThrow(value, Operation.COMPARE);
        if (isNull() || value.isNull()) {
            return new ValueBoolean(null);
        }
        return new ValueBoolean(getValue() < ((ValueDuration) value).getValue());
    }

    /**
     * Add two durations together.
     *
     * @param value the right side of the operator
     * @return sum of durations
     */
    @Override
    public ValueDuration addValue(Value value) {

        sameTypesOrThrow(value, Operation.ADD);
        if (isNull() || value.isNull()) {
            return new ValueDuration(ValueString.NULL_STRING.getValue());
        }
        return new ValueDuration(getValue() + ((ValueDuration) value).getValue());
    }

    /**
     * Subtract two durations each other.
     *
     * @param value the right side of the operator
     * @return difference of durations
     */
    @Override
    public ValueDuration subtract(Value value) {

        sameTypesOrThrow(value, Operation.SUBTRACT);
        if (isNull() || value.isNull()) {
            return new ValueDuration(ValueString.NULL_STRING.getValue());
        }
        return new ValueDuration(getValue() - ((ValueDuration) value).getValue());
    }

    /**
     * Multiplies duration by integer of double value.
     * Otherwise throws exception IncompatibleTypesException.
     *
     * @param value the right side of the operator
     * @return new duration
     * @throws IncompatibleTypesException
     */
    @Override
    public ValueDuration multiply(Value value) {

        if (isNull() || value.isNull()) {
            return new ValueDuration(ValueString.NULL_STRING.getValue());
        }
        switch (value.getType().getPrimaryType()) {
            case INT:
                return new ValueDuration(getValue() * ((ValueInt) value).getValue());
            case DOUBLE:
                return new ValueDuration((long) (getValue() * ((ValueDouble) value).getValue()));
            default:
                throw new IncompatibleTypesException(getType(), value.getType(), Operation.MULTIPLY);
        }
    }

    /**
     * Divide duration value. Allowed division operations are:
     * <ul>
     * <li>DURATION / DURATION --> DOUBLE</li>
     * <li>DURATION / INT --> DURATION</li>
     * <li>DURATION / DOUBLE --> DURATION</li>
     * </ul>
     *
     * @param value the right side of the operator
     * @return scaled DURATION or DOUBLE
     * @throws ArithmeticException
     * @throws IncompatibleTypesException
     */
    @Override
    public Value divide(Value value) {

        if (value.isNull() || isNull()) return new ValueDuration(ValueString.NULL_STRING.getValue());
        switch (value.getType().getPrimaryType()) {
            case INT:
                if (((ValueInt) value).getValue() == 0L) throw new ArithmeticException("Division by zero.");
                return new ValueDuration(getValue() / ((ValueInt) value).getValue());
            case DOUBLE:
                if (((ValueDouble) value).getValue() == 0L) throw new ArithmeticException("Division by zero.");
                return new ValueDuration((long) (getValue().doubleValue() / ((ValueDouble) value).getValue()));
            case DURATION:
                if (((ValueDuration) value).getValue() == 0L) throw new ArithmeticException("Division by zero.");
                return new ValueDouble(getValue().doubleValue() / ((ValueDuration) value).getValue().doubleValue());
            default:
                throw new IncompatibleTypesException(getType(), value.getType(), Operation.DIVIDE);
        }
    }

    @Override
    public ValueDuration negate() {

        return new ValueDuration(isNull() ? null : -getValue());
    }

    @Override
    public Value convertTo(Type type) {

        switch (type.getPrimaryType()) {
            case DOUBLE:
                return new ValueDouble(getValue() == null ? null : getValue().doubleValue());
            case INT:
                return new ValueInt(getValue());
            case DURATION:
                return this;
            case STRING:
                return getValue() == null ? ValueString.NULL_STRING : new ValueString(toString());
            default:
                throw new UnsupportedConversionException(getType(), type);
        }
    }

    @Override
    public String toString() {
        if (isNull())
            return ValueString.NULL_STRING.getValue();
        return stringifyDuration(getValue());
    }
}
