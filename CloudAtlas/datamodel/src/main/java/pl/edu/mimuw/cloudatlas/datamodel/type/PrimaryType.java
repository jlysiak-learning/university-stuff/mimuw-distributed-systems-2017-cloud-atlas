package pl.edu.mimuw.cloudatlas.datamodel.type;

/**
 * Base types. Each Value is characterized with one of base types.
 */
public enum PrimaryType {
    BOOLEAN, CONTACT, DOUBLE, TIME, DURATION, INT, STRING, LIST, SET, NULL
}