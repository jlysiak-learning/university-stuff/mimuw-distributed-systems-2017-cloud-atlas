/*
  Copyright (c) 2017, University of Warsaw All rights reserved. <p> Redistribution and use in source and binary forms,
  with or without modification, are permitted provided that the following conditions are met: <p> 1. Redistributions of
  source code must retain the above copyright notice, this list of conditions and the following disclaimer. <p> 2.
  Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
  disclaimer in the documentation and/or other materials provided with the distribution. <p> THIS SOFTWARE IS PROVIDED
  BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.datamodel.type;

import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueNull;

import java.io.Serializable;

/**
 * Convenient class for types that just wrap ordinary Java types.
 *
 * @see TypeCollection
 */
public class TypePrimitive extends Type implements Serializable {
    /**
     * Boolean type instance.
     */
    public static final TypePrimitive BOOLEAN = new TypePrimitive(PrimaryType.BOOLEAN);

    /**
     * Contact type.
     */
    public static final TypePrimitive CONTACT = new TypePrimitive(PrimaryType.CONTACT);

    /**
     * Double type.
     */
    public static final TypePrimitive DOUBLE = new TypePrimitive(PrimaryType.DOUBLE);

    /**
     * Duration type.
     */
    public static final TypePrimitive DURATION = new TypePrimitive(PrimaryType.DURATION);

    /**
     * Integer type.
     */
    public static final TypePrimitive INTEGER = new TypePrimitive(PrimaryType.INT);

    /**
     * A special "null type" that represents null value of an unknown type. It can be converted to any other type.
     *
     * @see Type#isCompatible(Type)
     * @see ValueNull
     */
    public static final TypePrimitive NULL = new TypePrimitive(PrimaryType.NULL);

    /**
     * String type.
     */
    public static final TypePrimitive STRING = new TypePrimitive(PrimaryType.STRING);

    /**
     * Time type.
     */
    public static final TypePrimitive TIME = new TypePrimitive(PrimaryType.TIME);

    /**
     * Set type.
     */
    public static final TypePrimitive SET = new TypePrimitive(PrimaryType.SET);

    /**
     * List type.
     */
    public static final TypePrimitive LIST = new TypePrimitive(PrimaryType.LIST);

    /**
     * Create primitive and throw exception if illegal type given.
     *
     * @param primaryType type of primitive
     * @throws IllegalArgumentException if <code>primaryType</code> is illegal.
     */
    private TypePrimitive(PrimaryType primaryType) {

        super(primaryType);
        switch (primaryType) {
            case INT:
            case SET:
            case LIST:
            case NULL:
            case TIME:
            case DOUBLE:
            case STRING:
            case BOOLEAN:
            case CONTACT:
            case DURATION:
                break;
            default:
                throw new IllegalArgumentException("TypePrimitive class can represent only primitive types.");
        }
    }

    // Hidden
    private TypePrimitive(){
        super(PrimaryType.NULL);
    }

    /**
     * Gets text representation.
     *
     * @return string representation
     */
    @Override
    public String toString() {

        return getPrimaryType().toString();
    }


    /**
     * Gets text representation.
     *
     * @return string representation
     */
    @Override
    public boolean isCompatible(Type type) {

        return super.isCompatible(type) || getPrimaryType() == type.getPrimaryType();
    }

    public Value getNull() {
        return ValueNull.getInstance();
    }

    public static TypePrimitive getInstanceOf(PrimaryType type) {
        switch (type) {

            case BOOLEAN:
                return BOOLEAN;
            case CONTACT:
                return CONTACT;
            case DOUBLE:
                return DOUBLE;
            case TIME:
                return TIME;
            case DURATION:
                return DURATION;
            case INT:
                return INTEGER;
            case STRING:
                return STRING;
            case LIST:
                return LIST;
            case SET:
                return SET;
            case NULL:
                return NULL;

            default:
                throw new IllegalArgumentException("TypePrimitive class can represent only primitive types.");
        }
    }
}
