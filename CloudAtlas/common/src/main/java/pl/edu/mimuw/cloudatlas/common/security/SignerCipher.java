package pl.edu.mimuw.cloudatlas.common.security;

import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class SignerCipher {

    private final static String ENCRYPTION_ALGORITHM = "RSA";

    private final static int NUM_KEY_BITS = 1024;

    public static Cipher getVerifier(PublicKey publicKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Cipher verifyCipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
        verifyCipher.init(Cipher.DECRYPT_MODE, publicKey);
        return verifyCipher;
    }

    public static Cipher getSigner(PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        Cipher verifyCipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
        verifyCipher.init(Cipher.ENCRYPT_MODE, privateKey);
        return verifyCipher;
    }

    /**
     * Get private key from file.
     *
     * @param filename private key DER file
     * @return #PrivateKey instance
     * @throws Exception
     */
    public static PrivateKey getPrivateKey(String filename) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException {
        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(spec);
    }

    /**
     * Get public key from file.
     *
     * @param filename public key DER file
     * @return #PublicKey instance
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static PublicKey getPublicKey(String filename) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(spec);
    }

    /**
     * Generate query descriptor.
     * @param name query name
     * @param queryString SQL like query
     * @param id unique id assigned by signer
     * @return descriptor bytes
     */
    public static byte[] generateDescriptor(Attribute name, String queryString, long id) {
        String descriptor = "[" + name.toString() + ":" + queryString + ":" + Long.toHexString(id) + "]";
        return descriptor.getBytes();
    }

}