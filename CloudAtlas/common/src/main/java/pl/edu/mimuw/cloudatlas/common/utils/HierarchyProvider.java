/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.common.utils;

import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.ZMI;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.*;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class HierarchyProvider {
    /**
     * Method creates ZMI hierarchy for tests provided during labs 03.
     *
     * @return
     * @throws ParseException
     * @throws UnknownHostException
     */
    public static ZMI createTestHierarchyForLab3Tests() throws ParseException, UnknownHostException {
        ValueContact violet07Contact = createContact("/uw/violet07", (byte) 10, (byte) 1, (byte) 1, (byte) 10);
        ValueContact khaki13Contact = createContact("/uw/khaki13", (byte) 10, (byte) 1, (byte) 1, (byte) 38);
        ValueContact khaki31Contact = createContact("/uw/khaki31", (byte) 10, (byte) 1, (byte) 1, (byte) 39);
        ValueContact whatever01Contact = createContact("/uw/whatever01", (byte) 82, (byte) 111, (byte) 52, (byte) 56);
        ValueContact whatever02Contact = createContact("/uw/whatever02", (byte) 82, (byte) 111, (byte) 52, (byte) 57);

        List<Value> list;
        ZMI root;

        root = new ZMI();
        root.getAttributes().add("level", new ValueInt(0L));
        root.getAttributes().add("name", new ValueString(null));
        root.getAttributes().add("owner", new ValueString("/uw/violet07"));
        root.getAttributes().add("timestamp", new ValueTime("2012/11/09 20:10:17.342"));
        root.getAttributes().add("contacts", new ValueSet(TypePrimitive.CONTACT));
        root.getAttributes().add("cardinality", new ValueInt(0L));

        ZMI uw = new ZMI(root);
        root.addSon(uw);
        uw.getAttributes().add("level", new ValueInt(1L));
        uw.getAttributes().add("name", new ValueString("uw"));
        uw.getAttributes().add("owner", new ValueString("/uw/violet07"));
        uw.getAttributes().add("timestamp", new ValueTime("2012/11/09 20:8:13.123"));
        uw.getAttributes().add("contacts", new ValueSet(TypePrimitive.CONTACT));
        uw.getAttributes().add("cardinality", new ValueInt(0L));

        ZMI pjwstk = new ZMI(root);
        root.addSon(pjwstk);
        pjwstk.getAttributes().add("level", new ValueInt(1L));
        pjwstk.getAttributes().add("name", new ValueString("pjwstk"));
        pjwstk.getAttributes().add("owner", new ValueString("/pjwstk/whatever01"));
        pjwstk.getAttributes().add("timestamp", new ValueTime("2012/11/09 20:8:13.123"));
        pjwstk.getAttributes().add("contacts", new ValueSet(TypePrimitive.CONTACT));
        pjwstk.getAttributes().add("cardinality", new ValueInt(0L));

        ZMI violet07 = new ZMI(uw);
        uw.addSon(violet07);
        violet07.getAttributes().add("level", new ValueInt(2L));
        violet07.getAttributes().add("name", new ValueString("violet07"));
        violet07.getAttributes().add("owner", new ValueString("/uw/violet07"));
        violet07.getAttributes().add("timestamp", new ValueTime("2012/11/09 18:00:00.000"));
        list = Arrays.asList(new Value[]{khaki31Contact, whatever01Contact});
        violet07.getAttributes().add("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        violet07.getAttributes().add("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{violet07Contact,});
        violet07.getAttributes().add("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        violet07.getAttributes().add("creation", new ValueTime("2011/11/09 20:8:13.123"));
        violet07.getAttributes().add("cpu_usage", new ValueDouble(0.9));
        violet07.getAttributes().add("num_cores", new ValueInt(3L));
        violet07.getAttributes().add("has_ups", new ValueBoolean(null));
        list = Arrays.asList(new Value[]{new ValueString("tola"), new ValueString("tosia"),});
        violet07.getAttributes().add("some_names", new ValueList(list, TypePrimitive.STRING));
        violet07.getAttributes().add("expiry", new ValueDuration(13L, 12L, 0L, 0L, 0L));

        ZMI khaki31 = new ZMI(uw);
        uw.addSon(khaki31);
        khaki31.getAttributes().add("level", new ValueInt(2L));
        khaki31.getAttributes().add("name", new ValueString("khaki31"));
        khaki31.getAttributes().add("owner", new ValueString("/uw/khaki31"));
        khaki31.getAttributes().add("timestamp", new ValueTime("2012/11/09 20:03:00.000"));
        list = Arrays.asList(new Value[]{violet07Contact, whatever02Contact,});
        khaki31.getAttributes().add("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        khaki31.getAttributes().add("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{khaki31Contact});
        khaki31.getAttributes().add("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        khaki31.getAttributes().add("creation", new ValueTime("2011/11/09 20:12:13.123"));
        khaki31.getAttributes().add("cpu_usage", new ValueDouble(null));
        khaki31.getAttributes().add("num_cores", new ValueInt(3L));
        khaki31.getAttributes().add("has_ups", new ValueBoolean(false));
        list = Arrays.asList(new Value[]{new ValueString("agatka"), new ValueString("beatka"), new ValueString("celina"),});
        khaki31.getAttributes().add("some_names", new ValueList(list, TypePrimitive.STRING));
        khaki31.getAttributes().add("expiry", new ValueDuration(-13L, -11L, 0L, 0L, 0L));

        ZMI khaki13 = new ZMI(uw);
        uw.addSon(khaki13);
        khaki13.getAttributes().add("level", new ValueInt(2L));
        khaki13.getAttributes().add("name", new ValueString("khaki13"));
        khaki13.getAttributes().add("owner", new ValueString("/uw/khaki13"));
        khaki13.getAttributes().add("timestamp", new ValueTime("2012/11/09 21:03:00.000"));
        list = Arrays.asList(new Value[]{});
        khaki13.getAttributes().add("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        khaki13.getAttributes().add("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{khaki13Contact,});
        khaki13.getAttributes().add("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        khaki13.getAttributes().add("creation", new ValueTime((Long) null));
        khaki13.getAttributes().add("cpu_usage", new ValueDouble(0.1));
        khaki13.getAttributes().add("num_cores", new ValueInt(null));
        khaki13.getAttributes().add("has_ups", new ValueBoolean(true));
        list = Arrays.asList(new Value[]{});
        khaki13.getAttributes().add("some_names", new ValueList(list, TypePrimitive.STRING));
        khaki13.getAttributes().add("expiry", new ValueDuration((Long) null));

        ZMI whatever01 = new ZMI(pjwstk);
        pjwstk.addSon(whatever01);
        whatever01.getAttributes().add("level", new ValueInt(2L));
        whatever01.getAttributes().add("name", new ValueString("whatever01"));
        whatever01.getAttributes().add("owner", new ValueString("/uw/whatever01"));
        whatever01.getAttributes().add("timestamp", new ValueTime("2012/11/09 21:12:00.000"));
        list = Arrays.asList(new Value[]{violet07Contact, whatever02Contact,});
        whatever01.getAttributes().add("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        whatever01.getAttributes().add("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{whatever01Contact,});
        whatever01.getAttributes().add("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        whatever01.getAttributes().add("creation", new ValueTime("2012/10/18 07:03:00.000"));
        whatever01.getAttributes().add("cpu_usage", new ValueDouble(0.1));
        whatever01.getAttributes().add("num_cores", new ValueInt(7L));
        list = Arrays.asList(new Value[]{new ValueString("rewrite")});
        whatever01.getAttributes().add("php_modules", new ValueList(list, TypePrimitive.STRING));

        ZMI whatever02 = new ZMI(pjwstk);
        pjwstk.addSon(whatever02);
        whatever02.getAttributes().add("level", new ValueInt(2L));
        whatever02.getAttributes().add("name", new ValueString("whatever02"));
        whatever02.getAttributes().add("owner", new ValueString("/uw/whatever02"));
        whatever02.getAttributes().add("timestamp", new ValueTime("2012/11/09 21:13:00.000"));
        list = Arrays.asList(new Value[]{khaki31Contact, whatever01Contact,});
        whatever02.getAttributes().add("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        whatever02.getAttributes().add("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{whatever02Contact,});
        whatever02.getAttributes().add("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        whatever02.getAttributes().add("creation", new ValueTime("2012/10/18 07:04:00.000"));
        whatever02.getAttributes().add("cpu_usage", new ValueDouble(0.4));
        whatever02.getAttributes().add("num_cores", new ValueInt(13L));
        list = Arrays.asList(new Value[]{new ValueString("odbc")});
        whatever02.getAttributes().add("php_modules", new ValueList(list, TypePrimitive.STRING));

        return root;
    }

    private static ValueContact createContact(String path, byte ip1, byte ip2, byte ip3, byte ip4) throws UnknownHostException {

        return new ValueContact(new PathName(path), InetAddress.getByAddress(new byte[]{ip1, ip2, ip3, ip4}));
    }

    /**
     * Method creates ZMI hierarchy for Assignment1.
     *
     * @return
     * @throws ParseException
     * @throws UnknownHostException
     */
    public static ZMI createHierarchyForAssignment1() throws ParseException, UnknownHostException {

        ValueContact violet07Contact = createContact("/uw/violet07", (byte) 10, (byte) 1, (byte) 1, (byte) 10);
        ValueContact khaki13Contact = createContact("/uw/khaki13", (byte) 10, (byte) 1, (byte) 1, (byte) 38);
        ValueContact khaki31Contact = createContact("/uw/khaki31", (byte) 10, (byte) 1, (byte) 1, (byte) 39);
        ValueContact whatever01Contact = createContact("/pjwstk/whatever01", (byte) 82, (byte) 111, (byte) 52, (byte) 56);
        ValueContact whatever02Contact = createContact("/pjwstk/whatever02", (byte) 82, (byte) 111, (byte) 52, (byte) 57);

        List<Value> list;

        ZMI root = new ZMI();
        root.getAttributes().addOrChange("level", new ValueInt(0L));
        root.getAttributes().addOrChange("name", new ValueString(null));

        ZMI uw = new ZMI(root);
        root.addSon(uw);
        uw.getAttributes().addOrChange("level", new ValueInt(1L));
        uw.getAttributes().addOrChange("name", new ValueString("uw"));

        ZMI pjwstk = new ZMI(root);
        root.addSon(pjwstk);
        pjwstk.getAttributes().addOrChange("level", new ValueInt(1L));
        pjwstk.getAttributes().addOrChange("name", new ValueString("pjwstk"));

        ZMI violet07 = new ZMI(uw);
        uw.addSon(violet07);
        violet07.getAttributes().addOrChange("level", new ValueInt(2L));
        violet07.getAttributes().addOrChange("name", new ValueString("violet07"));
        violet07.getAttributes().addOrChange("owner", new ValueString("/uw/violet07"));
        violet07.getAttributes().addOrChange("timestamp", new ValueTime("2012/11/09 18:00:00.000"));
        list = Arrays.asList(new Value[]{khaki31Contact, whatever01Contact, khaki13Contact});
        violet07.getAttributes().addOrChange("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        violet07.getAttributes().addOrChange("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{violet07Contact,});
        violet07.getAttributes().addOrChange("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        violet07.getAttributes().addOrChange("creation", new ValueTime("2011/11/09 20:8:13.123"));
        violet07.getAttributes().addOrChange("cpu_usage", new ValueDouble(0.9));
        violet07.getAttributes().addOrChange("num_cores", new ValueInt(3L));
        violet07.getAttributes().addOrChange("num_processes", new ValueInt(131L));
        violet07.getAttributes().addOrChange("has_ups", new ValueBoolean(null));
        list = Arrays.asList(new Value[]{new ValueString("tola"), new ValueString("tosia"),});
        violet07.getAttributes().addOrChange("some_names", new ValueList(list, TypePrimitive.STRING));
        violet07.getAttributes().addOrChange("expiry", new ValueDuration(13L, 12L, 0L, 0L, 0L));
        // Violet07 - correct!

        ZMI khaki31 = new ZMI(uw);
        uw.addSon(khaki31);
        khaki31.getAttributes().addOrChange("level", new ValueInt(2L));
        khaki31.getAttributes().addOrChange("name", new ValueString("khaki31"));
        khaki31.getAttributes().addOrChange("owner", new ValueString("/uw/khaki31"));
        khaki31.getAttributes().addOrChange("timestamp", new ValueTime("2012/11/09 20:03:00.000"));
        list = Arrays.asList(new Value[]{violet07Contact,});
        khaki31.getAttributes().addOrChange("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        khaki31.getAttributes().addOrChange("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{khaki31Contact});
        khaki31.getAttributes().addOrChange("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        khaki31.getAttributes().addOrChange("creation", new ValueTime("2011/11/09 20:12:13.123"));
        khaki31.getAttributes().addOrChange("cpu_usage", new ValueDouble(null));
        khaki31.getAttributes().addOrChange("num_cores", new ValueInt(3L));
        violet07.getAttributes().addOrChange("num_processes", new ValueInt(124L));
        khaki31.getAttributes().addOrChange("has_ups", new ValueBoolean(false));
        list = Arrays.asList(new Value[]{new ValueString("agatka"), new ValueString("beatka"), new ValueString("celina"),});
        khaki31.getAttributes().addOrChange("some_names", new ValueList(list, TypePrimitive.STRING));
        khaki31.getAttributes().addOrChange("expiry", new ValueDuration(-13L, -11L, 0L, 0L, 0L));

        ZMI khaki13 = new ZMI(uw);
        uw.addSon(khaki13);
        khaki13.getAttributes().addOrChange("level", new ValueInt(2L));
        khaki13.getAttributes().addOrChange("name", new ValueString("khaki13"));
        khaki13.getAttributes().addOrChange("owner", new ValueString("/uw/khaki13"));
        khaki13.getAttributes().addOrChange("timestamp", new ValueTime("2012/11/09 21:03:00.000"));
        list = Arrays.asList(new Value[]{violet07Contact, whatever02Contact});
        khaki13.getAttributes().addOrChange("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        khaki13.getAttributes().addOrChange("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{khaki13Contact,});
        khaki13.getAttributes().addOrChange("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        khaki13.getAttributes().addOrChange("creation", new ValueTime((Long) null));
        khaki13.getAttributes().addOrChange("cpu_usage", new ValueDouble(0.1));
        khaki13.getAttributes().addOrChange("num_cores", new ValueInt(null));
        khaki13.getAttributes().addOrChange("num_processes", new ValueInt(107L));
        khaki13.getAttributes().addOrChange("has_ups", new ValueBoolean(true));
        list = Arrays.asList(new Value[]{});
        khaki13.getAttributes().addOrChange("some_names", new ValueList(list, TypePrimitive.STRING));
        khaki13.getAttributes().addOrChange("expiry", new ValueDuration((Long) null));

        ZMI whatever01 = new ZMI(pjwstk);
        pjwstk.addSon(whatever01);
        whatever01.getAttributes().addOrChange("level", new ValueInt(2L));
        whatever01.getAttributes().addOrChange("name", new ValueString("whatever01"));
        whatever01.getAttributes().addOrChange("owner", new ValueString("/pjwstk/whatever01"));
        whatever01.getAttributes().addOrChange("timestamp", new ValueTime("2012/11/09 21:12:00.000"));
        list = Arrays.asList(new Value[]{whatever02Contact,});
        whatever01.getAttributes().addOrChange("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        whatever01.getAttributes().addOrChange("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{whatever01Contact,});
        whatever01.getAttributes().addOrChange("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        whatever01.getAttributes().addOrChange("creation", new ValueTime("2012/10/18 07:03:00.000"));
        whatever01.getAttributes().addOrChange("cpu_usage", new ValueDouble(0.1));
        whatever01.getAttributes().addOrChange("num_cores", new ValueInt(7L));
        khaki13.getAttributes().addOrChange("num_processes", new ValueInt(215L));
        list = Arrays.asList(new Value[]{new ValueString("rewrite")});
        whatever01.getAttributes().addOrChange("php_modules", new ValueList(list, TypePrimitive.STRING));

        ZMI whatever02 = new ZMI(pjwstk);
        pjwstk.addSon(whatever02);
        whatever02.getAttributes().addOrChange("level", new ValueInt(2L));
        whatever02.getAttributes().addOrChange("name", new ValueString("whatever02"));
        whatever02.getAttributes().addOrChange("owner", new ValueString("/pjwstk/whatever02"));
        whatever02.getAttributes().addOrChange("timestamp", new ValueTime("2012/11/09 21:13:00.000"));
        list = Arrays.asList(new Value[]{whatever01Contact,});
        whatever02.getAttributes().addOrChange("contacts", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        whatever02.getAttributes().addOrChange("cardinality", new ValueInt(1L));
        list = Arrays.asList(new Value[]{whatever02Contact,});
        whatever02.getAttributes().addOrChange("members", new ValueSet(new HashSet<>(list), TypePrimitive.CONTACT));
        whatever02.getAttributes().addOrChange("creation", new ValueTime("2012/10/18 07:04:00.000"));
        whatever02.getAttributes().addOrChange("cpu_usage", new ValueDouble(0.4));
        khaki13.getAttributes().addOrChange("num_processes", new ValueInt(222L));
        whatever02.getAttributes().addOrChange("num_cores", new ValueInt(13L));
        list = Arrays.asList(new Value[]{new ValueString("odbc")});
        whatever02.getAttributes().addOrChange("php_modules", new ValueList(list, TypePrimitive.STRING));

        return root;
    }
}
