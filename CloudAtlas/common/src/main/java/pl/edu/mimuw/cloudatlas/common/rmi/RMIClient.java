/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.common.rmi;

import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * CloudAtlas RMI client.
 */
public class RMIClient<T extends Remote> {
    private final static String LOGNAME = "RMI-CLIENT";

    private final static int CONNECTION_ATTEMPTS = 10;

    private final static long CONNECTION_ATTEMPTS_INTERVAL = 5000;

    /// Remote server host
    private final String host;

    /// Remote server port
    private final int port;

    /// Remote interface
    private T remote;

    public RMIClient(String host, int port) {
        this.host = host;
        this.port = port;

        Log.log(LOGNAME, "Creating client!");
        if (System.getSecurityManager() == null) {
            Log.log(LOGNAME, "Setting up new Security Manager.");
            System.setSecurityManager(new SecurityManager());
        }
    }

    /**
     * Start RMI client.
     */
    public void run() {
        Log.log(LOGNAME, "Starting...");
        for (int i = 0; i < CONNECTION_ATTEMPTS; i++) {
            try {
                Registry registry = LocateRegistry.getRegistry(host, port);
                // Get remote interface
                remote = (T) registry.lookup(RMIServer.CLOUDATLAS_RMI_SERVER_NAME);
                Log.log(LOGNAME, "RMI client started!");
                return;
            } catch (Exception e) {
                Log.err(LOGNAME, "Fatal client error: " + e.getMessage());
                if (i != CONNECTION_ATTEMPTS - 1) {
                    Log.log(LOGNAME, "Retry in " + CONNECTION_ATTEMPTS_INTERVAL + "ms.");
                    try {
                        Thread.sleep(CONNECTION_ATTEMPTS_INTERVAL);
                    } catch (InterruptedException ignored) {}
                    Log.log(LOGNAME, "Trying again...");
                }
            }
        }
        Log.fatal(LOGNAME, "Client couldn't connect with RMI server @ " + host + ":" + port);
        Log.fatal(LOGNAME, "Shutdown...");
        System.exit(1);
    }

    /**
     * Get remote server handler to execute commands.
     *
     * @return handler to remote server
     */
    public T getRemote() {
        return remote;
    }
}
