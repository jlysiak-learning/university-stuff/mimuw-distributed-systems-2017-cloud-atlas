package pl.edu.mimuw.cloudatlas.common.utils.logger;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Some static methods to print some logs on stderr.
 */
public class Log {
    private static final PrintStream out = System.err;

    private static String withColors(LogLevel lvl) {
        int v = 93;
        switch (lvl) {
            case LOG:
                v = 92;
                break;
            case WRN:
                v = 93;
                break;
            case ERR:
                v = 91;
                break;
            case FAT:
                v = 91;
                break;
        }
        return String.format("\033[%dm%s\033[0m", v, lvl.toString());
    }

    private static String template(LogLevel type, String module, String msg) {
        DateFormat dateFormat = new SimpleDateFormat("YY/MM/dd HH:mm:ss.SSS");
        Date date = new Date();
        return String.format("[%17s][%3s][%10s]: %s", dateFormat.format(date), withColors(type), module, msg);
    }

    public static void log(String module, String msg) {
        out.println(template(LogLevel.LOG, module, msg));
    }

    public static void wrn(String module, String msg) {
        out.println(template(LogLevel.WRN, module, msg));
    }

    public static void err(String module, String msg) {
        out.println(template(LogLevel.ERR, module, msg));
    }

    public static void fatal(String module, String msg) {
        out.println(template(LogLevel.FAT, module, msg));
    }

    public enum LogLevel {
        LOG, WRN, ERR, FAT
    }
}
