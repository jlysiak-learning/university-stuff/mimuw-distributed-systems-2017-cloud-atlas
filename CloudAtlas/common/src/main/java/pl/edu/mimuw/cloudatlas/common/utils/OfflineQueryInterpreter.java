/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.common.utils;

import pl.edu.mimuw.cloudatlas.datamodel.ZMI;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.service.interpreter.QueryResultHandler;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.QueryResult;

import java.util.Scanner;

import static pl.edu.mimuw.cloudatlas.service.interpreter.InterpreterService.executeQueries;
import static pl.edu.mimuw.cloudatlas.common.utils.HierarchyProvider.createHierarchyForAssignment1;
import static pl.edu.mimuw.cloudatlas.common.utils.HierarchyProvider.createTestHierarchyForLab3Tests;

/**
 * Offline query interpreter class. Lab 3 tests.
 */
public class OfflineQueryInterpreter {
    private static ZMI root;

    /**
     * Offline queries parser entry point. Reads and parses queries from STDIN and prints output on STDOUT.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 1) {
            System.out.println("Select test hierarchy: 'lab' or 'assignment'");
        }
        String opt = args[0];
        switch (opt) {
            case "lab":
                root = createTestHierarchyForLab3Tests();
                scanQueriesOnly();
                break;
            case "assignment":
                root = createHierarchyForAssignment1();
                scanQueriesWithAttribute();
                break;
            default:
                System.out.println("No such test dataset! Put 'lab' or 'assignemnt'.");
                System.exit(1);
        }
    }

    private static void scanQueriesOnly() {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");
        while (scanner.hasNext()) {
            try {
                executeQueries(root, scanner.next(), new ResultHandlerLab()); // Execute and print results inline
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        scanner.close();
    }

    /**
     * Scan lines of stdin for queries in format: < &attr >: <query 1>; <query 2>; ... <query N>. Attributes which are
     * results of executed queries are saved in ZMIs and stay there till program end. Run interpreter again to execute
     * queries on untouched hierarchy. Hierarchy is printed out only in case of successfully executed query.
     */
    private static void scanQueriesWithAttribute() {
        Scanner scanner = new Scanner(System.in);
        scanner.useDelimiter("\\n");
        while (scanner.hasNext()) {
            String[] line = scanner.next().split(":");
            Attribute attr;
            try {
                attr = new Attribute(line[0]);
                executeQueries(root, line[1]);
                root.printAttributes(System.out);
            } catch (Exception e) {
                System.out.println("Error: " + e.getMessage());
            }
        }
        scanner.close();
    }

    private static class ResultHandlerLab implements QueryResultHandler {
        @Override
        public void handleQueryResult(ZMI zmi, QueryResult result) {
            System.out.println(zmi.getFullName() + ": " + result);
        }
    }
}
