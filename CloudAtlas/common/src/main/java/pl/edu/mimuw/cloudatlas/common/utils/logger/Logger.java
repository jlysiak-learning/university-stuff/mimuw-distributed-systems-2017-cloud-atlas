/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.common.utils.logger;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

    private PrintStream out;
    private String moduleName;

    public Logger() {
        this("", System.err);
    }

    public Logger(String moduleName) {
        this(moduleName, System.err);
    }

    public Logger(String moduleName, PrintStream out) {
        this.out = out;
        this.moduleName = moduleName;
    }

    private String invocation (String module, String type, String msg) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String result = "[" + dateFormat.format(date) + "][";
        result += module + "][" + type + "]: " + msg;
        return result;
    }
    public void fatal(String s) {
        out.println(invocation(moduleName, "FATAL", s));
    }

    public void error(String s) {
        out.println(invocation(moduleName, "ERROR", s));
    }

    public void info(String s) {
        out.println(invocation(moduleName, "INFO", s));
    }

    public void fatal(String s, Exception e) {
        fatal(s + "\n" + exceptionStackTraceToString(e));
    }

    public void error(String s, Exception e) {
        error(s + "\n" + exceptionStackTraceToString(e));
    }

    /**
     * Get stacktrace from exception and return is as a Sting value.
     * Utility for logging all information.
     * @param e exception
     * @return stacktrace as string
     */
    public static String exceptionStackTraceToString(Exception e) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        e.printStackTrace(ps);
        ps.close();
        return baos.toString();
    }
}
