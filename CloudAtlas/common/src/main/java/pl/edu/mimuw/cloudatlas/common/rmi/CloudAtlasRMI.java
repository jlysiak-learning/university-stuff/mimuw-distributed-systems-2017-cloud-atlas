/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.common.rmi;

import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.AttributesMap;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * CloudAtlas RMI interface.
 * @note Assignment1 API - could be extended or changed
 */
public interface CloudAtlasRMI extends Remote {
    /**
     * Get set of ZMIs on which agents stores information.
     * @return
     * @throws RemoteException
     */
    public List<String> getStoredZMIs() throws RemoteException;

    /**
     * Get attributes of given zone.
     * @param path full zone path
     * @return ZMI attributes map, empty map if path is wrong
     * @throws RemoteException
     */
    public AttributesMap getZMIAttributes(String path) throws RemoteException;

    /**
     * Install query on agent.
     * @param attribute query name
     * @param query query
     * @param signature
     * @param id
     * @throws RemoteException
     */
    public RMICallResult installQuery(Attribute attribute, String query, byte[] signature, long id) throws RemoteException;

    /**
     * Uninstall query on agent.
     * @param name query name
     * @param query
     * @param signature
     * @param id
     * @throws RemoteException
     */
    public RMICallResult uninstallQuery(Attribute name, String query, byte[] signature, long id) throws RemoteException;

    /**
     * Set
     * @param name
     * @param value
     * @throws RemoteException
     */
    public RMICallResult setZMIAttribute(String name, Value value) throws RemoteException;

    /**
     * Set new list of fallback i-net addresses on agent.
     *
     * @param fallbackContacts
     * @throws RemoteException
     */
    public void setFallbackContacts(ValueList fallbackContacts) throws RemoteException;


    /**
     * Additional method.
     * This simple method returns singleton zone of CloudAtlas agent.
     * @return agent full path
     * @throws RemoteException
     */
    public String whoAreYou() throws RemoteException;

}
