/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.common.rmi;


import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * RMI server class.
 */
public class RMIServer {
    public static final String CLOUDATLAS_RMI_SERVER_NAME = "CloudAtlas-RMI";

    private static final String LOGNAME = "RMI-SERVER";

    private static final int RMI_DEFAULT_PORT = 1099;

    private final Remote executor;

    private final String rmiHost;

    private final int rmiPort;

    public RMIServer(String host, int port, Remote executor) {
        Log.log(LOGNAME, "Creating RMI server...");
        rmiHost = host;
        rmiPort = port;
        this.executor = executor;
        if (System.getSecurityManager() == null) {
            Log.log(LOGNAME, "Setting up new Security Manager.");
            System.setSecurityManager(new SecurityManager());
        }
    }

    public void run() {
        Log.log(LOGNAME, "Starting RMI server...");
        try {
            Remote stub = UnicastRemoteObject.exportObject(executor, 0);
            Registry registry = LocateRegistry.getRegistry(rmiHost, rmiPort);
            registry.rebind(CLOUDATLAS_RMI_SERVER_NAME, stub);
            Log.log(LOGNAME, "RMI server bound @ " + rmiHost + ":" + rmiPort);
            /*
            From: https://docs.oracle.com/javase/tutorial/rmi/implementing.html

             Once the server has registered with the local RMI registry, it prints a message indicating
              that it is ready to start handling calls. Then, the main method completes.
              It is not necessary to have a thread wait to keep the server alive.
              As long as there is a reference to the ComputeEngine object in another Java virtual machine,
              local or remote, the ComputeEngine object will not be shut down or garbage collected.
              Because the program binds a reference to the ComputeEngine in the registry,
              it is reachable from a remote client, the registry itself.
              The RMI system keeps the ComputeEngine's process running.
              The ComputeEngine is available to accept calls and won't be reclaimed until its binding
              is removed from the registry and no remote clients hold a remote reference
              to the ComputeEngine object.
             */
        } catch (Exception e) {
            Log.log(LOGNAME, "RMI server fatal error: " + e.getMessage());
            System.exit(1);
        }
    }
}
