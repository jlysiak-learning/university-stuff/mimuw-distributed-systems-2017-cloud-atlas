package pl.edu.mimuw.cloudatlas.common.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hash {
    private final static String DIGEST_ALGORITHM = "SHA-256";

    public static byte[] hashMe(byte[] bytes) throws NoSuchAlgorithmException {
        MessageDigest digestGenerator = MessageDigest.getInstance(DIGEST_ALGORITHM);
        return digestGenerator.digest(bytes);
    }
}
