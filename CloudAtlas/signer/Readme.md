# CloudAtlas
## Signer

Signer service signs queries sended by client and then client can try to install such signed query on agent.
Agent verifies query using signer public key.  
Signer provides query storage for intalled querues. Each signed query has assigned unique id.  
This id lets agents to know when query has been uninstalled. 

### Usage

Signer management scripts:
- `run` - starts signer and its RMI registry, logs are in `signer.log`
- `stop` - kills signer and RMI
- `clean` - removes logs
- `configure` - prepares configuration file, sets signer RMI and path to keys
- `generate-keys` - generates new key pair into `keys` directory

