package pl.edu.mimuw.cloudatlas.signer;


import pl.edu.mimuw.cloudatlas.common.rmi.RMICallResult;
import pl.edu.mimuw.cloudatlas.common.rmi.SignQueryResult;
import pl.edu.mimuw.cloudatlas.common.rmi.SignedQuery;
import pl.edu.mimuw.cloudatlas.common.rmi.SignerRMI;
import pl.edu.mimuw.cloudatlas.common.security.Hash;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;

import javax.crypto.Cipher;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import static pl.edu.mimuw.cloudatlas.common.security.SignerCipher.generateDescriptor;
import static pl.edu.mimuw.cloudatlas.service.interpreter.InterpreterService.testQuerySyntax;

public class SignerRMIExecutor implements SignerRMI {
    private static final String LOGNAME = "SIGNER-RMI";

    private final SignerService signer;

    private final Cipher cipher;

    private final Map<Attribute, Long> queries;

    private long queryNextId;

    SignerRMIExecutor(SignerService signer, Cipher cipher) {
        this.signer = signer;
        this.cipher = cipher;

        queries = new HashMap<>();
    }

    @Override
    public SignQueryResult signQuery(String name, String queryString) throws RemoteException {
        synchronized (queries) {
            try {
                Attribute attribute = new Attribute(name);
                if (!Attribute.isQuery(attribute)) return new SignQueryResult(RMICallResult.RMICallStatus.FAIL, "Query name invalid! Should start with &");
                if (queries.get(attribute) != null) return new SignQueryResult(RMICallResult.RMICallStatus.FAIL, "Query already exists in storage!");
                StringBuilder msg = new StringBuilder();
                if (!testQuerySyntax(queryString, msg))
                    return new SignQueryResult(RMICallResult.RMICallStatus.FAIL, "Error: " + msg.toString());
                long id = getNextId();
                byte[] descriptor = generateDescriptor(attribute, queryString, id);
                byte[] descriptorHash = Hash.hashMe(descriptor);
                byte[] signature = cipher.doFinal(descriptorHash);
                queries.put(attribute, id);
                return new SignQueryResult(new SignedQuery(signature, attribute, queryString, id));
            } catch (Exception e) {
                return new SignQueryResult(RMICallResult.RMICallStatus.FAIL, e.getMessage());
            }
        }
    }

    @Override
    public SignQueryResult uninstallQuery(String name) throws RemoteException {
        synchronized (queries) {
            try {
                Attribute attr = new Attribute(name);
                if (!Attribute.isQuery(attr)) return new SignQueryResult(RMICallResult.RMICallStatus.FAIL, "Query name invalid! Should start with &");
                if (queries.get(attr) == null) return new SignQueryResult(RMICallResult.RMICallStatus.FAIL, "Query name doesn't exist!");
                long id = getNextId();
                byte[] descriptor = generateDescriptor(attr, "", id);
                byte[] descriptorHash = Hash.hashMe(descriptor);
                byte[] signature = cipher.doFinal(descriptorHash);
                queries.remove(attr);
                return new SignQueryResult(new SignedQuery(signature, attr, "", id));
            } catch (Exception e) {
                return new SignQueryResult(RMICallResult.RMICallStatus.FAIL, e.getMessage());
            }
        }
    }

    public long getNextId() {
        return queryNextId++;
    }
}
