/*
 * bsd-2-clause
 *
 * Copyright (c) 2018 Jacek Łysiak
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package pl.edu.mimuw.cloudatlas.signer;


import org.ini4j.Wini;
import pl.edu.mimuw.cloudatlas.common.rmi.RMIServer;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static java.lang.System.exit;
import static pl.edu.mimuw.cloudatlas.common.security.SignerCipher.*;

/**
 * Signer service.
 *
 * Simple RMI server.
 * - Query verification
 * - Query signing
 *
 *
 * Key generation:
 * - openssl genrsa -out private_key.pem 1024
 *
 * Convert private key:
 * - openssl pkcs8 -topk8 -inform PEM -outform DER -in private_key.pem -out private_key.der -nocrypt
 *
 * Convert public key:
 * - openssl rsa -in private_key.pem -pubout -outform DER -out public_key.der
 */
public class SignerService {
    private final static String name = "SIGNER";

    private final Object semaphore;


    private SignerService(String configFile) throws IOException, InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException {
        Log.log(name, "Creating service...");

        semaphore = new Object();

        // Load agent configuration from *.ini file
        Wini ini = new Wini(new File(configFile));

        // Load RMI server config
        String rmiHost = ini.get("RMI", "HOST");
        int rmiPort = ini.get("RMI", "PORT", int.class);

        String publicKeyPath = ini.get("KEY", "PUBLIC_PATH");
        String privateKeyPath = ini.get("KEY", "PRIVATE_PATH");

        KeyPair keyPair = new KeyPair(getPublicKey(publicKeyPath), getPrivateKey(privateKeyPath));
        Cipher signer = getSigner(keyPair.getPrivate());

        // Create and run RMI server
        RMIServer server = new RMIServer(rmiHost, rmiPort, new SignerRMIExecutor(this, signer));
        server.run();

        // Add clean shutdown
        Runtime.getRuntime().addShutdownHook(new Thread() {
            private final String name = "SHUTDOWN";
            @Override
            public void run() {
                Log.log(name, "Interrupt caught! Closing system...");
                synchronized (semaphore) {
                    semaphore.notify();
                }
            }
        });

    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Configuration file not provided!");
            System.exit(1);
        }
        try {
            SignerService signer = new SignerService(args[0]);
            signer.run();
        } catch (IOException | InvalidKeySpecException | NoSuchAlgorithmException | InvalidKeyException | NoSuchPaddingException e) {
            Log.fatal(name, "CloudAtlas signer service startup exception...");
            Log.fatal(name, "Error message: " + e.getMessage());
            exit(1);
        }
    }

    private void run() {
        synchronized (semaphore) {
            try {
                Log.log(name, "Waiting for shutdown...");
                semaphore.wait();
            } catch (InterruptedException ignored) {
            }
        }
    }
}
