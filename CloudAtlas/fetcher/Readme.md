# CloudAtlas
## Fetcher

Fetcher service collects data from host and feeds agent's singleton zone.

### Usage

Fetcher starts as a daemon and stores its logs in `$hostname-fetcher.log` file.  
Some attributes are predefined in configuration template already, however it's possible to extend this set.  
It could be done easly following rules which are explained in configuration file.  
Attributes values are collected by executing complicated bash scripts.

Fetcher management scripts:
- `run` - starts fetcher which tries to connect with agent's RMI server,
- `stop` - kills fetcher
- `clean` - removes logs
- `configure` - asks few questions and prepare configuration file `config/fetcher.ini`

