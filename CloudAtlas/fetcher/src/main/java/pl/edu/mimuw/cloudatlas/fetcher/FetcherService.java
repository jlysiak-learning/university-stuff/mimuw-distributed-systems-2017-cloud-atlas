/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.fetcher;

import org.ini4j.Wini;
import pl.edu.mimuw.cloudatlas.common.rmi.CloudAtlasRMI;
import pl.edu.mimuw.cloudatlas.common.rmi.RMICallResult;
import pl.edu.mimuw.cloudatlas.common.rmi.RMIClient;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;
import pl.edu.mimuw.cloudatlas.fetcher.attributefetcher.AttributeFetcher;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * FetcherService main class. FetcherService run data from machine and sets corresponding attributes in corresponding
 * CloudAtlas singleton zone.
 */
public class FetcherService {
    private final static String LOGNAME = "FETCHER-SERVICE";

    /// Collection of attribute fetchers
    private final List<AttributeFetcher> attributeFetchers;

    private String caHost;

    private int caPort;

    private long collectingInterval;


    private RMIClient client;

    private FetcherService(String configFile) throws IOException {
        Log.log(LOGNAME,"Creating fetcher...");

        // Load fetcher configuration from *.ini file
        Wini ini = new Wini(new File(configFile));
        loadConfiguration(ini);

        // Create attribute fetchers from configuration file
        attributeFetchers = new ArrayList<>();
        loadFetchingAttributes(ini);

        // Prepare RMI client
        Log.log(LOGNAME,"Connecting fetcher to RMI server @ " + caHost + ":" + caPort);
        client = new RMIClient(caHost, caPort);
        client.run();
    }

    /**
     * Load global fetcher configuration.
     *
     * @param ini #Wini object
     */
    private void loadConfiguration(Wini ini) {
        collectingInterval = ini.get("FETCHER", "FETCH_INTERVAL", long.class);
        caHost = ini.get("RMI", "RMI_HOST");
        caPort = ini.get("RMI", "RMI_PORT", int.class);
    }

    /**
     * Create single attribute fetchers from configuration file.
     * @param ini #Wini object
     */
    private void loadFetchingAttributes(Wini ini) {
        String[] listString = ini.get("ATTRIBUTES", "attributes").split(":");
        for (String name : listString) {
            try {
                // Create new fetcher based upon loaded data
                AttributeFetcher fetcher = new AttributeFetcher(name, ini.get(name));
                // Append fetcher to list
                attributeFetchers.add(fetcher);
            } catch (IllegalArgumentException e) {
                Log.err(LOGNAME, "Wrong arguments! Fetcher for attribute " + name + " is skipped! Message: " + e.getMessage());
            } catch (NullPointerException e) {
                Log.err(LOGNAME, "Attribute fetcher '" + name + "' defined on fetcher list but its section is not " +
                        "defined! :" + e.getMessage());
            } catch (Exception e) {
                Log.err(LOGNAME, "Unrecognized exception! Shutdown..." + e.getMessage());
                System.exit(1);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("Configuration file not provided!");
            System.exit(1);
        }
        FetcherService fetcherService = new FetcherService(args[0]);
        fetcherService.run();
        Log.log(LOGNAME, "Fetcher service shutdown...");
    }

    /**
     * Start fetcher.
     */
    private void run() {
        while (true) {
            fetchAll();
            try {
                Thread.sleep(collectingInterval);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Get data from hosting machine and set attributes.
     */
    private void fetchAll() {
        for (AttributeFetcher fetcher : attributeFetchers) {
            try {
                // Fetch attribute value from machine.
                fetcher.fetchAttribute();
                RMICallResult result = ((CloudAtlasRMI) client.getRemote()).setZMIAttribute(fetcher.getName(), fetcher.getValue());
                Log.log(LOGNAME, String.format("name: %s, value: %s, info: %s, status: %s", fetcher.getName(), fetcher.getValue().toString(), result.getInfo(), result.getStatus().name()));
            } catch (IOException e) {
                Log.err(LOGNAME, "Shell command '" + fetcher.getCmd() + "'execution IOexception: " + e.getMessage());
            } catch (Exception e) {
                Log.err(LOGNAME, "Remote exception for attribute: " + fetcher.getName() +"\nMessage: " + e.getMessage());
                e.printStackTrace(System.err);
            }
        }
    }
}
