/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.fetcher.attributefetcher;

import org.ini4j.Profile;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.type.PrimaryType;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.*;
import pl.edu.mimuw.cloudatlas.fetcher.attributefetcher.datacollection.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class AttributeFetcher {
    private final Attribute attribute;
    private final String bashCmd;

    // Data collector responsible for store and auto averaging
    private final DataCollection data;

    public AttributeFetcher(String name, Profile.Section iniSection) {
        attribute = new Attribute(name);
        bashCmd = iniSection.get("bashCmd");

        TypePrimitive type = TypePrimitive.getInstanceOf(PrimaryType.valueOf(iniSection.get("type")));
        CollectionMode mode = CollectionMode.valueOf(iniSection.get("mode"));
        switch (mode) {
            case SINGLE:
                data = new SingleDataCollection(type);
                break;

            case AVERAGE:
                AveragingMode avg = AveragingMode.valueOf(iniSection.get("avg"));
                int limit = iniSection.get("limit", int.class);
                data = new AverageDataCollection(type, avg, limit);
                break;

            default:
                throw new IllegalArgumentException("Not such collection mode!");
        }
    }

    private ArrayList<String> runShellCommand(String cmd) throws IOException, InterruptedException {
        ArrayList<String> lines = new ArrayList<>();
        Process proc;

        // Build String array instead of running as single command,
        // it caused problem with single line parsing (why?.. \(o.0)/)
        String[] cmdList = {"/bin/sh", "-c", cmd};
        proc = Runtime.getRuntime().exec(cmdList);
        proc.waitFor();
        BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        return lines;
    }

    public void fetchAttribute() throws IOException, InterruptedException {
        ArrayList<String> output;
        output = runShellCommand(bashCmd);
        data.push(new ValueString(output.get(0)));
    }

    public Value getValue() {
        return data.getValue();
    }

    public String getName() {
        return attribute.getName();
    }

    public String getCmd() {
        return bashCmd;
    }
}
