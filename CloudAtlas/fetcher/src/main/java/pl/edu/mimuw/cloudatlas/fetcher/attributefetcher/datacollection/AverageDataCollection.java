/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.fetcher.attributefetcher.datacollection;

import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueDouble;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueString;

import java.util.ArrayList;
import java.util.List;

public class AverageDataCollection extends DataCollection {
    private final List<Value> values;

    private final List<Double> weights;

    private int begin;
    private int size;
    private final int limit;

    private Value mean;

    public AverageDataCollection(TypePrimitive typePrimitive, AveragingMode avg, int limit) {
        super(typePrimitive);
        if (!typePrimitive.equals(TypePrimitive.INTEGER) && !typePrimitive.equals(TypePrimitive.DOUBLE))
            throw new IllegalArgumentException("Values of type " + typePrimitive.toString() + " cannot be averaged!");

        this.limit = limit;
        this.begin = 0;
        this.size = 0;
        this.mean = new ValueDouble(0.).convertTo(type);
        values = new ArrayList<>();
        zeroList(values);
        weights = new ArrayList<>(limit);
        setWeights(weights, avg);
    }

    @Override
    public void push(ValueString value) {
        values.set(begin, value.convertTo(type));
        begin = (begin + 1) % limit;
        size++;
        if (size > limit) size = limit;
        mean = calculate();
    }

    @Override
    public Value getValue() {
        return mean;
    }

    private void zeroList(List<Value> list) {
        for (int i = 0; i < limit; i++)
            list.add(new ValueDouble(0.).convertTo(type));
    }

    private void setWeights(List<Double> list, AveragingMode avg) {
        double w = 1;
        switch (avg) {
            case UNI:
                for (int i = 0; i < limit; i++)
                    list.add(w);
                break;

            case EXP:
                for (int i = 0; i < limit; i++) {
                    list.add(w);
                    w /= Math.E;
                }
                break;
        }
    }

    /**
     * Arithmetic weighted.
     * @return Arithmetic weighted mean.
     */
    private Value calculate() {
        double result = 0;
        double weightSum = 0;
        for (int i = 0; i < size; i++) {
            // Weights list's elements are in order, values are in inverse order.
            // Watch out on modulo acting on negative values.
//            System.out.println("Calc: begin: " + ((begin - i - 1 + limit) % limit));
//            System.out.println("Calc: get el: " + ((begin - i - 1 + limit) % limit));
            double v = ((ValueDouble) values.get((begin - i - 1 + limit) % limit).convertTo(TypePrimitive.DOUBLE)).getValue();
            result += weights.get(i) * v;
            weightSum += weights.get(i);
        }
        return new ValueDouble(result / weightSum).convertTo(type);
    }
}
