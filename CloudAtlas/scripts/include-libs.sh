#!/bin/bash

INC_LIBS=""
for lib in $(ls ${PATH_LIBS});
do
  INC_LIBS=${PATH_LIBS}/$lib:$INC_LIBS
done

# Save extra libs in variable
INC_LIBS=${INC_LIBS%:}
