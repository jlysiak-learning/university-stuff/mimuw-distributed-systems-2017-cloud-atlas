#!/bin/bash

##################################
# Check configuration file on 

if [ ! -e "${CONFIG_FILE}" ]; then
  echo -e "\033[91mConfiguration file '${CONFIG_FILE}' doesn't exists!\033[0m\n"
  echo -e "Please, configure service and run start script again...\n\033[0m"
  exit 1
fi
