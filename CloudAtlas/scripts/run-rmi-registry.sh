#!/bin/bash

# Run Java RMI registry

# Prepare ClassPath
CLASSPATH=$1
RMI_HOST=$2
RMI_PORT=$3

export CLASSPATH

rmiregistry $RMI_PORT &
RMI_PID=$!

echo -e "RMI registry [pid: $RMI_PID] is running @ $RMI_HOST:$RMI_PORT"
