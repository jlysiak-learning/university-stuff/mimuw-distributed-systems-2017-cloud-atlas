#!/bin/ bash

# Jacek Lysiak <jacek.lysiako.o@gmail.com>
#
# Export project paths to setup environment properly
#
# This script is sourced from run.sh
# Don't run it alone.

SCRIPTS_DIR=$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)
ROOT_DIR=${SCRIPTS_DIR%/scripts}

# Project root
PATH_ROOT=${ROOT_DIR}

# Project outputs
PATH_TARGET=$PATH_ROOT/target

# Comiled code
PATH_CLASS=$PATH_TARGET/classes

# Scripts path
PATH_SCRIPTS=${PATH_ROOT}/scripts

# Templates path
PATH_TEMPLATES=${PATH_ROOT}/templates

# Compiled common client-server code 
PATH_CLASS_COMMON=$PATH_CLASS/pl/edu/mimuw/cloudatlas/remote/common

# External manual installed libs
PATH_LIBS=$PATH_ROOT/libs

# Common module
PATH_COMMON_CLASS=${PATH_ROOT}/common/target/classes

# Datamodel module
PATH_DATAMODEL_CLASS=${PATH_ROOT}/datamodel/target/classes

# Interpreter module
PATH_INTERPRETER_CLASS=${PATH_ROOT}/interpreter/target/classes

echo "Exporting..."
echo $PATH_ROOT
