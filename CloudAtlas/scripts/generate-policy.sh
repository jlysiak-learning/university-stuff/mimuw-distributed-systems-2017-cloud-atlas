#!/bin/bash

# Generate policy file

POLICY_FILE=policy
CODE_BASE=$1

echo "Generating ${POLICY_FILE} file..."
cat "${PATH_TEMPLATES}/policy.tmp" | sed 's,##PATH##,'${CODE_BASE}',' > ${POLICY_FILE}
