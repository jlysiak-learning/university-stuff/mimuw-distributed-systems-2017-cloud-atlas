# CloudAtlas
## Clinet / Web service

Client provides WWW service where one can observe CloudAtlas system state from selected agent's point of view.  
Web client should be started at the end. After agent and signer.  

### Usage

Client management scripts:
- `run` - starts client, connects with agent and signer
- `stop` - kills client
- `clean` - removes logs
- `configure` - asks few questions and prepare configuration file, you're asked for agent's and signer's RMI and port for WWW service


