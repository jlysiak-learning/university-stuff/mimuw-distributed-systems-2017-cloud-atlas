/**
 * Remove tables for attributes and queries.
 */
function removeElementsFromId(id) {
    var element = document.getElementById(id);
    while (element.firstChild)
        element.removeChild(element.firstChild);
}

/**
 * Create table.
 */
function createTableIn(idParent, idTable) {
    var element = document.getElementById(idParent);
    var table = document.createElement('table');
    table.id = idTable;
    element.appendChild(table);
    return table;
}