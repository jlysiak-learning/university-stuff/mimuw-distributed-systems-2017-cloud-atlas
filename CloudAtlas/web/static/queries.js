function uninstallQuery(queryName) {
    var request = {cmd: "uninstall-query", queryName: queryName};
    makeRequest(request, handleQueryUninstall);
}

function handleQueryUninstall(response) {
    showNotify(response['msg'], response['status']);
    makeRequest({cmd: 'get-zone-attributes', path: activePath}, updateAttributesOnly);
}

function installQuery() {
    var name = document.getElementById('query-name').value;
    var query = document.getElementById('query-content').value;
    var request = {cmd: "install-query", queryName: name, query: query};
    console.log(request);
    makeRequest(request, handleQueryInstall);
}

function handleQueryInstall(response) {
    showNotify(response['msg'], response['status']);
    if (response.status === 'ok') {
        makeRequest({cmd: 'get-zone-attributes', path: activePath}, updateAttributesOnly);
        document.getElementById('query-name').value = "";
        document.getElementById('query-content').value = "";
    }
}