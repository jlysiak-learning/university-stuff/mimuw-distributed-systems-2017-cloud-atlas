function showNotify(msg, type) {
    var x = document.getElementById("notify-bar");
    x.textContent = msg;
    if (type == "ok")
        x.style.border = "2px solid #12fd00";
    else if (type == "error")
        x.style.border = "2px solid #FD2100";
    else if (type == "warning")
        x.style.border = "2px solid #FDFA00";
    else
        x.style.border = "2px solid #94D665";

    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 4000);
}