/* WEB SERVICE VARIABLES */
var agentPath = null;   // Connected agent zone's full path
var activePath = null;  // Actually active path

// All zones stored on agent
var zonesList = [];
// Active path attributes map, list of {name: x, val: x, type: x}
var attributesList = [];

var attributesRefreshRate = 1000;   // in millis
var timerHandler = null;            // Timer handler
var liveCharts = [];                // Created charts



/* WEB SERVICE METHODS */

/**
 * Send data to server and handle response.
 * If error, run error handler.
 *
 * Request JSON:
 *  - cmd: command, OBLIGATORY
 *
 * Response JSON:
 *  - status: request status, OBLIGATORY
 *  - mgs: request status message, OBLIGATORY
 *
 * @param dataToSend JSON to send
 * @param responseHandler function handler(JSON object), handle results
 */
function makeRequest(dataToSend, responseHandler) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var response = JSON.parse(this.response);
            responseHandler(response);
        } else if (this.readyState === 4 && this.status !== 200) {
            console.error(this.response);
            showNotify("Request failed!", 'error');
        }
    };
    xhttp.open("POST", "/rest", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send(JSON.stringify(dataToSend));
}

/*---- WEB SERVICE INITIALIZERS */
/**
 *  Initialize service
 */
function initialize() {
    makeRequest({cmd: 'get-agent-path'}, initializeSetAgentLoadZones);
}

function initializeSetAgentLoadZones(response) {
    setAgentPath(response['agentPath']);
    updateListAndAttributes();
    if (timerHandler === null) {
        timerHandler = window.setInterval(updateListAndAttributes, attributesRefreshRate);
    }
}

/**
 * Update page after some interval.
 */
function updateListAndAttributes() {
    // Start with updating znones list
    makeRequest({cmd: 'get-stored-zmis'}, setZonesList);
    // Attributes table will be called automatically after setting zones list
}

function setAgentPath(path) {
    var div = document.getElementById('agent-info-path');
    div.textContent = path;
    agentPath = path;
    activePath = "/"; // Set active path to root
}

function setZonesList(response) {
    zonesList = response['zones-list'];
    updateZonesList();
    // Here make request for active path attributes.
    makeRequest({cmd: 'get-zone-attributes', path: activePath}, updateAttributes);
}

/**
 * General method to update attributes within page, including graph plotting.
 * @param response incoming JSON
 */
function updateAttributes(response) {
    // Update table
    attributesList = response['attributes-list'];
    updateAttributesTables();
    // Iterate over existing charts and update
    updateMonitors();
}

/**
 * General method to update attributes within page, on query list change.
 * @param response incoming JSON
 */
function updateAttributesOnly(response) {
    // Update table
    attributesList = response['attributes-list'];
    updateAttributesTables();
}

/**
 * Update attributes table.
 */
function updateAttributesTables() {
    removeElementsFromId('attributes-div');
    removeElementsFromId('queries-div');
    var tableAttr = createTableIn('attributes-div', 'attributes-table');
    var tableQuery = createTableIn('queries-div', 'queries-table');

    // Add table headers
    var trHead = document.createElement('tr');
    var th = document.createElement('th');
    tableAttr.appendChild(trHead);
    trHead.appendChild(th);
    th.colSpan = 3;
    th.textContent = "Attributes of: " + activePath;

    trHead = document.createElement('tr');
    th = document.createElement('th');
    tableQuery.appendChild(trHead);
    trHead.appendChild(th);
    th.colSpan = 3;
    th.textContent = "Queries of: " + agentPath;


    attributesList.forEach(function(entry) {
        // console.log(entry);
        var tr = document.createElement('tr');
        var tdType = document.createElement('td');
        var tdName = document.createElement('td');
        var tdVal = document.createElement('td');
        tdVal.textContent = entry['val'];
        tdType.textContent = entry['type'];
        tr.appendChild(tdName);
        tr.appendChild(tdType);
        tr.appendChild(tdVal);
        if (entry['type'] === 'INT' || entry['type'] === 'DOUBLE' ||  entry['type'] === 'QUERY') {
            var a = document.createElement('a');
            a.href = '#';
            a.textContent = entry['name'];
            if (entry['type'] === 'QUERY')
                a.onclick = function () { // remove query callback
                    uninstallQuery(entry.name);
                };
            else {
                a.onclick = function () { // run monitor callback
                    startMonitor(entry.name, parseFloat(entry.val));
                };
            }
            tdName.appendChild(a);
        } else {
            tdName.textContent = entry['name'];
        }

        if (entry['type'] !== 'QUERY') {
            tableAttr.appendChild(tr);
        } else {
            tr.id = entry['name'].substring(1);
            tableQuery.appendChild(tr);
        }

    });
}

/**
 * Update list of zones
 */
function updateZonesList() {
    removeElementsFromId('zones-list');
    var ul = document.getElementById('zones-list');
    zonesList.forEach(function (entry) {
        // console.log(entry);
        var li = document.createElement('li');
        var a = document.createElement('a');
        a.href = "#";
        a.onclick = function () {
            changeActivePath(entry);
        };
        li.textContent = entry;
        if (entry === activePath) {
            li.className = "selected";
        }
        a.appendChild(li);
        ul.appendChild(a);
    });
}

/**
 * Return connected agent path
 * @returns {*}
 */
function getAgentPath() {
    return agentPath;
}

/**
 * Change active path.
 * @param path
 */
function changeActivePath(path) {
    activePath = path;
    // Remove graph
    removeElementsFromId('attribute-monitor');
    liveCharts.forEach(function (entry) {
        entry.graph.destroy();
    });
    liveCharts = [];
    updateListAndAttributes();
}

/**
 * Create graph for given attribute name
 */
function startMonitor(attrName, startValue) {
    var monitorExists = false;
    liveCharts.forEach(function (entry) {
        if (entry.opt.attribute === attrName) {
            monitorExists = true;
        }
    });
    if (monitorExists) {
        showNotify("Monitor for attribute " + attrName + " already exists!", "warning");
        return;
    }

    var monitor = {
        attribute: attrName,
        zone: activePath,
        speed: 1000,
        samples: 30,
        values: [],
        labels: []
    };
    monitor.values.length = monitor.samples;
    monitor.labels.length = monitor.samples;
    monitor.values.fill(startValue);
    monitor.labels.fill("");


    var destination = document.getElementById('attribute-monitor');
    var chart = document.createElement('canvas');
    destination.appendChild(chart);
    chart.className = 'attribute-graph';
    chart.height = 200;
    var ctx = chart.getContext('2d');

    var graph = new Chart(ctx, {
        type: 'line',
        data: {
            labels: monitor.labels,
            datasets: [{
                data: monitor.values,
                backgroundColor: 'rgba(196, 231, 23, 0.2)',
                borderColor: 'rgba(196, 231, 23, 0.9)',
                borderWidth: 2,
                lineTension: 0.25,
                pointRadius: 2
            }]
        },
        options: {
            title: {
                display: true,
                text: activePath + ": " + attrName,
                fontColor: 'rgba(200, 200, 200, 1)',
                fontSize: 18,
                fontFamily: "'Ubuntu Mono', 'Monospaced', 'FreeMono'"
            },
            responsive: false,
            animation: {
                duration: monitor.speed * 1.5,
                easing: 'linear'
            },
            legend: false,
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        color: 'rgba(200, 200, 200, 0.2)'
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        color: 'rgba(200, 200, 200, 0.2)'
                    }
                }]
            }

        }
    });

    liveCharts.push({graph: graph, opt: monitor});
}

function updateMonitors() {
    liveCharts.forEach(function (entry) {
        var newValue = null;
        var name = entry.opt.attribute;
        attributesList.forEach(function (attr) {
            if (name === attr.name) {
                newValue = parseFloat(attr.val);
            }
        });
        if (newValue !== null) {
            advance(entry, "", newValue);
        }
    });
}

function advance(chartEntry, label, value) {
    chartEntry.opt.labels.push(label);
    chartEntry.opt.labels.shift();
    chartEntry.opt.values.push(value);
    chartEntry.opt.values.shift();
    chartEntry.graph.update();
}