
function createInputLine() {
    var tr = document.createElement('tr');
    var td = document.createElement('td');
    var input = document.createElement('input');
    input.className = 'contact-name';
    input.name = 'contact-name';
    input.type = 'text';
    input.placeholder = 'Name';
    td.appendChild(input);
    tr.appendChild(td);

    td = document.createElement('td');
    input = document.createElement('input');
    input.className = 'contact-ip';
    input.name = 'contact-ip';
    input.type = 'text';
    input.placeholder = 'IPv4 address';
    td.appendChild(input);
    tr.appendChild(td);
    tr.className = 'input-line';
    return tr;
}

function addContactField() {
    // var table = document.getElementById('contacts-table');
    var addButton = document.getElementById('contacts-add-slot-line');
    var table = addButton.parentNode;
    var newLine = createInputLine();
    table.insertBefore(newLine, addButton);
}

function setContacts() {
    contactsList = [];
    var list = document.getElementsByClassName('input-line');
    for (var i = 0; i < list.length; i++) {
        var name = list.item(i).childNodes[0].childNodes[0].value;
        var address = list.item(i).childNodes[1].childNodes[0].value;
        if (/^(\/[a-zA-Z0-9]+)+$/.test(name)) {
            list.item(i).childNodes[0].style.border = '1px solid green';
        } else {
            list.item(i).childNodes[0].style.border = '1px solid red';
            return;
        }

        if (validateAddress(address.split("."))) {
            list.item(i).childNodes[1].style.border = '1px solid green';
        } else {
            list.item(i).childNodes[1].style.border = '1px solid red';
            return;
        }
        contactsList.push({name: name, address: address});
    }
    var request = {cmd: 'set-fallback-contacts', contactsList: contactsList};
    makeRequest(request, handleContactsRequest);
}

function handleContactsRequest(response) {
    showNotify(response.msg, response.status);
    if (response.status === 'ok') {
        while (document.getElementsByClassName('input-line').length !== 0) {
            var lines = document.getElementsByClassName('input-line');
            var parent = lines[0].parentNode;
            parent.removeChild(lines[0]);
        }
    }
}


function validateAddress(address) {
    if (address.length !== 4)
        return false;
    var res = true;
    address.forEach(function (t) {
        if (!/^2[0-4][0-9]$|^25[0-5]$|^1[0-9][0-9]$|^[1-9][0-9]$|^[0-9]$/.test(t))
            res = false;
    });
    return res;
}
