/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.web.handlers;

import com.sun.net.httpserver.HttpExchange;
import pl.edu.mimuw.cloudatlas.common.rmi.RMIClient;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

/**
 * Static files handler.
 * Sends all requested files to browser.
 */
public class StaticHandler extends RequestHandler{
    private static final String LOGNAME = "STATIC-HANDLER";

    private static final String STATIC_DIRECTORY = "static";
    private final String wwwRoot;

    public StaticHandler(RMIClient client) {
        super(client);
        wwwRoot = System.getProperty("user.dir") + "/" + STATIC_DIRECTORY;
    }

    /**
     * Handle serving files from static directory.
     * @param httpExchange
     * @throws IOException
     */
    public void handle(HttpExchange httpExchange) throws IOException {
        URI uri = httpExchange.getRequestURI();
        String filePath = wwwRoot + uri.getPath();
        if (uri.getPath().equals("/")) {
            filePath += "index.html";
        }
        Log.log(LOGNAME,getRequestInfo(httpExchange) + ", file: " + uri.getPath());
        File file = new File(filePath).getCanonicalFile();
        if (!file.getPath().startsWith(wwwRoot)) {
            // We are under attack!:
            // Traversal attack -> reject with 403 error.
            Log.err(LOGNAME,"Access forbidden - 403");
            String response = "<h1>403</h1><h2>Access Forbidden!</h2>";
            httpExchange.sendResponseHeaders(403, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        } else if (!file.isFile()) {
            String response = "<h1>404</h1><h2>File not found!</h2>";
            Log.err(LOGNAME,"File not found - 404");
            httpExchange.sendResponseHeaders(404, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        } else {
            // Object exists and is a file: accept with response code 200.
            httpExchange.sendResponseHeaders(200, 0);
            OutputStream os = httpExchange.getResponseBody();
            FileInputStream fs = new FileInputStream(file);
            final byte[] buffer = new byte[0x10000];
            int count = 0;
            while ((count = fs.read(buffer)) >= 0)
                os.write(buffer,0,count);
            fs.close();
            os.close();
        }
    }
}
