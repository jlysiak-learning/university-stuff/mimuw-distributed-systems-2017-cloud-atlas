/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.web.communication;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

/**
 * Create REST factory.
 */
public abstract class RESTRequestFactory {
    public static final String CMD_GET_AGENT_PATH = "get-agent-path";

    public static final String CMD_GET_STORED_ZMIS = "get-stored-zmis";

    public static final String CMD_GET_ZONE_ATTRIBUTES = "get-zone-attributes";

    public static final String CMD_UNINSTALL_QUERY = "uninstall-query";

    public static final String CMD_INSTALL_QUERY = "install-query";

    public static final String CMD_SET_FALLBACK_CONTACTS = "set-fallback-contacts";

    public static final String CMD_UNKNOWN = "unknown-command";

    public static RESTRequest produceRequest(HttpExchange httpExchange) throws IOException, ParseException {
        RESTRequest request;
        String json = getJSONStringFromRequestBody(httpExchange.getRequestBody());
        String cmd = parseCmd(json);

        switch (cmd) {
            case CMD_GET_AGENT_PATH:
                request = new RESTRequestAgentPath(cmd);
                break;

            case CMD_GET_STORED_ZMIS:
                request = new RESTRequestGetStoredZMIs(cmd);
                break;

            case CMD_GET_ZONE_ATTRIBUTES:
                request = new RESTRequestGetZoneAttributes(cmd, json);
                break;

            case CMD_INSTALL_QUERY:
                request = new RESTRequestInstallQuery(cmd, json);
                break;

            case CMD_UNINSTALL_QUERY:
                request = new RESTRequestUninstallQuery(cmd, json);
                break;

            case CMD_SET_FALLBACK_CONTACTS:
                request = new RESTRequestSetFallbackContacts(cmd, json);
                break;

            default:
                request = new RESTRequestUnknown(CMD_UNKNOWN, cmd);
        }
        return request;
    }

    private static String parseCmd(String json) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject jobj = (JSONObject) parser.parse(json);
        return  (String) jobj.get("cmd");
    }

    private static String getJSONStringFromRequestBody(InputStream requestBody) {
        StringBuilder json = new StringBuilder();
        Scanner scanner = new Scanner(requestBody);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            json.append(line);
        }
        return json.toString();
    }
}
