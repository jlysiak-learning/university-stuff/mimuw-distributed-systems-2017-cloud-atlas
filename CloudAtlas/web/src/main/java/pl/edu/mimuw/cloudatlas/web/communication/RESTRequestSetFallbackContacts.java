/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.web.communication;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueContact;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class RESTRequestSetFallbackContacts extends RESTRequest {
    private final ValueList contacts;

    public RESTRequestSetFallbackContacts(String cmd, String json) throws ParseException, UnknownHostException {
        super(cmd);
        contacts = new ValueList(TypePrimitive.CONTACT);

        JSONParser parser = new JSONParser();
        JSONObject jobj = (JSONObject) parser.parse(json);
        JSONArray arr = (JSONArray) jobj.get("contactsList");
        for (Object anArr : arr) {
            JSONObject el = (JSONObject) anArr;
            String name = (String) el.get("name");
            String address = (String) el.get("address");
            contacts.add(new ValueContact(new PathName(name), InetAddress.getByName(address)));
        }
        System.out.println(contacts);
    }

    public ValueList getContacts() {
        return contacts;
    }
}
