/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.web.handlers;

import com.sun.net.httpserver.HttpExchange;
import pl.edu.mimuw.cloudatlas.common.rmi.*;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;
import pl.edu.mimuw.cloudatlas.web.communication.*;

import java.io.IOException;
import java.io.OutputStream;
import java.rmi.RemoteException;

import static pl.edu.mimuw.cloudatlas.web.communication.RESTRequestFactory.*;

public class RESTHandler extends RequestHandler {
    private static final String LOGNAME = "REST-HANDLER";
    
    private final RMIClient signerClient;

    public RESTHandler(RMIClient agentClient, RMIClient signerClient) {
        super(agentClient);
        this.signerClient = signerClient;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        Log.log(LOGNAME, getRequestInfo(httpExchange));
        String body = "";
        RESTRequest request;
        RESTResponse response;

        try {
            request = produceRequest(httpExchange);
        } catch (Exception e) {
            Log.fatal(LOGNAME, "JSON parsing exception: " + e.getMessage());
            body = new RESTResponse(RESTResponse.RESPONSE_ERROR, "JSON parsing error!").toString();
            httpExchange.sendResponseHeaders(200, body.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(body.getBytes());
            os.close();
            return;
        }

        Log.log(LOGNAME,"Incoming request: " + request.toString());
        switch (request.getCmd()) {
            case CMD_GET_AGENT_PATH:
                response = getAgentPath();
                break;

            case CMD_GET_STORED_ZMIS:
                response = getStoredZmis();
                break;

            case CMD_GET_ZONE_ATTRIBUTES:
                response = getZoneAttributes(request);
                break;

            case CMD_INSTALL_QUERY:
                response = installQuery(request);
                break;

            case CMD_UNINSTALL_QUERY:
                response = uninstallQuery(request);
                break;

            case CMD_SET_FALLBACK_CONTACTS:
                response = setFallbackContacts(request);
                break;

            default:
                Log.fatal(LOGNAME,"Unknown command: " + request.getCmd());
                body = new RESTResponse(RESTResponse.RESPONSE_ERROR, "Unknown command: " + request.getCmd()).toString();
                httpExchange.sendResponseHeaders(200, body.length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(body.getBytes());
                os.close();
                return;
        }
        body = response.toString();
        httpExchange.sendResponseHeaders(200, body.length());
        OutputStream os = httpExchange.getResponseBody();
        os.write(body.getBytes());
        os.close();
    }

    private RESTResponse getAgentPath() {
        try {
            return new RESTResponseAgentPath(RESTResponse.RESPONSE_OK, "", ((CloudAtlasRMI) client.getRemote()).whoAreYou());
        } catch (RemoteException e) {
            Log.fatal(LOGNAME,"CloudAtlasRMI exception! Can't get agent path: " + e.getMessage());
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, "Agent doesn't response!");
        }
    }

    private RESTResponse getStoredZmis() {
        try {
            return new RESTResponseStoredZMIs(RESTResponse.RESPONSE_OK, "", ((CloudAtlasRMI) client.getRemote()).getStoredZMIs());
        } catch (RemoteException e) {
            Log.fatal(LOGNAME,"CloudAtlasRMI exception! Can't get stored ZMIs: " + e.getMessage());
            e.printStackTrace(System.err);
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, "Can't read stored ZMIs!");
        }
    }

    private RESTResponse getZoneAttributes(RESTRequest request) {
        RESTRequestGetZoneAttributes getAttrReq = (RESTRequestGetZoneAttributes) request;
        try {
            return new RESTResponseZMIAttributes(RESTResponse.RESPONSE_OK, "", ((CloudAtlasRMI) client.getRemote()).getZMIAttributes(getAttrReq.getPath()));
        } catch (RemoteException e) {
            Log.fatal(LOGNAME,"CloudAtlasRMI exception! Can't get stored ZMIs: " + e.getMessage());
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, "Can't read ZMI's attrubute!");
        }
    }

    private RESTResponse installQuery(RESTRequest request) {
        RESTRequestInstallQuery getInstallReq = (RESTRequestInstallQuery) request;
        RMICallResult result;

        SignQueryResult queryResult;
        try {
            queryResult = ((SignerRMI) signerClient.getRemote()).signQuery(getInstallReq.getQueryName(), getInstallReq.getQuery());
            if (queryResult.getStatus() == RMICallResult.RMICallStatus.FAIL){
                Log.err(LOGNAME,"Installation failed: " + queryResult.getInfo());
                return new RESTResponse(RESTResponse.RESPONSE_ERROR, queryResult.getInfo());
            }
        } catch (RemoteException e) {
            Log.fatal(LOGNAME,"SignerRMI exception: " + e.getMessage());
            e.printStackTrace(System.err);
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, "Signer remote exception!");
        }
        SignedQuery sq = queryResult.getSignedQuery();

        try {
            result = ((CloudAtlasRMI) client.getRemote()).installQuery(sq.getName(), sq.getQueryString(), sq.getSignature(), sq.getId());
        } catch (RemoteException e) {
            Log.fatal(LOGNAME,"CloudAtlasRMI exception: " + e.getMessage());
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, "Agent remote exception!");
        }
        if (result.getStatus() != RMICallResult.RMICallStatus.OK) {
            Log.err(LOGNAME,"Installation failed: " + result.getInfo());
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, result.getInfo());
        }
        return new RESTResponse(RESTResponse.RESPONSE_OK, result.getInfo());
    }

    private RESTResponse uninstallQuery(RESTRequest request) {
        RESTRequestUninstallQuery getUninstallReq = (RESTRequestUninstallQuery) request;
        RMICallResult result;

        SignQueryResult queryResult;
        try {
            queryResult = ((SignerRMI) signerClient.getRemote()).uninstallQuery(getUninstallReq.getQueryName());
            if (queryResult.getStatus() == RMICallResult.RMICallStatus.FAIL){
                Log.err(LOGNAME,"Uninstalling failure: " + queryResult.getInfo());
                return new RESTResponse(RESTResponse.RESPONSE_ERROR, queryResult.getInfo());
            }
        } catch (RemoteException e) {
            Log.fatal(LOGNAME,"SignerRMI exception: " + e.getMessage());
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, "Signer remote exception!");
        }
        SignedQuery sq = queryResult.getSignedQuery();

        try {
            result = ((CloudAtlasRMI) client.getRemote()).uninstallQuery(sq.getName(), sq.getQueryString(), sq.getSignature(), sq.getId());
        } catch (RemoteException e) {
            Log.fatal(LOGNAME,"CloudAtlasRMI exception!: " + e.getMessage());
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, "Agent doesn't response!");
        }
        if (result.getStatus() != RMICallResult.RMICallStatus.OK) {
            Log.err(LOGNAME,"Uninstalling failure: " + result.getInfo());
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, result.getInfo());
        }

        return new RESTResponse(RESTResponse.RESPONSE_OK, result.getInfo());
    }

    private RESTResponse setFallbackContacts(RESTRequest request) {
        RESTRequestSetFallbackContacts setFallbackContactsReq = (RESTRequestSetFallbackContacts) request;
        try {
            ((CloudAtlasRMI) client.getRemote()).setFallbackContacts(setFallbackContactsReq.getContacts());
        } catch (Exception e) {
            Log.fatal(LOGNAME,"Setting contacts failed: " + e.getMessage());
            return new RESTResponse(RESTResponse.RESPONSE_ERROR, "Setting contacts failed. Internal error.");
        }
        return new RESTResponse(RESTResponse.RESPONSE_OK, "New fallback contacts set!");
    }
}
