/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.web.communication;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.AttributesMap;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueString;

import java.util.Iterator;
import java.util.Map;

public class RESTResponseZMIAttributes extends RESTResponse {
    private final AttributesMap attributesMap;

    public RESTResponseZMIAttributes(String status, String msg, AttributesMap map) {
        super(status, msg);
        attributesMap = map;
    }

    @Override
    public String toString() {
        JSONObject obj = new JSONObject();
        obj.put("status", status);
        obj.put("msg", msg);

        JSONArray list = new JSONArray();
        for (Map.Entry<Attribute, Value> entry : attributesMap) {
            JSONObject map = new JSONObject();
            map.put("name", entry.getKey().getName());
            if (Attribute.isQuery(entry.getKey())) map.put("type", "QUERY");
            else map.put("type", entry.getValue().getType().toString());
            map.put("val", entry.getValue().convertTo(TypePrimitive.STRING).toString());
            list.add(map);
        }
        obj.put("attributes-list", list);
        return obj.toJSONString();
    }
}
