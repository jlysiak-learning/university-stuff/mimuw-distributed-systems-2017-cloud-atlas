/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.web;

import com.sun.net.httpserver.HttpServer;
import org.ini4j.Wini;
import pl.edu.mimuw.cloudatlas.common.rmi.CloudAtlasRMI;
import pl.edu.mimuw.cloudatlas.common.rmi.RMIClient;
import pl.edu.mimuw.cloudatlas.common.rmi.SignerRMI;
import pl.edu.mimuw.cloudatlas.common.utils.logger.Log;
import pl.edu.mimuw.cloudatlas.web.handlers.RESTHandler;
import pl.edu.mimuw.cloudatlas.web.handlers.StaticHandler;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;

import static java.lang.System.exit;

/**
 * Poor's Man HTTP server...
 * I'm sorry for all ugly solutions.
 *
 * CloudAtlas web service class.
 * Connect to CloudAtlas agent on given (caHost, caPort) and serve web page on given webPort.
 *
 */
public class WebService {
    private final static String LOGNAME = "WEB-SERVICE";

    private int webPort;

    private HttpServer server;

    private RMIClient<CloudAtlasRMI> agentClient;

    private RMIClient<SignerRMI> signerClient;

    public WebService(String configFile) throws IOException {
        Log.log(LOGNAME, "Crearing CloudAtlas web service...");

        // Load webservice configuration from *.ini file
        Wini ini = new Wini(new File(configFile));
        webPort = ini.get("WEB", "WEB_SERVICE_PORT", int.class);

        String caHost = ini.get("AGENT", "RMI_HOST");
        int caPort = ini.get("AGENT", "RMI_PORT", int.class);

        String signerHost = ini.get("SIGNER", "RMI_HOST");
        int signerPort = ini.get("SIGNER", "RMI_PORT", int.class);

        Log.log(LOGNAME, "Connecting web service to agent's RMI server @ " + caHost + ":" + caPort);
        agentClient = new RMIClient<>(caHost, caPort);
        agentClient.run();

        Log.log(LOGNAME, "Connecting web service to signer's RMI server @ " + signerHost + ":" + signerPort);
        signerClient = new RMIClient<>(signerHost, signerPort);
        signerClient.run();
    }

    /**
     * Entry point. Start web service.
     *
     * @param args configuration file
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Configuration file not provided!");
            exit(1);
        }
        try {
            WebService webService = new WebService(args[0]);
            webService.serve();
        } catch (IOException e) {
            Log.fatal(LOGNAME, "WebService fatal error: " + e.getMessage());
            Log.fatal(LOGNAME, "Exit...");
            exit(1);
        }
    }

    public void serve() {
        Log.log(LOGNAME, "Starting CloudAtlas web service...");
        try {
            server = HttpServer.create(new InetSocketAddress(webPort), 0);
        } catch (IOException e) {
            Log.fatal(LOGNAME, "RMIServer start failed: " + e.getMessage());
            exit(1);
        }
        Log.log(LOGNAME, "Web service started @ http://localhost:" + webPort);
        server.createContext("/", new StaticHandler(agentClient)); // This should serving static files
        server.createContext("/rest", new RESTHandler(agentClient, signerClient)); // And this JSONs
        server.setExecutor(null);
        server.start();
    }
}
