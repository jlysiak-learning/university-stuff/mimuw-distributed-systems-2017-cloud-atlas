/*
  Copyright (c) 2014, University of Warsaw
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, are permitted
  provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of
  conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or other materials provided
  with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
  WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.table;

import pl.edu.mimuw.cloudatlas.datamodel.*;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.Attribute;
import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypeCollection;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueNull;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.exception.InternalInterpreterException;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.exception.NoSuchAttributeException;

import java.util.*;
import java.util.Map.Entry;

public class Table implements Iterable<TableRow> {
    private final List<String> columns = new ArrayList<>();

    private final Map<String, Integer> headersMap = new HashMap<>();

    private final List<TableRow> rows = new ArrayList<>();

    // creates whole table based on a given ZMI
    public Table(ZMI zmi) {
        Set<String> allColumns = new HashSet<>();
        for (ZMI z : zmi.getSons())
            for (Entry<Attribute, Value> e : z.getAttributes())
                allColumns.add(e.getKey().getName());

        columns.addAll(allColumns);
        int i = 0;
        for (String c : columns)
            headersMap.put(c, i++);
        for (ZMI z : zmi.getSons()) {
            Value[] row = new Value[columns.size()];
            for (int j = 0; j < row.length; ++j)
                row[j] = ValueNull.getInstance();
            for (Entry<Attribute, Value> e : z.getAttributes())
                row[getColumnIndex(e.getKey().getName())] = e.getValue();
            appendRow(new TableRow(row));
        }
    }

    public int getColumnIndex(String column) {
        try {
            return headersMap.get(column);
        } catch (NullPointerException exception) {
            throw new NoSuchAttributeException(column);
        }
    }

    public void appendRow(TableRow row) {
        if (row.getSize() != columns.size())
            throw new InternalInterpreterException("Cannot append row. Length expected: " + columns.size() + ", got: " + row.getSize() + ".");
        rows.add(row);
    }

    // creates an empty table with same columns as given
    public Table(Table table) {
        this.columns.addAll(table.columns);
        this.headersMap.putAll(table.headersMap);
    }

    public List<String> getColumns() {
        return Collections.unmodifiableList(columns);
    }

    public ValueList getColumn(String column) {
        if (column.startsWith("&")) {
            throw new NoSuchAttributeException(column);
        }
        try {
            int position = headersMap.get(column);
            List<Value> result = new ArrayList<>();
            for (TableRow row : rows) {
                Value v = row.getIth(position);
                result.add(v);
            }
            Type elementType = TypeCollection.computeElementType(result);
            return new ValueList(result, elementType);
        } catch (NullPointerException exception) {
            throw new NoSuchAttributeException(column);
        }
    }

    @Override
    public Iterator<TableRow> iterator() {
        return rows.iterator();
    }

    public void sort(Comparator<TableRow> comparator) {
        rows.sort(comparator);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(columns.toString() + "\n");
        for (TableRow row : rows) {
            result.append(row.toString()).append("\n");
        }
        return result.toString();
    }

}
