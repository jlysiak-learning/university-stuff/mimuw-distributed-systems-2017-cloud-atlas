/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.binary;

import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;

import static pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.binary.BinaryResults.*;

public interface BinaryOperation {

    BinaryOperation ADD_VALUE = new BinaryOperation() {
        @Override
        public Value perform(Value v1, Value v2) {
            return v1.addValue(v2);
        }

        @Override
        public String toString() {
            return "+";
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return ADD_RESULTS.getResultTypeOrNull(left, right);
        }
    };

    BinaryOperation IS_EQUAL = new BinaryOperation() {
        @Override
        public Value perform(Value v1, Value v2) {
            return v1.isEqual(v2);
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return IS_EQUAL_RESULTS.getResultTypeOrNull(left, right);
        }

        @Override
        public String toString() {
            return "==";
        }
    };

    BinaryOperation IS_LOWER_THAN = new BinaryOperation() {

        @Override
        public Value perform(Value v1, Value v2) {
            return v1.isLowerThan(v2);
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return IS_LOWER_RESULTS.getResultTypeOrNull(left, right);
        }

        @Override
        public String toString() {
            return "<";
        }
    };

    BinaryOperation SUBTRACT = new BinaryOperation() {
        @Override
        public Value perform(Value v1, Value v2) {
            return v1.subtract(v2);
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return SUBTRACT_RESULTS.getResultTypeOrNull(left, right);
        }

        @Override
        public String toString() {
            return "-";
        }
    };

    BinaryOperation MULTIPLY = new BinaryOperation() {
        @Override
        public Value perform(Value v1, Value v2) {
            return v1.multiply(v2);
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return MULTIPLY_RESULTS.getResultTypeOrNull(left, right);
        }

        @Override
        public String toString() {
            return "*";
        }
    };

    BinaryOperation DIVIDE = new BinaryOperation() {
        @Override
        public Value perform(Value v1, Value v2) {
            return v1.divide(v2);
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return DIVIDE_RESULTS.getResultTypeOrNull(left, right);
        }

        @Override
        public String toString() {
            return "/";
        }
    };

    BinaryOperation MODULO = new BinaryOperation() {
        @Override
        public Value perform(Value v1, Value v2) {
            return v1.modulo(v2);
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return MODULO_RESULTS.getResultTypeOrNull(left, right);
        }

        @Override
        public String toString() {
            return "%";
        }
    };

    BinaryOperation AND = new BinaryOperation() {
        @Override
        public Value perform(Value v1, Value v2) {
            return v1.and(v2);
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return BOOL_RESULTS.getResultTypeOrNull(left, right);
        }

        @Override
        public String toString() {
            return "&&";
        }
    };

    BinaryOperation OR = new BinaryOperation() {
        @Override
        public Value perform(Value v1, Value v2) {
            return v1.or(v2);
        }

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return BOOL_RESULTS.getResultTypeOrNull(left, right);
        }

        @Override
        public String toString() {
            return "||";
        }
    };

    Value perform(Value v1, Value v2);

    Type getResultTypeOrNull(Type left, Type right);

    String toString();
}