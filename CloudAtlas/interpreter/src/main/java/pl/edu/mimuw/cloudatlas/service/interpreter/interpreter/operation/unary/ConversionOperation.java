/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.unary;

import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypeCollection;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;

public class ConversionOperation implements UnaryOperation {
    private Type to;

    public ConversionOperation(Type target) {
        to = target;
    }

    @Override
    public Value perform(Value v) {
        return v.convertTo(to);
    }

    @Override
    public Type getResultTypeOrNull(Type type) {
        if (to.equals(TypePrimitive.NULL) || type.equals(TypePrimitive.NULL))
            return to.equals(TypePrimitive.NULL) ? type : to;
        if (type.equals(to)) return to;
        if (to.equals(TypePrimitive.STRING)) return to;
        if (to.isCollection()) {
            if (type.isCollection()) {
                TypeCollection targetC = (TypeCollection) to;
                TypeCollection typeC = (TypeCollection) type;
                return typeC.getElementType().isCompatible(targetC.getElementType()) ? to : null;
            } else return null;
        }
        if ((type.equals(TypePrimitive.DOUBLE) || type.equals(TypePrimitive.DURATION) && to.equals(TypePrimitive.INTEGER)))
            return to;
        if (type.equals(TypePrimitive.STRING) && !to.isCollection() && !to.equals(TypePrimitive.CONTACT))
            return to;
        if (type.equals(TypePrimitive.INTEGER) && (to.equals(TypePrimitive.DOUBLE) || to.equals(TypePrimitive.DURATION)))
            return to;
        return null;
    }

    @Override
    public String toString() {
        return "convert to " + to.toString();
    }

}