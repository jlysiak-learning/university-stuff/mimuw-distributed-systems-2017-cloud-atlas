/*
 * Copyright (c) 2017, University of Warsaw
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.aggregation;

import pl.edu.mimuw.cloudatlas.datamodel.type.PrimaryType;
import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypeCollection;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.*;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.result.Result;

public interface AggregationOperation {
    Value perform(ValueList values);

    AggregationOperation AND = new AggregationOperation() {
        @Override
        public ValueBoolean perform(ValueList values) { // lazy
            ValueList nlist = Result.filterNullsList(values);
            if (nlist.getValue() == null) {
                return new ValueBoolean(null);
            } else if (values.isEmpty()) {
                return new ValueBoolean(true);
            }
            for (Value v : nlist) {
                if (v.getType().isCompatible(TypePrimitive.BOOLEAN)) {
                    if (v.isNull() || !((ValueBoolean) v).getValue()) return new ValueBoolean(false);
                } else throw new IllegalArgumentException("Aggregation doesn't support type: " + v.getType() + ".");
            }
            return new ValueBoolean(true);
        }
    };

    AggregationOperation OR = new AggregationOperation() {

        @Override
        public ValueBoolean perform(ValueList values) { // lazy

            ValueList nlist = Result.filterNullsList(values);
            if (nlist.getValue() == null) {
                return new ValueBoolean(null);
            } else if (values.isEmpty()) {
                return new ValueBoolean(true);
            }
            for (Value v : nlist) {
                if (v.getType().isCompatible(TypePrimitive.BOOLEAN)) {
                    if (!v.isNull()) return new ValueBoolean(true);
                } else throw new IllegalArgumentException("Aggregation doesn't support type: " + v.getType() + ".");
            }
            return new ValueBoolean(false);
        }
    };

    AggregationOperation MIN = new AggregationOperation() {
        @Override
        public Value perform(ValueList values) {
            ValueList nlist = Result.filterNullsList(values);
            if (nlist.getValue() == null || nlist.isEmpty()) {
                return ValueNull.getInstance();
            }
            Value result = nlist.get(0);
            for (Value v : nlist) {
                if (((ValueBoolean) v.isLowerThan(result)).getValue()) {
                    result = v;
                }
            }
            return result;
        }
    };

    AggregationOperation MAX = new AggregationOperation() {
        @Override
        public Value perform(ValueList values) {
            ValueList nlist = Result.filterNullsList(values);
            if (nlist.getValue() == null || nlist.isEmpty()) {
                return ValueNull.getInstance();
            }
            Value result = nlist.get(0);
            for (Value v : nlist) {
                if (((ValueBoolean) v.isLowerThan(result)).negate().and(v.isEqual(result).negate()).getValue()) {
                    result = v;
                }
            }
            return result;
        }
    };

    AggregationOperation COUNT = new AggregationOperation() {
        @Override
        public ValueInt perform(ValueList values) {

            ValueList nlist = Result.filterNullsList(values);
            if (nlist.getValue() == null) {
                return new ValueInt(null);
            }
            return new ValueInt((long) nlist.size());
        }
    };

    AggregationOperation SUM = new AggregationOperation() {
        @Override
        public Value perform(ValueList values) {
            Type elementType = ((TypeCollection) values.getType()).getElementType();
            PrimaryType primaryType = elementType.getPrimaryType();

            if (primaryType != PrimaryType.INT && primaryType != PrimaryType.DOUBLE && primaryType != PrimaryType.DURATION && primaryType != PrimaryType.NULL) {
                throw new IllegalArgumentException("Aggregation doesn't support type: " + elementType + ".");
            }

            ValueList nlist = Result.filterNullsList(values);
            if (nlist.getValue() == null || nlist.isEmpty()) {
                return ValueNull.getInstance();
            }

            Value result = nlist.get(0).getDefaultValue();

            for (Value v : nlist) {
                result = result.addValue(v);
            }

            return result;
        }
    };

    AggregationOperation AVERAGE = new AggregationOperation() {
        @Override
        public Value perform(ValueList values) {

            Type elementType = ((TypeCollection) values.getType()).getElementType();
            PrimaryType primaryType = elementType.getPrimaryType();

            if (primaryType != PrimaryType.INT && primaryType != PrimaryType.DOUBLE && primaryType != PrimaryType.DURATION && primaryType != PrimaryType.NULL) {
                throw new IllegalArgumentException("Aggregation doesn't support type: " + elementType + ".");
            }

            ValueList nlist = Result.filterNullsList(values);
            if (nlist.getValue() == null || nlist.isEmpty()) {
                return ValueNull.getInstance();
            }

            Value result = nlist.get(0).getDefaultValue();

            for (Value v : nlist) {
                result = result.addValue(v);
            }
            Value size = primaryType == PrimaryType.DOUBLE ? new ValueDouble((double) nlist.size()) : new ValueInt((long) nlist.size());
            return result.divide(size);
        }
    };
}