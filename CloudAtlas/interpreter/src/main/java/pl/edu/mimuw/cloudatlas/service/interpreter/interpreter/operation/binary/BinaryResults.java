/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.binary;

import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;

import java.util.*;

import static pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive.*;

public abstract class BinaryResults {

    static final BinaryResults ADD_RESULTS = new BinaryResults() {
        private final TypePrimitive[][] supportedTypes = {{INTEGER, INTEGER, INTEGER}, {DOUBLE, DOUBLE, DOUBLE}, {DURATION, DURATION, DURATION}, {STRING, STRING, STRING}, {TIME, DURATION, TIME}, {DURATION, TIME, TIME}};

        private final Map<TypePrimitive, Map<TypePrimitive, TypePrimitive>> mapping = BinaryResults.createResultsMapping(supportedTypes);

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            if ((left.equals(NULL) || left.isCollection()) && left.isCompatible(right)) {
                return right;
            }
            return checkMappingOrNull(mapping, left, right);
        }


    };

    static final BinaryResults IS_LOWER_RESULTS = new BinaryResults() {
        private final TypePrimitive[] supportedPrimitives = {INTEGER, DOUBLE, DURATION, TIME, STRING, NULL};

        Set<TypePrimitive> supportedPrimitivesSet = new HashSet<>(Arrays.asList(supportedPrimitives));

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            if (left.isCompatible(right) && supportedPrimitivesSet.contains(left)) {
                return BOOLEAN;
            }
            return null;
        }
    };

    static final BinaryResults SUBTRACT_RESULTS = new BinaryResults() {
        private final TypePrimitive[][] supportedTypes = {{INTEGER, INTEGER, INTEGER}, {DOUBLE, DOUBLE, DOUBLE}, {DURATION, DURATION, DURATION}, {TIME, DURATION, TIME}, {TIME, TIME, DURATION}};

        private final Map<TypePrimitive, Map<TypePrimitive, TypePrimitive>> mapping = BinaryResults.createResultsMapping(supportedTypes);

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return checkMappingOrNull(mapping, left, right);
        }


    };

    static final BinaryResults MULTIPLY_RESULTS = new BinaryResults() {
        private final TypePrimitive[][] supportedTypes = {{INTEGER, DURATION, DURATION}, {DOUBLE, DURATION, TIME}, {INTEGER, INTEGER, INTEGER}, {DOUBLE, DOUBLE, DOUBLE}, {DURATION, INTEGER, DURATION}, {DURATION, DOUBLE, DURATION}};

        private final Map<TypePrimitive, Map<TypePrimitive, TypePrimitive>> mapping = BinaryResults.createResultsMapping(supportedTypes);

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return checkMappingOrNull(mapping, left, right);
        }


    };

    static final BinaryResults DIVIDE_RESULTS = new BinaryResults() {
        private final TypePrimitive[][] supportedTypes = {{INTEGER, INTEGER, DOUBLE}, {DURATION, DOUBLE, DURATION}, {DURATION, INTEGER, DURATION}, {DOUBLE, DOUBLE, DOUBLE}};

        private final Map<TypePrimitive, Map<TypePrimitive, TypePrimitive>> mapping = BinaryResults.createResultsMapping(supportedTypes);

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            return checkMappingOrNull(mapping, left, right);
        }


    };

    static final BinaryResults MODULO_RESULTS = new BinaryResults() {
        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            if (left.isCompatible(INTEGER) && left.isCompatible(INTEGER)) {
                return INTEGER;
            }
            return null;
        }
    };

    static final BinaryResults IS_EQUAL_RESULTS = new BinaryResults() {

        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            if (left.isCompatible(right)) {
                return BOOLEAN;
            }
            return null;
        }
    };

    static final BinaryResults BOOL_RESULTS = new BinaryResults() {
        @Override
        public Type getResultTypeOrNull(Type left, Type right) {
            if (left.isCompatible(BOOLEAN) && left.isCompatible(BOOLEAN)) {
                return BOOLEAN;
            }
            return null;
        }
    };

    private static Map<TypePrimitive, Map<TypePrimitive, TypePrimitive>> createResultsMapping(TypePrimitive[][] mapping) {
        Map<TypePrimitive, Map<TypePrimitive, TypePrimitive>> result = new HashMap<>();
        for (TypePrimitive[] tuple : mapping) {
            if (!result.containsKey(tuple[0])) {
                result.put(tuple[0], new HashMap<>());
            }
            result.get(tuple[0]).put(tuple[1], tuple[2]);
        }
        return result;
    }

    private static Type checkMappingOrNull(Map<TypePrimitive, Map<TypePrimitive, TypePrimitive>> mapping, Type left, Type right) {
        if (left.equals(NULL)) {
            if (right.equals(NULL)) return NULL;

            List<Type> possibleTypes = new ArrayList<>();
            mapping.forEach((key, value) -> {
                if (value.containsKey(right)) possibleTypes.add(value.get(right));
            });
            if (possibleTypes.contains(right)) return right;
            if (possibleTypes.size() == 0) return null;
            return possibleTypes.get(0);
        } else if (right.equals(NULL)) {
            if (mapping.containsKey(left)) {
                Collection<TypePrimitive> possibleTypes = mapping.get(left).values();
                if (possibleTypes.contains(left)) return left;
                if (possibleTypes.size() == 0) return null;
                return possibleTypes.iterator().next();
            }
            return null;
        } else if (mapping.containsKey(left) && mapping.get(left).containsKey(right)) {
            return mapping.get(left).get(right);
        }
        return null;
    }

    public abstract Type getResultTypeOrNull(Type left, Type right);
}
