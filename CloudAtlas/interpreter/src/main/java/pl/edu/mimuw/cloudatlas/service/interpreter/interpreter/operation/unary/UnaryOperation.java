/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.unary;

import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueBoolean;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueDouble;

public interface UnaryOperation {

    UnaryOperation NOT = new UnaryOperation() {
        @Override
        public Value perform(Value v) {
            return v.negate();
        }

        @Override
        public String toString() {
            return "~";
        }

        @Override
        public Type getResultTypeOrNull(Type type) {
            if (type.isCompatible(TypePrimitive.BOOLEAN)) return TypePrimitive.BOOLEAN;
            return null;
        }
    };

    UnaryOperation NEGATE = new UnaryOperation() {
        @Override
        public Value perform(Value v) {
            return v.negate();
        }

        @Override
        public Type getResultTypeOrNull(Type type) {
            if (type.isCompatible(TypePrimitive.DOUBLE)) return TypePrimitive.DOUBLE;
            if (type.isCompatible(TypePrimitive.INTEGER)) return TypePrimitive.INTEGER;
            if (type.isCompatible(TypePrimitive.DURATION)) return TypePrimitive.DURATION;
            return null;
        }

        @Override
        public String toString() {
            return "-";
        }
    };

    UnaryOperation VALUE_SIZE = new UnaryOperation() {

        @Override
        public Value perform(Value v) {
            return v.valueSize();
        }

        @Override
        public Type getResultTypeOrNull(Type type) {
            if (type.equals(TypePrimitive.NULL) || type.equals(TypePrimitive.STRING) || type.isCollection())
                return TypePrimitive.INTEGER;
            return null;
        }

        @Override
        public String toString() {
            return "size";
        }
    };

    UnaryOperation ROUND = new UnaryOperation() {
        @Override
        public Value perform(Value v) {
            if (v.getType().isCompatible(TypePrimitive.DOUBLE)) {
                if (v.isNull()) return new ValueDouble(null);
                return new ValueDouble((double) Math.round(((ValueDouble) v).getValue()));
            }
            throw new IllegalArgumentException(TypePrimitive.DOUBLE + " type required");
        }

        @Override
        public Type getResultTypeOrNull(Type type) {
            if (type.isCompatible(TypePrimitive.DOUBLE)) return TypePrimitive.DOUBLE;
            return null;
        }

        @Override
        public String toString() {
            return "round";
        }

    };

    UnaryOperation FLOOR = new UnaryOperation() {
        @Override
        public Value perform(Value v) {
            if (v.getType().isCompatible(TypePrimitive.DOUBLE)) {
                if (v.isNull()) return new ValueDouble(null);
                return new ValueDouble(Math.floor(((ValueDouble) v).getValue()));
            }
            throw new IllegalArgumentException(TypePrimitive.DOUBLE + " type required");
        }

        @Override
        public Type getResultTypeOrNull(Type type) {
            if (type.isCompatible(TypePrimitive.DOUBLE)) return TypePrimitive.DOUBLE;
            return null;
        }

        @Override
        public String toString() {
            return "floor";
        }

    };

    UnaryOperation CEIL = new UnaryOperation() {
        @Override
        public Value perform(Value v) {
            if (v.getType().isCompatible(TypePrimitive.DOUBLE)) {
                if (v.isNull()) return new ValueDouble(null);
                return new ValueDouble(Math.ceil(((ValueDouble) v).getValue()));
            }
            throw new IllegalArgumentException(TypePrimitive.DOUBLE + " type required");
        }

        @Override
        public Type getResultTypeOrNull(Type type) {
            if (type.isCompatible(TypePrimitive.DOUBLE)) return TypePrimitive.DOUBLE;
            return null;
        }

        @Override
        public String toString() {
            return "ceil";
        }
    };

    UnaryOperation IS_NULL = new UnaryOperation() {
        @Override
        public Value perform(Value v) {
            return new ValueBoolean(v.isNull());
        }

        @Override
        public Type getResultTypeOrNull(Type type) {
            return TypePrimitive.BOOLEAN;
        }

        @Override
        public String toString() {
            return "isNull";
        }
    };

    Value perform(Value v);

    Type getResultTypeOrNull(Type type);

    String toString();
}