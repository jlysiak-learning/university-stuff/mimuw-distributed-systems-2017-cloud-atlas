/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.result;

import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.exception.InternalInterpreterException;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.binary.BinaryOperation;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.unary.UnaryOperation;

public class ResultList extends Result {

    private final ValueList list;

    public ResultList(ValueList list) {
        if (list == null) throw new NullPointerException("Null instead of list!");
        this.list = list;
    }

    @Override
    protected Result binaryOperationTyped(BinaryOperation operation, ResultSingle right) {
        return new ResultList(binaryOperationTypedValueList(list, operation, right));
    }

    @Override
    protected Result binaryOperationTyped(BinaryOperation operation, ResultColumn right) {
        throw new InternalInterpreterException("Operation: LIST ? COLUMN not permitted!");
    }

    @Override
    protected Result binaryOperationTyped(BinaryOperation operation, ResultList right) {
        throw new InternalInterpreterException("Operation on lists not permitted!");
    }

    @Override
    protected Result callMe(BinaryOperation operation, Result left) {
        return left.binaryOperationTyped(operation, this);
    }

    @Override
    public Value getValue() {
        throw new InternalInterpreterException("List is not a single value!");
    }

    @Override
    public ValueList getValues() {
        return list;
    }

    @Override
    public Result unaryOperation(UnaryOperation operation) {
        return new ResultList(unaryOperation(list, operation));
    }

    @Override
    public Type getType() {
        return list.getElementType();
    }


}
