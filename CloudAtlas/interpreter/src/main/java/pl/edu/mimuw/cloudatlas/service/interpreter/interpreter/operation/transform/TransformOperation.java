/*
  Copyright (c) 2017, University of Warsaw
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
  following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
  disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.transform;

import pl.edu.mimuw.cloudatlas.datamodel.type.PrimaryType;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypeCollection;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueBoolean;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueSet;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.result.Result;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public interface TransformOperation {
    ValueList perform(ValueList values);

    TransformOperation UNFOLD = new TransformOperation() {
        @Override
        public ValueList perform(ValueList values) {
            if (!((TypeCollection) values.getType()).getElementType().isCollection()) {
                throw new IllegalArgumentException("All elements must have a collection compatible type.");
            }
            ValueList nlist = Result.filterNullsList(values);
            if (nlist.getValue() == null) {
                return new ValueList(null, ((TypeCollection) ((TypeCollection) values.getType()).getElementType()).getElementType());
            } else if (nlist.isEmpty()) {
                return new ValueList(((TypeCollection) ((TypeCollection) values.getType()).getElementType()).getElementType());
            }
            List<Value> ret = new ArrayList<>();
            for (Value v : nlist) {
                if (v.getType().getPrimaryType() == PrimaryType.SET) {
                    ret.addAll((ValueSet) v);
                } else if (v.getType().getPrimaryType() == PrimaryType.LIST) {
                    ret.addAll((ValueList) v);
                }
            }
            return new ValueList(ret, ((TypeCollection) ((TypeCollection) values.getType()).getElementType()).getElementType());
        }
    };

    TransformOperation DISTINCT = new TransformOperation() {
        @Override
        public ValueList perform(ValueList values) {

            if (values.isEmpty()) return new ValueList(((TypeCollection) values.getType()).getElementType());
            List<Value> ret = new ArrayList<>();
            for (Value v : values) {
                if (!ret.contains(v)) {
                    ret.add(v);
                }
            }
            return new ValueList(ret, ((TypeCollection) values.getType()).getElementType());
        }
    };

    TransformOperation SORT = new TransformOperation() {
        @Override
        public ValueList perform(ValueList values) {

            System.out.println("SORT");
            if (values.isEmpty()) return new ValueList(((TypeCollection) values.getType()).getElementType());
            List<Value> ret = new ArrayList<>();
            ret.addAll(values);
            ret.sort(new Comparator<Value>() {
                public int compare(Value v1, Value v2) {

                    if (((ValueBoolean) v1.isLowerThan(v2)).getValue()) {
                        return -1;
                    } else if (((ValueBoolean) v1.isEqual(v2)).getValue()) {
                        return 0;
                    } else {
                        return 1;
                    }
                }
            });
            return new ValueList(ret, ((TypeCollection) values.getType()).getElementType());
        }
    };

}