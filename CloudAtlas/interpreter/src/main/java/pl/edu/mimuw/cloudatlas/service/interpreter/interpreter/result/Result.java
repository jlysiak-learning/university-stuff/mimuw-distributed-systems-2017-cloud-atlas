/*
  Copyright (c) 2017, University of Warsaw All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
  following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
  disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.result;

import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.value.Value;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueList;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.exception.InternalInterpreterException;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.aggregation.AggregationOperation;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.binary.BinaryOperation;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.transform.TransformOperation;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.unary.ConversionOperation;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.unary.UnaryOperation;

import java.util.ArrayList;
import java.util.List;

public abstract class Result {

    static ValueList binaryOperationTypedValueList(ValueList left, BinaryOperation operation, ResultSingle right) {
        Type resultType = operation.getResultTypeOrNull(left.getElementType(), right.getType());
        if (resultType == null)
            throw generateException(operation, left.getElementType(), right.getType());

        List<Value> result = new ArrayList<>();
        for (Value v : left) {
            result.add(operation.perform(v, right.getValue()));
        }
        return new ValueList(result, resultType);
    }

    private static InternalInterpreterException generateException(BinaryOperation op, Type l, Type r) {
        return new InternalInterpreterException("Unsupported operation: " + l.toString() + " " + op.toString() + " " + r.toString());
    }

    static ValueList binaryOperationTyped(ResultSingle left, BinaryOperation operation, ValueList right) {
        Type resultType = operation.getResultTypeOrNull(left.getType(), right.getElementType());
        if (resultType == null)
            throw generateException(operation, left.getType(), right.getType());

        List<Value> result = new ArrayList<>();
        for (Value v : right) {
            result.add(operation.perform(left.getValue(), v));
        }
        return new ValueList(result, resultType);
    }

    static ValueList unaryOperation(ValueList list, UnaryOperation operation) {
        Type type = operation.getResultTypeOrNull(list.getElementType());
        if (type == null)
            throw new InternalInterpreterException("Unsupported operation: " + operation.toString() + " with " + list.getElementType().toString());
        List<Value> result = new ArrayList<>();
        for (Value v : list) {
            result.add(operation.perform(v));
        }
        return new ValueList(result, type);
    }

    public static ValueList convertTo(ValueList list, Type to) {
        List<Value> result = new ArrayList<>();
        for (Value v : list) {
            result.add(v.convertTo(to));
        }
        return new ValueList(result, to);
    }

    protected abstract Result binaryOperationTyped(BinaryOperation operation, ResultSingle right);

    protected abstract Result binaryOperationTyped(BinaryOperation operation, ResultColumn right);

    protected abstract Result binaryOperationTyped(BinaryOperation operation, ResultList right);

    public Result binaryOperation(BinaryOperation operation, Result right) {
        return right.callMe(operation, this);
    }

    protected abstract Result callMe(BinaryOperation operation, Result left);

    public abstract Value getValue();

    public Result aggregationOperation(AggregationOperation operation) {
        return new ResultSingle(operation.perform(filterNullsList(getValues())));
    }

    public static ValueList filterNullsList(ValueList list) {
        List<Value> result = new ArrayList<>();
        if (list.isEmpty()) return new ValueList(result, list.getElementType());
        for (Value v : list)
            if (!v.isNull()) result.add(v);
        return new ValueList(result, list.getElementType());
    }

    public abstract ValueList getValues();

    public Result transformOperation(TransformOperation operation) {
        return new ResultList(operation.perform(getValues()));
    }

    public Result convertTo(Type to) {
        return this.unaryOperation(new ConversionOperation(to));
    }

    public abstract Result unaryOperation(UnaryOperation operation);

    public abstract Type getType();

}
