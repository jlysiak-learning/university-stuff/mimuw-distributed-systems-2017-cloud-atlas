/*
 * Copyright (c) 2017, Jacek Łysiak <jacek.lysiako.o@gmail.com>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of
 * conditions and the following disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter;

import pl.edu.mimuw.cloudatlas.datamodel.PathName;
import pl.edu.mimuw.cloudatlas.datamodel.ZMI;
import pl.edu.mimuw.cloudatlas.datamodel.attribute.AttributesMap;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.Interpreter;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.QueryResult;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.exception.InterpreterException;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.query.Absyn.Program;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.query.Yylex;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.query.parser;

import java.io.ByteArrayInputStream;
import java.util.List;

import static pl.edu.mimuw.cloudatlas.datamodel.ZMI.generatePathName;

public class InterpreterService {

    public static void executeQueries(ZMI zmi, String query) throws Exception {
        executeQueries(zmi, query, null);
    }

    /**
     * Run query DFS-like procedure at certain ZMI.
     * Install results in ZMI where query was successfully executed.
     * Attributes are not removed.
     *
     * @param zmi
     * @param query
     * @param resultHandler
     * @throws Exception
     * @deprecated used in old assignement-1
     */
    public static void executeQueries(ZMI zmi, String query, QueryResultHandler resultHandler) throws Exception {
        /*  Check whether zone has any child zone.
            The lowest singleton zone is one physical machine and this ZMI is fed by fetcher. */
        if (!zmi.getSons().isEmpty()) {

            for (ZMI son : zmi.getSons())
                executeQueries(son, query, resultHandler); // Execute queries in children first.

            // Create interpreter object for given ZMI
            Interpreter interpreter = new Interpreter(zmi);
            // Parse query
            Yylex lex = new Yylex(new ByteArrayInputStream(query.getBytes()));
            try {
                // Create program and run it in interpreter.
                // Interpreter entry point.
                List<QueryResult> result = interpreter.interpretProgram((new parser(lex)).pProgram());
                PathName zone = generatePathName(zmi);
                for (QueryResult r : result) {
                    if (resultHandler != null)
                        // Here one may execute specific action on generated data via provided interface.
                        resultHandler.handleQueryResult(zmi, r);
                    zmi.getAttributes().addOrChange(r.getName(), r.getValue());
                }
            } catch (InterpreterException exception) {
                // Don't print stacktrace
                // Catch interpreter errors.
                // These are usually errors caused by absence of some attributes.
                // Some results can be calculated only for subtree.
                // exception.printStackTrace();
            }
        }
    }

    /**
     * Put query string and get parsed program, ready to interpret.
     * @param query
     * @return query program
     * @throws Exception
     */
    public static Program parseQueryProgram(String query) throws Exception {
        return Interpreter.parseProgram(query);
    }

    /**
     * Execute query on given ZMI with attached sons ZMIs.
     * Query results are stored in attributes map.
     * If query executes without errors, results are put into map.
     * Otherwise given map will be preserved unchanged.
     * @param queryProgram program to execute
     * @param parentZmi parent ZMI
     * @param results map to store results
     * @return
     */
    public static boolean executeQuery(Program queryProgram, ZMI parentZmi, AttributesMap results) {
        // Create interpreter object for given ZMI
        try {
            Interpreter interpreter = new Interpreter(parentZmi);
            List<QueryResult> list = interpreter.interpretProgram(queryProgram);
            for (QueryResult qr : list)
                results.addOrChange(qr.getName(), qr.getValue());
            return true;
        } catch (Exception ignored) {return false;}
    }

    /**
     * Check query syntax correctness.
     *
     * @param query   query to test
     * @param message result message
     * @return test result: true if ok, false if query syntax is not correct
     */
    public static boolean testQuerySyntax(String query, StringBuilder message) {
        ZMI zmi = new ZMI(null);
        Interpreter interpreter = new Interpreter(zmi);
        Yylex lex = new Yylex(new ByteArrayInputStream(query.getBytes()));
        try {
            interpreter.interpretProgram((new parser(lex)).pProgram());
        } catch (InterpreterException exception) {
            message.append("Syntax correct!\n");
        } catch (Exception e) {
            message.append("Query syntax error!");
            return false;
        }
        message.append("Pass!\n");
        return true;
    }

}
