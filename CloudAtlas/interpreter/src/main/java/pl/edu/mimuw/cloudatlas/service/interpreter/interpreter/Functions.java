/*
  Copyright (c) 2017, University of Warsaw All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
  following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following
  disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
  following disclaimer in the documentation and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package pl.edu.mimuw.cloudatlas.service.interpreter.interpreter;

import pl.edu.mimuw.cloudatlas.datamodel.type.PrimaryType;
import pl.edu.mimuw.cloudatlas.datamodel.type.Type;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypeCollection;
import pl.edu.mimuw.cloudatlas.datamodel.type.TypePrimitive;
import pl.edu.mimuw.cloudatlas.datamodel.value.ValueTime;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.exception.InternalInterpreterException;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.aggregation.*;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.result.Result;
import pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.result.ResultSingle;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

import static pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.unary.UnaryOperation.*;
import static pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.aggregation.AggregationOperation.*;
import static pl.edu.mimuw.cloudatlas.service.interpreter.interpreter.operation.transform.TransformOperation.*;


public class Functions {

    private static Functions instance = null;

    private final ValueTime EPOCH;

    private Functions() {
        try {
            EPOCH = new ValueTime("2000/01/01 00:00:00.000");
        } catch (ParseException exception) {
            throw new InternalInterpreterException("Cannot parse time when creating an EPOCH object.\n" + exception.getMessage());
        }
    }

    public static Functions getInstance() {
        if (instance == null) instance = new Functions();
        return instance;
    }

    Result evaluate(String name, List<Result> arguments) {
        switch (name) {
            case "round":
                if (arguments.size() == 1) return arguments.get(0).unaryOperation(ROUND);
                break;
            case "floor":
                if (arguments.size() == 1) return arguments.get(0).unaryOperation(FLOOR);
                break;
            case "ceil":
                if (arguments.size() == 1) return arguments.get(0).unaryOperation(CEIL);
                break;
            case "now":
                if (arguments.size() == 0)
                    return new ResultSingle(new ValueTime(Calendar.getInstance().getTimeInMillis()));
                break;
            case "epoch":
                if (arguments.size() == 0) return new ResultSingle(EPOCH);
                break;
            case "count":
                if (arguments.size() == 1) return arguments.get(0).aggregationOperation(COUNT);
                break;
            case "size":
                if (arguments.size() == 1) return arguments.get(0).unaryOperation(VALUE_SIZE);
                break;
            case "sum":
                if (arguments.size() == 1) return arguments.get(0).aggregationOperation(SUM);
                break;
            case "avg":
                if (arguments.size() == 1) return arguments.get(0).aggregationOperation(AVERAGE);
                break;
            case "land":
                if (arguments.size() == 1) return arguments.get(0).aggregationOperation(AND);
                break;
            case "lor":
                if (arguments.size() == 1) return arguments.get(0).aggregationOperation(OR);
                break;
            case "min":
                if (arguments.size() == 1) return arguments.get(0).aggregationOperation(MIN);
                break;
            case "max":
                if (arguments.size() == 1) return arguments.get(0).aggregationOperation(MAX);
                break;
            case "unfold":
                if (arguments.size() == 1) return arguments.get(0).transformOperation(UNFOLD);
                break;
            case "distinct":
                if (arguments.size() == 1) return arguments.get(0).transformOperation(DISTINCT);
                break;
            case "sort":
                if (arguments.size() == 1) return arguments.get(0).transformOperation(SORT);
                break;
            case "first":
                if (arguments.size() == 2)
                    return arguments.get(1).aggregationOperation(new AggregationOperationListFirst(arguments.get(0).getValue()));
                break;
            case "last":
                if (arguments.size() == 2) {
                    return arguments.get(1).aggregationOperation(new AggregationOperationListLast(arguments.get(0).getValue()));
                }
                break;
            case "random":
                if (arguments.size() == 2) {
                    return arguments.get(1).aggregationOperation(new AggregationOperationListRandom(arguments.get(0).getValue()));
                }
                break;
            case "to_boolean":
                if (arguments.size() == 1) return arguments.get(0).convertTo(TypePrimitive.BOOLEAN);
                break;
            case "to_contact":
                if (arguments.size() == 1) return arguments.get(0).convertTo(TypePrimitive.CONTACT);
                break;
            case "to_double":
                if (arguments.size() == 1) return arguments.get(0).convertTo(TypePrimitive.DOUBLE);
                break;
            case "to_duration":
                if (arguments.size() == 1) return arguments.get(0).convertTo(TypePrimitive.DURATION);
                break;
            case "to_integer":
                if (arguments.size() == 1) return arguments.get(0).convertTo(TypePrimitive.INTEGER);
                break;
            case "to_string":
                if (arguments.size() == 1) return arguments.get(0).convertTo(TypePrimitive.STRING);
                break;
            case "to_time":
                if (arguments.size() == 1) return arguments.get(0).convertTo(TypePrimitive.TIME);
                break;
            case "to_set":
                if (arguments.size() == 1) {
                    Type t = arguments.get(0).getType();
                    if (t.isCollection()) {
                        Type elementType = ((TypeCollection) t).getElementType();
                        return arguments.get(0).convertTo(new TypeCollection(PrimaryType.SET, elementType));
                    }
                    throw new IllegalArgumentException("First argument must be a collection.");
                }
                break;
            case "to_list":
                if (arguments.size() == 1) {
                    Type t = arguments.get(0).getType();
                    if (t.isCollection()) {
                        Type elementType = ((TypeCollection) t).getElementType();
                        return arguments.get(0).convertTo(new TypeCollection(PrimaryType.LIST, elementType));
                    }
                    throw new IllegalArgumentException("First argument must be a collection.");
                }
                break;
            case "isNull":
                if (arguments.size() == 1) return arguments.get(0).unaryOperation(IS_NULL);
                break;
            default:
                throw new IllegalArgumentException("Illegal function name.");
        }
        throw new IllegalArgumentException("Illegal number of arguments.");
    }
}
