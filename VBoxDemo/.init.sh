#!/bin/bash

# Init script
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

conf_file=env-config

if [ ! -e $conf_file ]; then
  ./env-prepare.sh
fi

source $conf_file       # Load variables
source .setup-paths.sh  # Setup environment paths

PATH=${PATH}:${VLAB_PATH}:${CA_PATH}
export PATH

