# VirtualBox test environment for CloudAtlas distributed system
-------------------------------------------------------------------
## Distributed Systems @ MIMUW - Fall 2017

### Description

This repository contains some scripts used for CloudAtlas testing in VirtualBox-based simple *VirtualLab*.

*VirtualLab* uses my scripts for VMs management which are available [here](https://github.com/jlysiak/virtual-lab).  

Each script should have short description what it does, but in short:
- `cloudatlas-delete-logs.sh` - all services prints some logs to files, so sometimes cleanup is required
- `cloudatlas-deploy.sh` - compile, pack project then upload and unpack on each VM
- `cloudatlas-monitor.sh X` - read and follow log file of agent running on VM `dslab-X`
- `cloudatlas-run.sh` - start agents, signer, fetchers and clinets in screen terminal on all VMs (except signer, see below)
- `cloudatlas-stop.sh` - complementary to run
- `sshm.sh X` - fast SSH to `dslab-X` machine, no password - no cry...
- `env-prepare.sh` - for quick start, provide all required parameters of VirtualLab, VBox OS image and some common paths
- `env-setup.sh` - read confirguration and setup all environment
- `env-clean.sh` - delete add unregister VBox VMs
- `vm-start.sh` - start all VMs
- `vm-stop.sh` - stop all VMs

- `env-config` - contains saved configuration
- `profile-map` - contains mapping of agent profiles on VMs


#### VirtualLab configuration

- 5 VMs are running on host machine, each behind VBox NAT
- VMs are organized in internal network `dsnet`
- Static IP adresses are used:
  - VM name: `dslab-X`, `dsnet` IP: `10.0.0.X` for `X` in `{1, 2, 3, 4, 5}`
- `CloudAtlas-services-config.tar.gz` contains configuration of agent, fetcher, signer and clients for this demo
  - 3-level hierarchy consists of 5 nodes `\uw\violet07`, `\uw\khaki13`, `\uw\khaki31` `\pjwstk\whatever01`, `\pjwstk\whatever02`
- RMI doesn't work with VBox NAT (or at least I don't have enougth time to make it working) so client instance is running on each node with agent and fetcher which simplifies testing because, in this case, only simple WWW service is requried to forward through the NAT
- Signer service is running on `dslab-1`
- Agents logs are accessible from host by `nc localhost 1100X`
- WWW services are forwarded to `1300X` for each `dslab-X`
- SSH is mapped on ports `300X`

#### Image
Here's link to image I used: [LINK](http://www.mediafire.com/file/me00glxyrrrt2l0/debian-ds2017.ova)


