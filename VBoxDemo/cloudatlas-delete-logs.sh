#!/bin/bash

# Deploy CloudAtlas on VBox machines
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh


vms=${VLAB_CONF_DIR}/config/*

vlab-do "cd CloudAtlas/agent && ./clean && cd ../fetcher && ./clean && cd ../web && ./clean && cd ../signer && ./clean" $vms
