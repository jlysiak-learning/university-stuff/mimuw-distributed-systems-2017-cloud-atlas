#!/bin/bash

# Deploy CloudAtlas on VBox machines
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh


vms=${VLAB_CONF_DIR}/config/*

date=$(date +%Y%m%d-%H%M%S)
package=cloudatlas-$date.tar.gz
cloudatlas-pack runtime $package

vlab-upload $package '~' $vms
vlab-do "tar xzf $package && rm -f *.tar.gz" $vms

rm $package
