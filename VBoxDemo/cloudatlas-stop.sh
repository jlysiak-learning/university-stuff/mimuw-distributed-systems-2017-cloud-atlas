#!/bin/bash

# Stop CloudAtlas agents
#
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh

vmdir=${VLAB_CONF_DIR}/config
map=profile-map

IFS=$'\n'
for line in $(cat $map)
do
  i=$(echo $line | cut -d: -f1)
  profile=$(echo $line | cut -d: -f2)
  echo "Stopping agent on $vmdir/$i"
  vlab-do "cd CloudAtlas/web && ./stop && screen -r web -X kill" $vmdir/$i
  vlab-do "cd CloudAtlas/fetcher && ./stop && screen -r fetcher -X kill" $vmdir/$i
  vlab-do "cd CloudAtlas/agent && ./stop && screen -r agent -X kill" $vmdir/$i
  vlab-do "screen -r reader -X kill" $vmdir/$i
  if [[ $i == 1 ]]; then
    vlab-do "cd CloudAtlas/signer && ./stop && screen -r signer -X kill" $vmdir/$i
  fi

done
