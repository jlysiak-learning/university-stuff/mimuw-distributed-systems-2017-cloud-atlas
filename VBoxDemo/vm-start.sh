#!/bin/bash

# Setup VirtualLab for CloudAtlas test
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh

# Setup VirtualBox machines
vlab-start ${VLAB_CONF_DIR}/config/*

