#!/bin/bash

# Setup VirtualLab for CloudAtlas test
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh

echo "Cleaning up..."

vlab-clean ${VLAB_CONF_DIR}

