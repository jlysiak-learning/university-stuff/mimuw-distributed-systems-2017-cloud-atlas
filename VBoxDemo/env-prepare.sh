#!/bin/bash
# Prepare environment for setup script
#
# Jacek Lysiak <jacek.lysiako.o@gmail.com>
#

out=env-config

function getdata {
  echo -e "\033[93mPut relative paths to important things below...\033[0m"
  read -e -p "VirtualLab scripts base path: " vlab
  read -e -p "CloudAtlas base directory: " ca
  read -e -p "VBox OS image path: " image
  read -p "Configured user name: " user
  read -p "$user password: " pass
  read -p "root password: " rpass
  read -p "VM images base name: " base_name
  read -p "# of machnes in VLab: " vm_count
  read -p "Virtual network name: " inet_name
  read -p "Base port for SSH forwarding: " ssh_fwd_port_base
  echo "Which GP forwarding will be used?"
  read -p "Log monitor base port: " log_monitor_port_base
  read -p "Agent's RMI registry base port: " rmi_reg_port_base
  read -p "Where VLab can store configuration? [default=./vlab_conf]: " vlab_conf_dir
  
  if [ -z "${vlab_conf_dir}" ]; then
    vlab_conf_dir=./vlab_conf
  fi

  echo "IMAGE_RPATH=$image" > $out
  echo "VLAB_RPATH=${vlab%/}" >> $out
  echo "CA_RPATH=${ca%/}" >> $out
  echo "USER=$user" >> $out
  echo "PASS=$pass" >> $out
  echo "RPASS=$rpass" >> $out
  echo "BASE_NAME=$base_name">> $out
  echo "VM_COUNT=$vm_count">> $out
  echo "INET_NAME=$inet_name">> $out
  echo "SSH_FWD_PORT_BASE=$ssh_fwd_port_base">> $out
  echo "LOG_MONITOR_PORT_BASE=$log_monitor_port_base">>$out
  echo "RMI_REG_PORT_BASE=$rmi_reg_port_base">>$out
  echo "VLAB_CONF_DIR=$vlab_conf_dir">> $out
}

if [ -f $out ]; then
  read -p "Configuration file exists. Overwrite? [y/N]" opt
  case $opt in
    [Yy] )
      echo "Ok.."
      getdata
    ;;
    * )
      echo "Bye!"
    ;;
  esac
else
  getdata
fi

