#!/bin/bash

# Run log monitor for given machine 
#
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh

map=profile-map

i=$1
title=$(cat $map | grep $i:)

ORIG=$PS1
TITLE="\e]2;\"$title\"\a"
PS1=${TITLE}

nc localhost 1100$i

PS1=${ORIG}
