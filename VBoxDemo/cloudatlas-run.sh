#!/bin/bash

# Start CloudAtlas agents
# Existing agent's profiles are mapped onto VMs via 'profile-map' file.
# All programs are fired in screen to stay alive after ssh disconnection.
#
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh

vmdir=${VLAB_CONF_DIR}/config
map=profile-map

# Running commands after reattach session: screen -r agent -X stuff "./stop^M"
# ^M := Ctrl-V + Enter -> CR key

IFS=$'\n'
for line in $(cat $map)
do
  i=$(echo $line | cut -d: -f1)
  profile=$(echo $line | cut -d: -f2)
  monitor_port=11000 # Field no. 7 in config
  echo "Start $profile on $vmdir/$i"
  if [[ $i == 1 ]]; then
    vlab-do "screen -dmS signer bash -c 'cd CloudAtlas/signer && ./run && exec bash'" $vmdir/$i
  fi
  vlab-do "screen -dmS agent bash -c 'cd CloudAtlas/agent && ./run $profile && exec bash'" $vmdir/$i
  vlab-do "screen -dmS reader bash -c 'while true; do tail -f -n 100 CloudAtlas/agent/logs/$profile/agent.log | nc -lkp $monitor_port; done'" $vmdir/$i
  sleep 1
  vlab-do "screen -dmS fetcher bash -c 'cd CloudAtlas/fetcher && ./run && exec bash'" $vmdir/$i
  sleep 1
  vlab-do "screen -dmS web bash -c 'cd CloudAtlas/web && ./run && exec bash'" $vmdir/$i
done 


