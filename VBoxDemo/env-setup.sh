#!/bin/bash

# Setup VirtualLab for CloudAtlas test
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh

echo "Preparing test environment..."
# Setup VirtualBox machines
vlab-setup ${IMAGE_PATH} ${BASE_NAME} ${VM_COUNT} ${INET_NAME} ${SSH_FWD_PORT_BASE} ${USER} ${PASS} ${RPASS} ${VLAB_CONF_DIR}

