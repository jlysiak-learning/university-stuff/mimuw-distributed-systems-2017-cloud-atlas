#!/bin/bash

# Resolve environment paths
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

# Test if file path is relative 
if [[ "${IMAGE_RPATH}" == "/*" ]]; then
  IAMGE_PATH=${IMAGE_RPATH}
else
  IMAGE_PATH=$(cd $(dirname $(pwd)/${IMAGE_RPATH}) && pwd)/$(basename ${IMAGE_RPATH})
fi

# This works for absolute and relative paths
VLAB_PATH=$(cd ${VLAB_RPATH} && pwd)
CA_PATH=$(cd ${CA_RPATH} && pwd)

