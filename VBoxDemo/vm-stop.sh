#!/bin/bash

# Setup VirtualLab for CloudAtlas test
# Jacek Lysiak <jacek.lysiako.o@gmail.com>

source .init.sh

# Setup VirtualBox machines
vlab-stop ${VLAB_CONF_DIR}/config/*

